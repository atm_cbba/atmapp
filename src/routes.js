//Dependecies
import React from 'react';
import { Route, Switch, BrowserRouter } from 'react-router-dom';

//Components
import App from './components/App';
import DeclacionJurada from './components/DeclaracionJurada';
import DeclaracionJuradaCreate from './components/DeclaracionJurada/DeclaracionJuradaCreate';
import DeclaracionJuradaEdit from './components/DeclaracionJurada/DeclaracionJuradaEdit';
import Prescripcion from './components/Prescripcion';
import PrescripcionCreate from './components/Prescripcion/PrescripcionCreate';
import PrescripcionUpdate from './components/Prescripcion/PrescripcionUpdate';
import PrescripcionDetalle from './components/Prescripcion/PrescripcionDetalle';
import IndexFiscalizacion from './components/Prescripcion/IndexFiscalizacion';
import FormConsolidacionFisca from './components/Prescripcion/FormConsolidacionFisca';
import Home from './components/Home';
import Page404 from './components/Page404';
import Login from './components/Usuario/Login';
import RegisterUser from './components/Usuario/RegisterUser';
import ConfirmUser from './components/Usuario/ConfirmUser';
import Perfil from './components/Usuario/Perfil';
import ForgotPassword from './components/Usuario/ForgotPassword'
import ChangePassword from './components/Usuario/ChangePassword'
import TerminosCondiciones from './components/Page/TerminosCondiciones'
import Ayuda from './components/Page/Ayuda'
import Guia from './components/Page/Guia'
import ConsultaTuDeuda from './components/Page/ConsultaTuDeuda'
import Perdonazo2020 from './components/Page/Perdonazo2020'
import Usuario from './components/Usuario'
import ReActivacionCuenta from './components/Usuario/SendMailActivationAccount';
import Gerencia from './components/Gerencia';
import Cram from './components/Cram';
import CramCreate from './components/Cram/Create';
import Ruat from './components/Ruat';
import Notificacion from './components/Notificacion'
import Liquidacion from './components/Notificacion/Liquidacion'
import LiquidacionLista from './components/Notificacion/LiquidacionLista'
import Fiscalizacion from './components/Page/Fiscalizacion'


//https://reacttraining.com/react-router/web/api/Hooks/useparams
//{ document.URL === document.baseURI+'robots' ?  "hola robots": "no hay robots"}
const AppRoutes = () =>
    <App>
        <Switch>
            <Route exact path="/licencia-actividad-economica" component={DeclacionJurada} />
            <Route exact path="/licencia-actividad-economica-create" component={DeclaracionJuradaCreate} />
            <Route exact path="/licencia-actividad-economica-edit" component={DeclaracionJuradaEdit} />
            <Route exact path="/licencia-actividad-economica-ruat" component={Ruat} />
            
            <Route exact path="/prescripcion" component={Prescripcion} />
            <Route exact path="/prescripcion-create" component={PrescripcionCreate} />
            <Route exact path="/prescripcion-edit" component={PrescripcionUpdate} />
            <Route exact path="/prescripcion-fiscalizacion" component={IndexFiscalizacion} />
            <Route exact path="/prescripcion-fiscalizacion/consolidacion-fiscalizacion/:token" component={FormConsolidacionFisca} />
            <Route exact path="/prescripcion-fiscalizacion/prescripcion-detalle/:token" component={PrescripcionDetalle} />
            
            <Route exact path="/login" component={Login} />
            <Route exact path="/usuario-create" component={RegisterUser} />
            <Route exact path="/usuario-confirmar-cuenta" component={ConfirmUser} />
            <Route exact path="/usuario-perfil" component={Perfil} />
            <Route exact path="/usuario" component={Usuario} />
            <Route exact path="/usuario-reactivacion-cuenta" component={ReActivacionCuenta} />
            <Route exact path="/soporte-gerencial" component={Gerencia} />
            <Route exact path="/cram" component={Cram} />
            <Route exact path="/cram-create" component={CramCreate} />
            <Route exact path="/" component={Home} />

            <Route exact path="/recuperar-contrasenia" component={ForgotPassword} />
            <Route exact path="/change-contrasenia" component={ChangePassword} />
            <Route exact path="/terminos-condiciones" component={TerminosCondiciones} />

            <Route exact path="/guia" component={Guia} />
            <Route exact path="/consulta-tu-deuda" component={ConsultaTuDeuda} />
            <Route exact path="/declaraciones-juradas" component={Perdonazo2020} />
            <Route exact path="/notificacion" component={Notificacion} />
            <Route exact path="/liquidaciones" component={Liquidacion} />
            <Route exact path="/liquidaciones/mixtas" component={LiquidacionLista} />

            <Route exact path="/fiscalizacion" component={Fiscalizacion} />
            
            <Route exact path="/ayuda" component={Ayuda} />
            <Route exact path="/page-404" component={Page404} />

            <Route component={Page404} />
        </Switch>
    </App>;

export default AppRoutes;