import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Links from '../../data/link';
import Config from '../../data/config';
import AuthService from '../../components/Usuario/AuthService';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Fetch from '../../components/utils/Fetch';
//import { throwStatement } from '@babel/types';
import Carroucel from '../../data/Carroucel';

// ================ Showcase section ===================
var auth = undefined
class ShowCase extends Component {

    constructor(props) {
        super(props);
        this.Auth = new AuthService();
        auth = this.Auth

        this.fetch = new Fetch();
        this.fetch.setToast(toast);
        this.list_li = []
        this.list_carroucel = []

        this.state = { tipoCambioDolar: 0, tipoCambioUfv: 0, tipoCambioTazaInteres: 0, renderCarrousel: false };
    }

    async componentDidMount() {

        window.jQuery('#show-case-slider').on('slid.bs.carousel', function (event) {
            //console.log('slid at ', event.timeStamp)
        })

        window.jQuery('#show-case-slider').on('show.bs.modal', function (e) {
            e.stopPropagation();
            return e.preventDefault();
        });

        const cotizaciones = await this.fetch.axiosAsyncGet(`api/cobros/cotizaciones`);
        if (cotizaciones !== null && cotizaciones.status === true) {
            if (Boolean(cotizaciones.Dolar)) {
                this.setState({ tipoCambioDolar: cotizaciones.Dolar.cotizacion })
            }

            if (Boolean(cotizaciones.Ufv)) {
                this.setState({ tipoCambioUfv: cotizaciones.Ufv.cotizacion })
            }

            if (Boolean(cotizaciones.Interes)) {
                this.setState({ tipoCambioTazaInteres: cotizaciones.Interes.tasa_nct })
            }
        }

        if (this.list_li.length <= 0) {
            this.list_li = Carroucel.map((item) => {
                return <li key={item.key} data-target="#show-case-slider" data-slide-to={item.key} className={item.class_active}></li>;
            })

            this.list_carroucel = Carroucel.map((item) => {
                return (<div className={"carousel-item " + item.class_active} key={item.key}>
                    <a href={item.href} title={item.title} className="item-title" target={item.target}>
                        <img className="img-fluid" src={item.src} alt={item.alt} />
                    </a>
                </div>);
            })
            this.setState({ renderCarrousel: true })
        }

    }

    render = () => {



        return <div id="showcase">
            <div className="container-fluid showcase padding-left-right-0">

                <div className="row">
                    <div className="col-lg-9 col-md-9 col-sm-12 col-12 bottom-space padding-right-0">

                        {this.state.renderCarrousel
                            ?
                            <div id="show-case-slider" className="carousel slide" data-ride="carousel">
                                <ol className="carousel-indicators">
                                    {this.list_li}
                                </ol>
                                <div className="carousel-inner">
                                    {this.list_carroucel}
                                </div>

                                <a className="carousel-control-prev" href="#show-case-slider" role="button" data-slide="prev">
                                    <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span className="sr-only Previous"></span>
                                </a>
                                <a className="carousel-control-next" href="#show-case-slider" role="button" data-slide="next">
                                    <span className="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span className="sr-only Next"></span>
                                </a>
                            </div>
                            : ""
                        }
                    </div>

                    <div className="col-lg-3 col-md-3 col-sm-12 col-12 padding-left-0">
                        <div className=" text-center showlogo-button">
                            <div className="row">
                                <div className="col-12 show-mobil ">
                                    <img src={Config[2].url + '/static/img/logo_atm.png'} alt="ATM" className="img-fluid" />
                                    {!auth.loggedIn() ? <Link to={{ pathname: Links[5].url }} className="button-style showcase-btn" >Registrate</Link>
                                        : " "}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-12 ">
                        <div className="quotations-container">
                            <div className="quotations-label">COTIZACIONES</div>
                            <div className="quotations helper-dato-quotation ">
                                <p className="helper-marquee-quotation">
                                    &nbsp;<span>Tipos de Cambio</span>&nbsp; {this.state.tipoCambioDolar} | UFV: {this.state.tipoCambioUfv}&nbsp;&nbsp; <span>Tasa Interes</span>&nbsp;MN: {this.state.tipoCambioTazaInteres}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <ToastContainer enableMultiContainer containerId={'Z'}
                position="top-right"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnVisibilityChange
                draggable
                pauseOnHover
            />
            <ToastContainer />
        </div>;
    }
}

export default ShowCase;