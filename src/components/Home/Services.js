import React, { Component } from 'react';
import Links from '../../data/link';
import Config from '../../data/config';

class Services extends Component {

    hanldeOpenModalClick(ev, url) {
        window.jQuery("#modalFormContacts").modal("show")
    }

    handleRedirectUrlClick(ev, url){
        window.location.href = url;
    }

    render = () => {
        return <div id="services" className="blog">
            <div className="container features">
                <div className="row">
                    <div className="col-lg-3 col-md-6 col-sm-12 bottom-space">
                        <div className="folded-corner service_tab_1 pointer" style={{ minHeight: 252}} >
                            <div className="text">
                                <img className='img-thumbnail rounded mx-auto d-block img-noborder' alt='img-ingresos-tributarios'
                                    src={Config[2].url + '/static/img/serv-ingresos-tributarios.png'} style={{height:'8em'}}/>
                                <h2 className="item-title"> Ingresos Tributarios</h2>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-3 col-md-6 col-sm-12 bottom-space">
                        <div className="folded-corner service_tab_1 pointer" style={{ minHeight: 252}} onClick={e => this.handleRedirectUrlClick(e, Links[40].url)}>
                            <div className="text">
                                <img className='img-thumbnail rounded mx-auto d-block img-noborder' alt='img-fiscalizacion'
                                    src={Config[2].url + '/static/img/serv-fiscalizacion.png'} style={{height:'8em'}}/>
                                <h2 className="item-title"> Fiscalización</h2>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-3 col-md-6 col-sm-12 bottom-space">
                        <div className="folded-corner service_tab_1 pointer" style={{ minHeight: 252}} > 
                            <div className="text">
                                <img className='img-thumbnail rounded mx-auto d-block img-noborder' alt='img-legal-tributario'
                                    src={Config[2].url + '/static/img/serv-legal-tributario.png'} style={{height:'8em'}}/>
                                <h2 className="item-title"> Legal Tributario</h2>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-3 col-md-6 col-sm-12 bottom-space">
                        <div className="folded-corner service_tab_1 pointer" style={{ minHeight: 252}}>
                            <div className="text">
                                <img className='img-thumbnail rounded mx-auto d-block img-noborder' alt='img-plataforma'
                                    src={Config[2].url + '/static/img/serv-plataforma.png'} style={{height:'8em'}}/>
                                <h2 className="item-title"> Plataforma</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        ;
    }
}

export default Services;