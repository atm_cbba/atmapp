import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Config from '../../data/config';
import { Link } from 'react-router-dom';
import '../style/redes-sociales.css';

import ModalRedesSociales from '../../components/utils/ModalRedesSociales';

//<!--================== Features section =====================-->
class Features extends Component {

    handleRedirectUrlClick(ev, url) {
        window.location.href = url;
    }

    static propTypes = {
        links: PropTypes.array.isRequired
    }

    componentWillMount() {
        return { data: [] };
    }

    componentDidMount() {

        var detector = new window.MobileDetect(window.navigator.userAgent)
        window.jQuery('#services-slider').carousel({
            interval: Boolean(detector.mobile()) ? 5000 : 500000,
            wrap: false,
            ride: false
        })

        //funciona bien si son varios
        window.jQuery('#services-slider').on('slide.bs.carousel', function (e) {

            let $e = window.jQuery(e.relatedTarget);
            let idx = $e.index();
            let itemsPerSlide = 4;
            let carouselItem = window.jQuery('#services-slider').find(".carousel-item-custom")
            let totalItems = carouselItem.length;

            if (idx >= totalItems - (itemsPerSlide - 1)) {
                let it = itemsPerSlide - (totalItems - idx);
                for (let i = 0; i < it; i++) {
                    if (e.direction === "left") {
                        carouselItem.eq(i).appendTo('.carousel-inner-custom');
                    }
                    else {
                        carouselItem.eq(0).appendTo('.carousel-inner-custom');
                    }
                }
            }
        });
    }

    render = () => {
        const { links } = this.props;

        return <div id="features">
            <div className="container">
                <div className="carousel-custom">
                    <div id="services-slider" className="carousel slide " data-ride="carousel" data-interval="9000">
                        <div className="carousel-inner-custom  row w-100 mx-auto" >
                            <div className="carousel-item carousel-item-custom col-md-3 active bottom-space">
                                <div className="folded-corner service_tab_1 folded-corner-rounded">
                                    <Link to={links[36].url} title={links[36].title}>
                                        <div className="text">
                                            <img className='img-thumbnail rounded mx-auto d-block img-noborder' alt='img-consulta-tu-deuda'
                                                src={Config[2].url + '/static/img/consulta_deuda.png'} />
                                        </div>
                                    </Link>
                                </div>
                                <a href={links[36].url} title={links[36].title} className="item-title link-folded" target="_blank">{links[36].title}</a>
                            </div>
                            <div className="carousel-item carousel-item-custom col-md-3 bottom-space">
                                <div className="folded-corner service_tab_1 folded-corner-rounded">
                                    <Link to={links[1].url} title={links[1].title}>
                                        <div className="text">
                                            <img className='img-thumbnail rounded mx-auto d-block img-noborder' alt='img-actividades-economicas'
                                                src={Config[2].url + '/static/img/actividades_economicas.png'} />
                                        </div>
                                    </Link>
                                </div>
                                <a href={links[1].url} title={links[1].title} className="item-title link-folded">{links[1].title}</a>
                            </div>

                            <div className="carousel-item carousel-item-custom col-md-3 bottom-space">
                                <div className="folded-corner service_tab_1 folded-corner-rounded " >
                                    <Link to={links[38].url} title={links[38].title} title="Declaraciones Juradas">
                                        <div className="text">
                                            <img className='img-thumbnail rounded mx-auto d-block img-noborder' alt='img-prescripcion'
                                                src={Config[2].url + '/static/img/ddjj.png'} />
                                        </div>
                                    </Link>
                                </div>
                                <a href={links[38].url} title={links[38].title} title="Declaraciones Juradas" className="item-title link-folded">{links[38].title}</a>
                            </div>

                            <div className="carousel-item carousel-item-custom col-md-3 bottom-space">
                                <div className="folded-corner service_tab_1 folded-corner-rounded ">
                                    <Link to={links[33].url} title={links[35].title}>
                                        <div className="text">
                                            <img className='img-thumbnail rounded mx-auto d-block img-noborder' alt='notificaciones'
                                                src={Config[2].url + '/static/img/notificaciones.png'} />
                                        </div>
                                    </Link>
                                </div>
                                <a href={links[33].url} title={links[35].title} className="item-title text-uppercase link-folded">{links[35].code}</a>
                            </div>
                        </div>
                        <a className="carousel-control-prev custom-control" href="#services-slider" role="button" data-slide="prev">
                            <i className="fa fa-chevron-left fa-lg text-muted"></i>
                            <span className="sr-only">Previous</span>
                        </a>
                        <a className="carousel-control-next text-faded custom-control" href="#services-slider" role="button" data-slide="next">
                            <i className="fa fa-chevron-right fa-lg text-muted"></i>
                            <span className="sr-only">Next</span>
                        </a>
                    </div>
                </div>

                <div className="carousel-custom d-flex justify-content-center" >
                    <div className="carousel-item carousel-item-custom col-md-3 active bottom-space">
                        <div className="folded-corner service_tab_1 folded-corner-rounded">
                            <Link to={{ pathname: links[39].url}} title={links[39].title} target="_blank">
                                <div className="text">
                                    <img className='img-thumbnail rounded mx-auto d-block img-noborder' alt='img-visacion'
                                        src={Config[2].url + '/static/img/visacion.png'} />
                                </div>
                            </Link>
                        </div>
                        <a href={links[39].url} title={links[39].title} className="item-title link-folded" target="_blank">{links[39].title}</a>
                    </div>
                </div>

            </div>

            <ModalRedesSociales urlDestiny={'localhost'} links={links} />
        </div>;
    }
}

export default Features;