import React, { Component } from 'react';

import ShowCase from './../../components/Home/ShowCase';
import Features from './../../components/Home/Features';
import Services from './../../components/Home/Services';
//import UnderContruction from '../utils/UnderContruction';
import News from '../utils/News';
//import CookieMessage from '../utils/CookieMessage';
//import Whattsapp from '../utils/Whattsapp';
import links from '../../data/link';

class Home extends Component {

    constructor(props) {
        super(props);
        //this.underContrucction = new UnderContruction()
        //this.news = new News()
    }

    componentDidMount() {
        //window.jQuery("#modalFormUnderContruction").modal("show")
        window.jQuery("#modalFormNews").modal("show")
        window.scrollTo(0, 0);
    }
    /*
        constructor(props, context) {
            super(props, context);    
            this.state = {
                showMessageVersion: false,
            };
        }
    
        componentDidMount() {
            debugger
    
            //mobile
            var detector = new window.MobileDetect(window.navigator.userAgent)
            if (Boolean(detector.mobile())) {
    
                detector.mobile()
                detector.userAgent()
                detector.os()
                detector.versionStr('Build')
    
            } else {
                var browserInfo = window.getBrowserInfo()
    
                if (Boolean(browserInfo)) {
                    var version_browser = browserInfo.browser.split('-')
                    if (version_browser.length > 1) {
                        //Chrome
                        if (browserInfo.browser.indexOf('Chrome') >= 0) {
                            let version_chrome = parseInt(version_browser[1])
    
                            if (version_chrome < 49) 
                                this.setState({ showMessageVersion: true })
                        }
    
                        console.log("Navegador ++++++++++++ ", browserInfo.browser);
                        //firefox
                        if (browserInfo.browser.indexOf('Firefox') >= 0) {
                            let version_firefox = parseInt(version_browser[1])
    
                            if (version_firefox < 68) 
                                this.setState({ showMessageVersion: true })
                        }
    
                        //interner explorer
                        if (browserInfo.browser.indexOf('IE') >= 0) {
                            let version_ie = parseInt(version_browser[1])
    
                            if (version_ie < 11) 
                                this.setState({ showMessageVersion: true })
                        }
    
                        //Edge
                        if (browserInfo.browser.indexOf('Edge') >= 0) {
                            let version_edge = parseInt(version_browser[1])
    
                            if (version_edge < 17) 
                                this.setState({ showMessageVersion: true })
                        }
    
                        //Safari
                        if (browserInfo.browser.indexOf('Safari') >= 0) {
                            let version_safari = parseInt(version_browser[1])
    
                            if (version_safari < 5) 
                                this.setState({ showMessageVersion: true })
                        }
    
                    }
                }
            }
        }
    */
    render() {
        return (
            <>
                <ShowCase />
                <Features links={links} />
                <Services />

                {/* <UnderContruction /> */}
                <News />
                {/*<CookieMessage />*/}
                {/*<Whattsapp />*/}
            </>
        );
    }
}

export default Home;