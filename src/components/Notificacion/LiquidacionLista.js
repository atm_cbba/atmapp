import React, { useState, useRef, useEffect } from 'react';
import Fetch from '../utils/Fetch';
import Constant from '../../data/constant';
import Config from '../../data/config';
import AuthService from '../Usuario/AuthService';
import Links from '../../data/link';
import TitlePage from '../utils/TitlePage';
import Texto from '../../data/es';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import ModalPdfBasic from '../utils/ModalPdfBasic';

let textSearch = ""
const getParamRequest = (param) => {

    param = param || ""

    switch (param) {
        case "vehiculos": {
            return "Vehículos"
            break;
        }
        case "inmuebles": {
            return "Inmuebles"
            break;
        }
        case "mercados": {
            return "Mercados"
            break;
        }
        case "publicidad_exterior": {
            return "Publicidad Exterior"
            break;
        }
        case "actividades_economicas": {
            return "Actividades Económicas"
            break;
        }
        default:{
            return ""
            break;
        }
    }
}

const getParamRequestCode = (param) => {
    param = param || ""
    switch (param) {
        case "vehiculos": {
            textSearch = "Ingresa el Nro. de placa del vehículo/nombre o la razón social del vendedor"
            return "VEHICULOS"
            break;
        }
        case "inmuebles": {
            textSearch = "Ingresa el Nro. número de inmueble/nombre/la razón social"
            return "INMUEBLES"
            break;
        }
        case "mercados": {
            textSearch = "Ingresa el Código de Licencia o la razón social"
            return "MERCADOS"
            break;
        }
        case "publicidad_exterior": {
            textSearch = "Ingresa el Código de Licencia o la razón social"
            return "PUBLICIDAD_EXTERIOR"
            break;
        }
        case "actividades_economicas": {
            textSearch = "Ingresa el Nro. de Actividad/Nro. de Licencia/nombre o razón social"
            return "ACTIVIDADES_ECONOMICAS"
            break;
        }
        default: {
            return "";
            break;
        }
    }
}

const param_request = window.getParameterByName("proceso")
let breadcrumbs = [
    {
        title: Links[0].title,
        url: Links[0].url
    },
    {
        title: Links[33].title,
        url: Links[33].url
    },
    {
        title: Links[34].title + " " + getParamRequest(param_request),
        url: Links[34].url
    }
];

const LiquidacionLista = (props) => {

    const constant = Constant[0];
    const fetch = new Fetch();
    fetch.setToast(toast);
    const auth = new AuthService();

    const refModalPdfBasic = useRef();
    const [notificacionesList, setNotificacionesList] = useState("")
    const [totalList, setTotalList] = useState(0)
    const [currentPage, setCurrentPage] = useState(1)
    const [resultSearch, setResultSearch] = useState("")

    const titlePage = Texto.liquidaciones_mixtas + " " + getParamRequest(param_request)

    useEffect(() => {
        let departament = getParamRequestCode(param_request)

        getDataLiquidacion('api/fiscalizacion/mixtas/' + departament)
    }, []);

    const handleOpenFile = (event, id) => {
        event.preventDefault()
        refModalPdfBasic.current.openModal(true, `legal-tributario/document/${id}`, `legal-tributario/document-download/${id}`)
    }

    const handleDownladFile = (event, id) => {
        event.preventDefault()
        window.location.href = Config[0].url + `legal-tributario/document-download/${id}`;
    }

    const hanldeClickReset = (event) => {
        event.preventDefault()
        event.target.closest("div").parentElement.firstElementChild.value = ""
    }

    const hanldeClickSearch = (event) => {
        event.preventDefault()
        let search = event.target.closest("div").parentElement.firstElementChild.value.trim()
        search = search.replace(/ /g, "")
        if (search !== "") {
            window.jQuery.preloader.start();
            let departament = getParamRequestCode(param_request)
            dataSearchRecursive(search, departament, currentPage)
        } else {
            toast.warn("Criterio de busqueda no de valido", {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true
            });
        }
    }

    const getDataLiquidacion = (url, callback) => {
        fetch.fetchGet(url).then(dataJson => {
            if (dataJson !== undefined && dataJson.status === true) {
                if (Boolean(dataJson.Notificacion)) {
                    fetch.toast.success(dataJson.message, {
                        position: "top-right",
                        autoClose: 5000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true
                    });

                    let cont = 0
                    let row_css = 'row-purple'
                    let notificaciones = dataJson.Notificacion.map((item, index) => {
                        return <div className={"row pt-3 pb-2 " + (cont % 2 === 0 ? 'row-purple' : '')} key={cont++} style={{ marginLeft: 0, marginRight: 0 }}>
                            <div className="col-12 col-md-2 col-lg-2 text-center pr-0 pt-1 pb-2">
                                <i className={"fa fa-file-pdf-o fa-3x fa-icon-image d-block " + (cont % 2 === 0 ? '' : 'text-white')} title={item.title} ></i>
                                <small className="feature-box__text text-center d-block pt-1">{window.moment(item.created_at).format("DD-MM-YYYY HH:mm:ss")}</small>
                                {item.doc_name.indexOf('a parte') > 0 ?
                                    <strong className="feature-box__text text-center text-overflow">{item.doc_name}</strong>
                                    : ""}
                            </div>

                            <div className="col-12 col-md-8 col-lg-8 pt-1 pb-2">
                                <p className="feature-box__text text-justify pb-0 mb-0 text-overflow"><strong>Nombre:</strong> {item.title}</p>
                                <p className="feature-box__text text-justify pb-0 mb-0" style={{ display: 'flex', justifyContent: 'space-between' }}>
                                    <span className="d-inline-block" ><strong>Formato:</strong> {item.doc_content_type} </span>
                                    <span className="d-inline-block" style={{ width: "40%" }}><strong>Tamaño:</strong> {item.doc_size} KB</span>
                                </p>
                                <p className="feature-box__text text-justify pb-0 mb-0"><strong>Descripción:</strong> {item.description}</p>
                            </div>

                            <div className="col-12 col-md-2 col-lg-2 text-center pt-1 pb-2">
                                <i className={"fa fa-file-pdf-o fa-2x fa-icon-image pr-3 pointer " + (cont % 2 === 0 ? '' : 'text-white')} onClick={(e) => handleOpenFile(e, item.doc_id)} id={item.doc_id}></i>
                                <i className={"fa fa-download fa-2x fa-icon-image  pointer " + (cont % 2 === 0 ? '' : 'text-white')} onClick={(e) => handleDownladFile(e, item.doc_id)} id={item.doc_id}></i>
                            </div>
                        </div>
                    })
                    setNotificacionesList(notificaciones)
                    if (dataJson.total_mixtas !== undefined && dataJson.total_mixtas > 0) {
                        setTotalList(dataJson.total_mixtas)
                    }
                    if (callback !== undefined)
                        callback(dataJson.Notificacion, dataJson.total_coincidencias_econtradas)
                } else {
                    if (callback !== undefined)
                        callback(undefined, 0)
                    fetch.toast.warn(dataJson.message, {
                        position: "top-right",
                        autoClose: 5000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true
                    });
                }
            }

            if (callback === undefined)
                window.jQuery.preloader.stop();
        })
    }

    const dataSearchRecursive = (search, departament, page) => {
        getDataLiquidacion('api/fiscalizacion/search/' + search + "/" + departament + "/" + page + "/10", function (notificacion, total_encontrados) {
            if ((notificacion.length === 0 || notificacion === undefined) && (page + 10 <= totalList)) {
                dataSearchRecursive(search, departament, ++page)
                setCurrentPage(page)
            } else
                window.jQuery.preloader.stop();
                setResultSearch("Se han encontrado un total de "+total_encontrados+" coincidencias, en los archivos siguientes.")
        })
    }

    return (
        <div className="paddingTop30" id="features" >
            <TitlePage titlePage={titlePage} breadcrumbs={breadcrumbs} position={'left'} leftfull={false} />

            <div className="container features "  >
                <div className="row">
                    <div className="col-12 col-md-4 col-lg-6">
                    </div>

                    <div className="col-12 col-md-8 col-lg-6">
                    <label htmlFor="inputSearch">{textSearch}</label>
                        <div className="input-group mb-3 form-group">
                            <input type="text" id="inputSearch" className="form-control input-uppercase" placeholder="Buscar" aria-label="Buscar" aria-describedby="Buscar" name="buscar" />
                            <div className="input-group-append">
                                <button className="btn btn-outline-secondary" type="button" onClick={hanldeClickSearch}><i className="fa fa-search" aria-hidden="true" ></i></button>
                                <button className="btn btn-outline-secondary" type="button" onClick={hanldeClickReset}><i className="fa fa-times" aria-hidden="true" ></i></button>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="row mb-2">
                    <div className="col-12">
                        {resultSearch}
                    </div>
                </div>

                {notificacionesList}

                <div className="row">
                    <ModalPdfBasic ref={refModalPdfBasic} />
                </div>

                <ToastContainer enableMultiContainer containerId={'Z'}
                    position="top-right"
                    autoClose={5000}
                    hideProgressBar={false}
                    newestOnTop={false}
                    closeOnClick
                    rtl={false}
                    pauseOnVisibilityChange
                    draggable
                    pauseOnHover
                />
                <ToastContainer />
            </div>
        </div>
    )
}

export default LiquidacionLista;