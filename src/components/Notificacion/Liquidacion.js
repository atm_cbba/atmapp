import React, { useEffect } from 'react';
import Fetch from '../utils/Fetch';
import Constant from '../../data/constant';
import Config from '../../data/config';
import AuthService from '../Usuario/AuthService';
import Links from '../../data/link';
import TitlePage from '../utils/TitlePage';
import Texto from '../../data/es';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

let breadcrumbs = [
    {
        title: Links[0].title,
        url: Links[0].url
    },
    {
        title: Links[33].title,
        url: Links[33].url
    }
];

const Liquidacion = (props) => {

    const constant = Constant[0];
    const fetch = new Fetch();
    fetch.setToast(toast);
    const auth = new AuthService();

    const titlePage = Texto.liquidaciones_mixtas
    const profile = auth.getProfile();

    useEffect(() => {
        window.scrollTo(0, 0);
    }, []);

    const handleRedirectUrlClick = (ev, url) => {
        ev.preventDefault();
        window.location.href = url;
    }

    return (
        <div className="paddingTop30" id="features" >
            <TitlePage titlePage={titlePage} breadcrumbs={breadcrumbs} position={'left'} leftfull={false} />

            <div className="container features " style={{ marginTop: '-30px' }} >

                <div className="row">
                    <div className="col-sm">
                        <div className="blog-item-box">
                            <figure className="blog-item pointer" style={{ background: 'transparent' }} onClick={e => handleRedirectUrlClick(e, Links[34].url + '?proceso=vehiculos')}>
                                <div className="image">
                                    <img className='img-thumbnail rounded mx-auto d-block img-noborder' alt='img-consulta-tu-deuda'
                                        src={Config[2].url + '/static/img/bt_vehiculo.png'} />
                                    <i className="fa fa-clock-o" aria-hidden="true"></i>
                                    <div className="date"><span className="day fa fa-gavel" style={{ fontSize: 30 }}></span></div>
                                </div>
                                <figcaption>
                                    <h3 className="text-overflow">{Texto.vehiculos}</h3>
                                    <p className="text-justify" style={{ minHeight: 70 }}>IMPUESTO MUNICIPAL A LA PROPIEDAD DE VEHICULOS AUTOMOTORES <br /> IMPUESTO MUNICIPAL A LA TRANSFERENCIA</p>
                                    <a href={Links[34].url + '?proceso=vehiculos'} title={Links[34].title} className="read-more">Leer mas</a>
                                </figcaption>
                            </figure>
                        </div>
                    </div>

                    <div className="col-sm">
                        <div className="blog-item-box">
                            <figure className="blog-item pointer" style={{ background: 'transparent' }} onClick={e => handleRedirectUrlClick(e, Links[34].url+ '?proceso=inmuebles')}>
                                <div className="image">
                                    <img className='img-thumbnail rounded mx-auto d-block img-noborder' alt='img-consulta-tu-deuda'
                                        src={Config[2].url + '/static/img/bt_inmuebles.png'} />

                                    <i className="fa fa-clock-o" aria-hidden="true"></i>

                                    <div className="date"><span className="day fa fa-gavel" style={{ fontSize: 30 }}></span></div>
                                </div>
                                <figcaption>
                                    <h3 className="text-overflow">{Texto.inmuebles}</h3>
                                    <p className="text-justify" style={{ minHeight: 70 }}>IMPUESTO MUNICIPAL A LA PROPIEDAD DE BIENES INMUEBLES e IMPUESTO MUNICIPAL A LA TRANSFERENCIA</p>
                                    <a href={Links[34].url + '?proceso=inmuebles'} title={Links[34].title} className="read-more">Leer mas</a>
                                </figcaption>
                            </figure>
                        </div>
                    </div>

                    <div className="col-sm">
                        <div className="blog-item-box">
                            <figure className="blog-item pointer" style={{ background: 'transparent' }} onClick={e => handleRedirectUrlClick(e, Links[34].url+ '?proceso=mercados')}>
                                <div className="image">
                                    <img className='img-thumbnail rounded mx-auto d-block img-noborder' alt='img-consulta-tu-deuda'
                                        src={Config[2].url + '/static/img/bt_mercados.png'} />

                                    <i className="fa fa-clock-o" aria-hidden="true"></i>
                                    <div className="date"><span className="day fa fa-gavel" style={{ fontSize: 30 }}></span></div>
                                </div>
                                <figcaption>
                                    <h3 className="text-overflow">{Texto.sitios_mercados}</h3>
                                    <p className="text-justify" style={{ minHeight: 70 }}>PATENTE MUNICIPAL DE SITIOS Y MERCADOS (PATENTE UNICA ANUAL - PUM) </p>
                                    <a href={Links[34].url + '?proceso=mercados'} title={Links[34].title} className="read-more">Leer mas</a>
                                </figcaption>
                            </figure>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-sm">
                        <div className="blog-item-box">
                            <figure className="blog-item pointer" style={{ background: 'transparent' }} onClick={e => handleRedirectUrlClick(e, Links[34].url+ '?proceso=publicidad_exterior')}>
                                <div className="image">
                                    <img className='img-thumbnail rounded mx-auto d-block img-noborder' alt='img-consulta-tu-deuda'
                                        src={Config[2].url + '/static/img/bt_publicidad_exterior.png'} />
                                    <i className="fa fa-clock-o" aria-hidden="true"></i>

                                    <div className="date"><span className="day fa fa-gavel" style={{ fontSize: 30 }}></span></div>
                                </div>
                                <figcaption>
                                    <h3 className="text-overflow">{Texto.publicidad_exterior}</h3>
                                    <p className="text-justify" style={{ minHeight: 70 }}>PATENTE MUNICIPAL DE PUBLICIDAD EXTERIOR(PATENTE UNICA ANUAL - PUM) </p>
                                    <a href={Links[34].url + '?proceso=publicidad_exterior'} title={Links[34].title} className="read-more">Leer mas</a>
                                </figcaption>
                            </figure>
                        </div>
                    </div>

                    <div className="col-sm">
                        <div className="blog-item-box">
                            <figure className="blog-item pointer" style={{ background: 'transparent' }} onClick={e => handleRedirectUrlClick(e, Links[34].url+ '?proceso=actividades_economicas')}>
                                <div className="image">
                                    <img className='img-thumbnail rounded mx-auto d-block img-noborder' alt='img-consulta-tu-deuda'
                                        src={Config[2].url + '/static/img/bt_actividad_economica.png'} />
                                    <i className="fa fa-clock-o" aria-hidden="true"></i>

                                    <div className="date"><span className="day fa fa-gavel" style={{ fontSize: 30 }}></span></div>
                                </div>
                                <figcaption>
                                    <h3 className="text-overflow">{Texto.actividades_economicas}</h3>
                                    <p className="text-justify" style={{ minHeight: 70 }}>PATENTE MUNICIPAL DE ACTIVIDADES ECONOMICAS (PATENTE UNICA ANUAL - PUM) </p>
                                    <a href={Links[34].url + '?proceso=actividades_economicas'} title={Links[34].title} className="read-more">Leer mas</a>
                                </figcaption>
                            </figure>
                        </div>
                    </div>

                    <div className="col-sm">
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Liquidacion;