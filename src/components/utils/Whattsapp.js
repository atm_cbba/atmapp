

import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class Whattsapp extends Component {

    constructor(props, context) {
        super(props, context);
        this.handleOnClickWhatsapp = this.handleOnClickWhatsapp.bind(this);
        this.state = { showNumberWhatsapp: false };
    }

    handleOnClickWhatsapp(event) {
        event.preventDefault()
    }

    render() {
        return (
            <div className="whatsapp-icon">
                <Link to="#" onClick={this.handleOnClickWhatsapp}   >
                    <img src="static/img/whatsapp.png" alt='whatsap' />
                </Link>

                <div className="hover-text" style={{ display: 'none' }}>
                    <div className="float-left">
                        <div className="mb-3">
                            Dpto. L. Trib. <br />
                            <Link to={{ pathname: "https://api.whatsapp.com/send?phone=59179956377" }} target="_blank"  >
                                79956377
                            </Link>
                        </div>
                        <div className="mb-3">
                            Agente 1 <br />
                            <Link to={{ pathname: "https://api.whatsapp.com/send?phone=59179957716" }} target="_blank"  >
                                79957716
                            </Link>
                        </div>
                    </div>

                    <div className="float-right">
                        <div className="mb-3">
                            Dpto. Fisc. <br />
                            <Link to={{ pathname: "https://api.whatsapp.com/send?phone=59179961140" }} target="_blank"  >
                                79961140
                            </Link>
                        </div>
                        <div className="mb-3">
                            Agente 2 <br />
                            <Link to={{ pathname: "https://api.whatsapp.com/send?phone=59179957084" }} target="_blank"  >
                                79957084
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}



