import React, { Component } from 'react';

class Contact extends Component {

    constructor(props, context) {
        super(props, context)
        this.id_modal = "modalFormUnderContruction"
    }

    render() {
        return (
            <div className="modal fade " id={this.id_modal} tabIndex="-1" role="dialog" aria-labelledby="contactModalCenterTitle" aria-hidden="true" data-backdrop="static" data-keyboard="false">
                <div className="modal-dialog modal-lg" >
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" >Web en Construcción</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <div className="row">
                                <div className="col-12">
                                    <img src={'../static/img/web_contruccion.jpg'} alt="Web en Contrucción" title="Web en Contrucción" className="img-fluid" />
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-12">
                                    <p>Se recomienda no realizar operaciones mientras este visible esta nota</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Contact;
