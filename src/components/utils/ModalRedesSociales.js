import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

class ModalRedesSociales extends Component {

    constructor(props, context) {
        super(props, context);
        this.handleRedirectUrlClick = this.handleRedirectUrlClick.bind(this);
    }

    static propTypes = {
        links: PropTypes.array.isRequired
    }

    handleRedirectUrlClick(ev, url) {
        window.location.href = url;
    }

    render() {
        //const { links } = this.props;

        return (

            <div className="modal fade" id="modalRedesSociales" tabIndex="-1" role="dialog" aria-labelledby="movalRedesSocialesLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
                <div className="modal-dialog modal-lg" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="movalRedesSocialesLabel">Redes Sociales</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <div className="row">
                                <div className="col-12 col-md-4 col-lg-4 ">
                                    <img src="static/img/whatsapp.png" alt='whatsap' />
                                     <br/> <br/> 
                                    <span>Agente 1 </span>  
                                    <Link to={{ pathname: "https://api.whatsapp.com/send?phone=79957084" }} target="_blank"  >
                                        79957716
                                    </Link>
                                    <br /><br />
                                    <span>Agente 2 </span> 
                                    <Link to={{ pathname: "https://api.whatsapp.com/send?phone=79957084" }} target="_blank"  >
                                        79957084
                                    </Link>
                                </div>

                                <div className="col-12 col-md-4 col-lg-4 ">

                                </div>

                                <div className="col-12 col-md-4 col-lg-4 ">

                                </div>
                            </div>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary pull-right" data-dismiss="modal" >Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default ModalRedesSociales;