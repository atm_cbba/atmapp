import React, { Component } from 'react';
import { withStuffFunction } from '../utils/WithStuff'

class FullScreenModal extends Component {

    modalCategoriaRef = React.createRef()
    constructor(props) {
        super(props);

        this.handleCloseModal = this.handleCloseModal.bind(this)
        this.handleOpenActividadesEconomicas = this.handleOpenActividadesEconomicas.bind(this)
        this.handleOpenVideo = this.handleOpenVideo.bind(this)
        this.state = {};

        //const {clickShowActividad, clickShowVideo} = this.props
    }

    componentDidMount() {

    }

    handleCloseModal(event) {
        this.openModal(null, false)
    }

    handleOpenActividadesEconomicas(event) {
        this.openModal(null, false)
        this.props.clickShowActividad()
    }

    handleOpenVideo(event){
        this.openModal(null, false)
        this.props.clickShowVideo()
    }

    openModal = (event, open) => {
        let modal = this.modalCategoriaRef.current
        if (open) {
            modal.classList.add("uk-open")
        } else {
            modal.classList.remove("uk-open")
        }
    }
    render() {
        //clickShowActividad={handleClickShowActividadEconomica} clickShowVideo={handleClickShowVideo}

        return <div id="categories-modal" className="uk-modal background-white" uk-modal="" ref={this.modalCategoriaRef} >
            <div className="uk-modal-dialog">
                <div className="modal-header">
                    <strong className="title-modal"> Categorías</strong>
                    <div className="cat-dot "></div>
                    <button className="uk-close uk-icon" type="button" onClick={this.handleCloseModal}>
                        <i className="fa fa-times" aria-hidden="true"></i>
                    </button>
                </div>
                <div className="uk-container " >
                    <ul className="uk-nav mediatype-nav">
                        <li className="left-nav--category ">
                            <a href="#" className="text-secondary" onClick={this.handleOpenActividadesEconomicas}>Actividades Económicas</a>
                        </li>
                        <li className="left-nav--category ">
                            <a href="#" className="text-secondary" onClick={this.handleOpenVideo}>Videos</a>
                        </li>
                    </ul>
                </div>
            </div>
            <button className="fab-close uk-icon uk-icon-button" onClick={this.handleCloseModal}>
                <i className="fa fa-times" aria-hidden="true"></i>
            </button>
        </div>
    }
}

export default withStuffFunction(FullScreenModal);



