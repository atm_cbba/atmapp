export default class PrescripcionDTO {
    constructor() {
        this.token = ""
        this.fur = ""
        this.contribuyente = ""
        this.prescripcion = ""
        this.persona = ""
        this.personaRL = ""
        this.derecho_admision = 0
        this.nitRL = ""
    }

    setToken(token_dj) {
        this.token = token_dj
    }

    getToken() {
        return this.token
    }

    setFur(fur){
        this.fur = fur
    }

    getFur(){
        return this.fur
    }

    setPrescripcion(act_eco){
        this.prescripcion = act_eco
    }

    getPrescripcion(){
        return this.prescripcion
    }

    setContribuyente(contribuyente){
        this.contribuyente = contribuyente
    }

    getContribuyente(){
        return this.contribuyente
    }

    setPersona(persona){
        this.persona = persona
    }

    getPersona(){
        return this.persona
    }

    setPersonaRL (personaRL){
        this.personaRL = personaRL
    }

    getPersonaRl(){
        return this.personaRL
    }

    setNitRL (nitRL){
        this.nitRL = nitRL
    }

    getNitRl(){
        return this.nitRL
    }

    setDerechoAdmision(derecho_admision){
        this.derecho_admision = derecho_admision
    }

    getDerechoAdmision(){
        return this.derecho_admision
    }
}
