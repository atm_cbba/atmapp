import React, { Component } from 'react';

class News extends Component {

    constructor(props, context) {
        super(props, context)
        this.id_modal = "modalFormNews"
    }

    handleRedirectUrlClick(ev, url) {
        window.open(url, '_blank');
    }

    render() {
        return (
            <div className="modal fade " id={this.id_modal} tabIndex="-1" role="dialog" aria-labelledby="contactModalCenterTitle" 
            aria-hidden="true"  data-keyboard="false">
                <div className="modal-dialog modal-md" >
                    <div className="modal-content">
                        {/*<div className="modal-header">
                            <h5 className="modal-title text-danger" >IMPORTANTE</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>*/}
                        <div className="modal-body">
                            <div className="row">
                                <div className="col-12 ">
                                    <a href="#" className="close-custom" data-dismiss="modal" aria-label="Close"><i className="fa fa-times" aria-hidden="true"></i></a>
                                    <img src={'../static/img/comunicado.jpg'} alt="Trámite de Visación de Minutas" title="Trámite de Visación de Minutas" className="img-fluid pointer"  
                                    onClick={e => this.handleRedirectUrlClick(e, "https://atmapi.cochabamba.bo/restServerAtm/legal-tributario/document/37")} width="100%"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default News;
