import React, { Component } from 'react';
import { Link } from 'react-router-dom';
//import '../style/cookie.css';


export default class CookieMessage extends Component {

    constructor(props, context) {
        super(props, context);

        this.handleAcceptCookie = this.handleAcceptCookie.bind(this);
        //this.handleConfirmOnClick = this.handleConfirmOnClick.bind(this);

        this.state = { showCookie: true };
    }

    componentDidMount() {

        var cookie_preference = window.leerCookie('cookie_preference');
        this.setState({ showCookie: cookie_preference !== null ? false : true })
    }

    handleAcceptCookie(event) {
        var current_date = new Date();
        current_date.setHours(current_date.getHours() + 1);
        var today = new Date(current_date).toUTCString();
        document.cookie = "cookie_preference=1; expires=" + today.toString() + "; path=/";

        this.setState({ showCookie: false })
    }
    render() {
        const { titlePage, breadcrumbs } = this.props

        return (
            <>
                {

                    this.state.showCookie === true ?
                        <div id="cookie_policy_popup" style={{ display: 'block' }} >

                            <div className="cookie-icon">
                                <img src="static/img/cookie.png" />
                            </div>
                            <p className="cookie_popup_text"> para el correcto funcionamiento del sitio
                                Utilizamos cookies que son esenciales para su uso de las funciones del sitio. Al utilizarlos, acepta el uso de cookies..
                            </p>
                            <p className="cookie_popup_buttons">
                                <a className="btn btn-xsmall btn-ghost btn-secondary" href="#" target="_blank" rel="noopener">Más información</a>
                                <Link to="#" onClick={this.handleAcceptCookie} className="btn btn-xsmall btn-plain btn-secondary right ok-cookie"  >
                                    Aceptar
                                </Link>
                            </p>
                        </div>

                        :
                        ""
                }
            </>
        )
    }
}
//const CookieMessage = ({ titlePage, breadcrumbs }) => {
/*<div id="block-search">
<form className="form-inline" >
    <label className="sr-only" htmlFor="inputSearchCI">CI</label>

    <select className="form-control" id="declaracion_jurada[id_actividad_economica]" name="declaracion_jurada[id_actividad_economica]"
        >
        <option defaultValue>CI</option>
        <option key={1} value={1}>Nit</option>
    </select>

    <label className="sr-only" htmlFor="inputSearchCiNit">Ci/Nit</label>
    <div className="input-group mb-2 mr-sm-2">
        <div className="input-group-prepend">
            <div className="input-group-text"> <i className="fa fa-address-card-o" aria-hidden="true"></i></div>
        </div>
        <input type="text" className="form-control" name="inputSearchCiNit" placeholder="Buscar por CI" />
    </div>

    <input name="submit" type="submit" className="button-style" value="Buscar" />
</form>
</div>*/

//};
/*
TitlePage.prototype={
    titlePage: PropTypes.string.isRequired,
    url: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired
}*/

//export default CookieMessage;