import React, { Component } from 'react';
import styled, { keyframes } from 'styled-components';
//import { storiesOf } from '@storybook/react';

const rotate360 = keyframes`
  from {
    transform: rotate(0deg);
  }
  to {
    transform: rotate(360deg);
  }
`;

const Spinner = styled.div`
  margin: 14px;
  animation: ___CSS_0___ 1s linear infinite;
  transform: translateZ(0);
  border-top: 2px solid grey;
  border-right: 2px solid grey;
  border-bottom: 2px solid grey;
  border-left: 4px solid black;
  background: transparent;
  width: 60px;
  height: 60px;
  border-radius: 50%;
  animation-name: ${rotate360};
`;
/*
const CustomLoader = () => (
    <div style={{ padding: '24px' }}>
        <Spinner />
    </div>
);
*/


export default class CustomLoader extends Component {
    render() {
        return (
            <div style={{ padding: '20px' }}>
                <Spinner />
            </div>
        )
    }
}
/*
storiesOf('Progress Indicator', module)
  .add('Custom', () => <CustomLoader />);
  */