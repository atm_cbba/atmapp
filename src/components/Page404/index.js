import React, { Component } from 'react';
import Links from '../../data/link';
import { Link } from 'react-router-dom';
import Texto from '../../data/es';

class Page404 extends Component {

    constructor(props) {
        super(props);
        this.handleContactanosClick = this.handleContactanosClick.bind()

        this.state = {};
    }

    handleContactanosClick(event) {
        event.preventDefault()
        window.jQuery("#modalFormContacts").modal("show")
      }

    componentDidMount() {
        if (window.location.pathname === Links[6].url) {  //'/declaracion-jurada-edit'
            window.location.reload();
        }
    }

    render = () => {
        return <div id="services" className="paddingTop30" >
            <div className="container">
                <div className="row">
                    <div className="col-md-12">
                        <div className="error-template">
                            <h1> Oops!</h1>
                            <h2>{Texto.page_not_found}</h2>
                            <div className="error-details">
                                {Texto.ha_ocurrido_un_error_pagina_no_encontrada}
                            </div>
                            <div className="error-actions">
                                <Link to={{ pathname: Links[0].url }} className="button-style showcase-btn" ><span className="glyphicon glyphicon-home"></span> Inicio </Link>
                                <Link to={'#'} className="button-style showcase-btn"  onClick={this.handleContactanosClick}><span className="glyphicon glyphicon-envelope"></span> Contactanos </Link>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    }
}

export default Page404;