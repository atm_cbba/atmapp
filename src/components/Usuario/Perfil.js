import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import TitlePage from '../../components/utils/TitlePage';
import AsyncSelect from 'react-select/async';

import Fetch from '../../components/utils/Fetch';
import Links from '../../data/link';
import Languaje from '../../data/es';
import AuthService from '../../components/Usuario/AuthService';
import DatePicker, { registerLocale } from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";  //import datepicker from "react-datepicker/dist/react-datepicker.css";
import Config from '../../data/config';
import DataTable from 'react-data-table-component';
import styled from 'styled-components';  //styled for data table
import '../style/input-file-drag.css';
import '../style/parsley.css';

const columns = [
    {
        name: Languaje.creado,
        selector: 'created_at',
        sortable: true,
        maxWidth: '150px'
    },

    {
        name: Languaje.ip,
        center: false,
        sortable: true,
        maxWidth: '170px',
        cell: row => <div>
            {(JSON.parse(row.data_new)).ip}
        </div>
    },
    {
        name: Languaje.dispositivo,
        center: false,
        sortable: true,
        hide: 'sm',
        cell: row => <div>
            {(JSON.parse(row.data_new)).device}
        </div>
    },
    {
        name: Languaje.navegador,
        center: false,
        sortable: true,
        hide: 'sm',
        cell: row => <div>
            {(JSON.parse(row.data_new)).navegador}
        </div>
    },
    {
        name: Languaje.action,
        selector: 'action',
        sortable: true,
        hide: 'md',
        maxWidth: '50px'
    },
];

const detector = new window.MobileDetect(window.navigator.userAgent)
const columnsActivity = [
    {
        name: Languaje.creado,
        selector: 'created_at',
        sortable: true,
        maxWidth: '150px'
    },

    {
        name: Languaje.description,
        center: false,
        sortable: true,
        cell: row => <>
            {detector.mobile() 
            ? 
            (window.innerWidth < 768 ?  row.description.substring(0, 16)+'...' : 
            (window.innerWidth >= 768 && window.innerWidth < 1024 ?  row.description.substring(0,60)+'...' : row.description ) )
            : 
            row.description }
        </>
    }
];


const DivExpandedStyle = styled.div`
  padding: 10px;
  display: block;
  width: 100%;

  p {
    font-size: 12px;
    word-break: break-all;
  }
`;

const HistoryExpandedComponent = ({ data }) => (
    <DivExpandedStyle>
        <p><strong>{Languaje.creado}: </strong>{data.created_at}</p>
        <p><strong>{Languaje.ip}: </strong>{(JSON.parse(data.data_new)).ip}</p>
        <p><strong>{Languaje.navegador}: </strong>{(JSON.parse(data.data_new)).navegador}</p>
        <p><strong>{Languaje.action}: </strong>{data.action}</p>
        <p><strong>{Languaje.dispositivo}: </strong>{(JSON.parse(data.data_new)).device}</p>
    </DivExpandedStyle>
);

const ActivityExpandedComponent = ({ data }) => (
    <DivExpandedStyle>
        <p><strong>{Languaje.creado}: </strong>{data.created_at}</p>
        <p><strong>{Languaje.description}: </strong>{data.description}</p>
    </DivExpandedStyle>
);

var _usuario = []
var session_usuario = null
var fetch = null
class Perfil extends Component {

    constructor(props, context) {
        super(props, context);

        this.handlePerfilInformatcionClick = this.handlePerfilInformatcionClick.bind(this)
        this.handlePerfilEditClick = this.handlePerfilEditClick.bind(this)
        this.handlePerfilPictureClick = this.handlePerfilPictureClick.bind(this)
        this.handleChangePasswordClick = this.handleChangePasswordClick.bind(this)

        this.hanldePerfilClick = this.hanldePerfilClick.bind(this)
        this.onSubmitFormDatos = this.onSubmitFormDatos.bind(this)
        this.onSubmitFormChangePassword = this.onSubmitFormChangePassword.bind(this)
        this.onSubmitFormImage = this.onSubmitFormImage.bind(this)

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleDatePickerChange = this.handleDatePickerChange.bind(this);

        this.fetch = new Fetch();
        this.fetch.setToast(toast)

        fetch = this.fetch

        this.Auth = new AuthService();

        this.state = {
            _usuario: [],
            showPerfil: true,
            showHistorySession: false,
            showActivity: false,
            optionsSelect: [],
            startDate: null,
            totalRows: 0,
            perPage: 10,
            totalRowsActivity: 0,
            data: [],
            loading: false,
            dataActivity: [],
            loadingActivity: false
        };

        this.titlePage = Languaje.perfil
    }

    componentDidMount() {
        if (this.Auth.loggedIn()) {
            this.getDatausuario()
        } else
            this.props.history.replace(Links[4].url)
    }

    componentDidUpdate() {
        window.jQuery(".sc-kAzzGY").remove()  //pertenece al datatable
    }

    hanldePerfilClick(event) {
        event.preventDefault()
        this.setState({ showPerfil: true, showHistorySession: false, showActivity: false })
        this.getDatausuario()
    }

    handleLoginHistory = async (event) => {
        event.preventDefault()
        this.setState({
            loading: true,
            showPerfil: false,
            showHistorySession: true,
            showActivity: false
        });

        const response = await this.fetch.axiosAsyncGet(`api/log/history-login-by-token-user/1/${this.state.perPage}/${session_usuario.token}`);
        if (response !== null && response.status === true) {
            this.setState({
                data: response.Log,
                totalRows: response.total,
                loading: false,
            });
        }
    }

    handleActivity = async (event) => {
        event.preventDefault();

        this.setState({
            loadingActivity: true,
            showPerfil: false,
            showHistorySession: false,
            showActivity: true
        });

        const response = await this.fetch.axiosAsyncGet(`api/log/activity-by-token-user/1/${this.state.perPage}/${session_usuario.token}`);
        if (response !== null && response.status === true) {
            this.setState({
                dataActivity: response.Log,
                totalRowsActivity: response.total,
                loadingActivity: false,
            });
        }
    }

    getDatausuario = () => {
        session_usuario = this.Auth.getProfile()
        var self = this
        _usuario = []
        const response = this.fetch.fetchGet(`api/usuario/get-by-token/${session_usuario.token}`);
        response.then(res => {
            if (res !== undefined && res.status === true) {

                if (Boolean(res.data) && Boolean(res.data.Usuario)) {
                    let usuario = res.data.Usuario
                    _usuario.push(<li key={1}> <span>Username: </span>{usuario.username}</li>)
                }

                if (Boolean(res.data) && Boolean(res.data.DatosUsuario)) {
                    let datos = res.data.DatosUsuario
                    _usuario.push(<li key={2} > <span>Nombre: </span>{datos.name}</li>)
                    _usuario.push(<li key={3}> <span>Apellido Paterno: </span>{datos.apellido_paterno}</li>)
                    _usuario.push(<li key={4}> <span>Apellido Materno: </span>{datos.apellido_materno}</li>)
                    _usuario.push(<li key={5}> <span>Documento de Identificación: </span>{datos.ci}</li>)
                    _usuario.push(<li key={6}> <span>Institución: </span>{datos.company}</li>)
                    _usuario.push(<li key={7}> <span>Dirección: </span>{datos.address}</li>)

                    let prefijo = ''
                    if (Boolean(res.data) && Boolean(res.data.Pais)) {
                        _usuario.push(<li key={8}> <span>Pais: </span>{res.data.Pais.name}</li>)
                        prefijo = res.data.Pais.code_phone
                    }

                    _usuario.push(<li key={9}> <span>Teléfono: </span>{prefijo + "-" + datos.phone}</li>)

                    if (Boolean(datos.image))
                        document.getElementById('avatarPerfilUser').src = datos.image
                }
                self.setState({
                    usuario: _usuario,
                });

                toast.success(res.message, {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true
                });
            }
        })
    }


    handlePerfilInformatcionClick(event) {
        window.jQuery('.link-style').removeClass('active');

        event.target.classList.add('active')
        window.jQuery('#multiCollapsePerfilInformation').collapse("show")
        window.jQuery('#multiCollapsePerfilEdit').collapse("hide")
        window.jQuery('#multiCollapsePerfilPicture').collapse('hide')
        window.jQuery('#multiCollapseChangePassword').collapse('hide')
    }

    async handlePerfilEditClick(event) {
        window.jQuery('.link-style').removeClass('active');

        event.target.classList.add('active')
        window.jQuery('#multiCollapsePerfilInformation').collapse("hide")
        window.jQuery('#multiCollapsePerfilEdit').collapse("show")
        window.jQuery('#multiCollapsePerfilPicture').collapse('hide')
        window.jQuery('#multiCollapseChangePassword').collapse('hide')
        var self = this

        this.fetch.fetchGet('api/datos-usuario/get-by-user').then(dataJson => {
            if (dataJson !== undefined && dataJson.status === true) {

                if (Boolean(dataJson.DatosUsuario)) {
                    var datos = dataJson.DatosUsuario;
                    document.getElementsByName("datos[name]")[0].value = datos.name;
                    document.getElementsByName("datos[apellido_paterno]")[0].value = datos.apellido_paterno;
                    document.getElementsByName("datos[apellido_materno]")[0].value = datos.apellido_materno;
                    document.getElementsByName("datos[ci]")[0].value = datos.ci;
                    document.getElementsByName("datos[company]")[0].value = datos.company;
                    document.getElementsByName("datos[address]")[0].value = datos.address;
                    document.getElementsByName("datos[phone]")[0].value = datos.phone;

                    if (Boolean(dataJson.Pais)) {
                        var pais = dataJson.Pais;
                        document.getElementById("basic-addon-phone-code").innerHTML = pais.code_phone;
                        self.setState({
                            optionsSelect: [{ value: pais.id, label: pais.name }]
                        })
                    }

                    self.setState({ startDate: new Date(datos.fecha_nacimiento) })

                    toast.success(dataJson.message, {
                        position: "top-right",
                        autoClose: 5000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true
                    });
                } else {
                    toast.warn(dataJson.message, {
                        position: "top-right",
                        autoClose: 5000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true
                    });
                }
            }
        })
    }

    handlePerfilPictureClick(event) {
        event.preventDefault()
        window.jQuery('.link-style').removeClass('active');

        event.target.classList.add('active')
        window.jQuery('#multiCollapsePerfilInformation').collapse("hide")
        window.jQuery('#multiCollapsePerfilEdit').collapse("hide")
        window.jQuery('#multiCollapsePerfilPicture').collapse('show')
        window.jQuery('#multiCollapseChangePassword').collapse('hide')
    }

    handleChangePasswordClick(event) {
        window.jQuery('.link-style').removeClass('active');

        event.target.classList.add('active')
        window.jQuery('#multiCollapsePerfilInformation').collapse("hide")
        window.jQuery('#multiCollapsePerfilEdit').collapse("hide")
        window.jQuery('#multiCollapsePerfilPicture').collapse('hide')
        window.jQuery('#multiCollapseChangePassword').collapse('show')
    }

    onSubmitFormDatos(event) {
        event.preventDefault();
        const form = new FormData(event.target);
        window.jQuery("#" + event.target.getAttribute("id")).parsley().validate();

        var li_error = window.jQuery("#inputDatosPhone").siblings('ul');
        window.jQuery("#inputDatosPhone").siblings('ul').remove();
        window.jQuery("#inputDatosPhone").parent('div').parent('div').append(li_error);

        if (window.jQuery("#" + event.target.getAttribute("id")).parsley().isValid()) {
            this.fetch.fetchPost(form, 'api/datos-usuario/create').then(dataJson => {
                if (dataJson !== undefined && dataJson.status === true) {
                    toast.success(dataJson.message, {
                        position: "top-right",
                        autoClose: 5000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true
                    });
                }
            })
        } else {

        }
    }

    onSubmitFormChangePassword(event) {
        event.preventDefault();
        window.jQuery("#" + event.target.getAttribute("id")).parsley().validate();
        const form = new FormData(event.target);
        if (window.jQuery("#" + event.target.getAttribute("id")).parsley().isValid()) {
            this.fetch.fetchPost(form, 'api/usuario/change-password-members').then(dataJson => {
                if (dataJson !== undefined && dataJson.status === true) {
                    toast.success(dataJson.message, {
                        position: "top-right",
                        autoClose: 5000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true
                    });
                }
            })
        }
    }

    onSubmitFormImage(event) {
        event.preventDefault();
        window.jQuery("#" + event.target.getAttribute("id")).parsley().validate();
        const form = new FormData(event.target);

        if (window.jQuery("#" + event.target.getAttribute("id")).parsley().isValid()) {

            var input = document.getElementsByName("usuariofile")[0]
            var file = input.files[0], self = this

            window.blobToBase64(input.files[0], function (base64) {
                form.append('usuario[mime_type_image]', file.type)
                form.append('usuario[image]', base64.replace(/\s/g, ''))
                self.fetch.fetchPost(form, 'api/usuario/change-image').then(dataJson => {
                    if (dataJson !== undefined && dataJson.status === true) {
                        toast.success(dataJson.message, {
                            position: "top-right",
                            autoClose: 5000,
                            hideProgressBar: false,
                            closeOnClick: true,
                            pauseOnHover: true,
                            draggable: true
                        });
                    }
                })
            });
        }
    }

    async loadOption(inputValue) {
        if (inputValue.length > 2) {
            const response = await fetch.axiosAsyncGet(`api/country/search-by-name/${inputValue}`);
            if (response !== null && response.status === true)
                return response.data;
            else
                return [];
        } else {
            return this.state.optionsSelect
        }
    }

    handleInputChange = selectedOption => {
        if (selectedOption !== null) {
            document.getElementById("basic-addon-phone-code").innerHTML = selectedOption.code_phone
        }
    };

    handleDatePickerChange(date) {
        this.setState({
            startDate: date
        });
    }

    /**funciones para el datatable de history login */
    handlePerRowsChange = async (perPage, page) => {
        this.setState({ loading: true });
        session_usuario = this.Auth.getProfile()
        await this.getRequestHistoryLogin (page, perPage, session_usuario.token, true) 
    };

    handlePageChange = async page => {
        const { perPage } = this.state;
        this.setState({ loading: true });
        session_usuario = this.Auth.getProfile()
        await this.getRequestHistoryLogin (page, perPage, session_usuario.token, false) 
    };

    getRequestHistoryLogin = async (page, perPage, token_usr, state_per_page) => {

        const response = await this.fetch.axiosAsyncGet(`api/log/history-login-by-token-user/${page}/${perPage}/${token_usr}`);
        if (response !== null && response.status === true) {
            if(state_per_page){
                this.setState({
                    loading: false,
                    data: response.Log,
                    perPage,
                });
            }else{
                this.setState({
                    loading: false,
                    data: response.Log,
                });
            }
        }
    }

    /**  funciones para el databla de activities */
    handleActivityPerRowsChange = async (perPage, page) => {
        this.setState({ loadingActivity: true });
        session_usuario = this.Auth.getProfile()

        await this.getRequestActivities  (page, perPage, session_usuario.token, true) 
    };

    handleActivityPageChange = async page => {
        const { perPage } = this.state;
        this.setState({ loadingActivity: true });
        session_usuario = this.Auth.getProfile()
        await this.getRequestActivities  (page, perPage, session_usuario.token, false) 
    };

    getRequestActivities = async (page, perPage, token_usr, state_per_page) => {

        const response = await this.fetch.axiosAsyncGet(`api/log/activity-by-token-user/${page}/${perPage}/${token_usr}`);
        if (response !== null && response.status === true) {
            if(state_per_page){
                this.setState({
                    loadingActivity: false,
                    dataActivity: response.Log,
                    perPage,
                });
            }else{
                this.setState({
                    loadingActivity: false,
                    dataActivity: response.Log,
                });
            }
        }
    }

    render() {
        const breadcrumbs = [
            {
                title: Links[0].title,
                url: Links[0].url
            },
            {
                title: Links[8].title,
                url: Links[8].url
            }
        ];

        const { loading, data, totalRows, loadingActivity, dataActivity, totalRowsActivity } = this.state

        return (

            <div id="services " className="paddingTop30" >

                <TitlePage titlePage={this.titlePage} breadcrumbs={breadcrumbs} position={'left'} />

                <div className="container features">

                    <section className="panel-menu-info">
                        <div className="panel-menu-info-content">
                            <div className="row">
                                <div className="col-4 col-md-2 col-lg-2">
                                </div>
                                <div className="col-4 col-md-2 col-lg-2">
                                </div>
                                <div className="col-4 col-md-2 col-lg-2">
                                </div>
                                <div className="col-4 col-md-2 col-lg-2">
                                    <div className="single-contact-info pointer" onClick={this.handleActivity}>
                                        <i className="fa fa-tasks" aria-hidden="true"></i>
                                        <p >{Languaje.actividades}</p>
                                    </div>
                                </div>
                                <div className="col-4 col-md-2 col-lg-2">
                                    <div className="single-contact-info pointer" onClick={this.handleLoginHistory}>
                                        <i className="fa fa-key" aria-hidden="true"></i>
                                        <p >{Languaje.login_history}</p>
                                    </div>
                                </div>
                                <div className="col-4 col-md-2 col-lg-2">
                                    <div className="single-contact-info pointer" onClick={this.hanldePerfilClick}>
                                        <i className="fa fa-cog" aria-hidden="true"></i>
                                        <p >{Languaje.perfil}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                    {this.state.showPerfil === true ?

                        <div className="row">
                            <div className="col-12 col-md-3 col-lg-3">
                                <ul className="ver-inline-menu tabbable margin-bottom-10" id="profile-menu-tabs">
                                    <li style={{textAlign: 'center'}}>
                                        <img alt="gravatar" title="avatar" id="avatarPerfilUser" width="208" height="208" className="img-fluid"
                                            src="https://secure.gravatar.com/avatar/950057323d85eb9984bc44374502823d?s=208&amp;d=mm&amp;r=g" />
                                    </li>
                                    <li className="active">
                                        <Link to={"#"} className={`link-style active`} style={{ width: "100%" }}
                                            onClick={this.handlePerfilInformatcionClick}>{Languaje.perfil_information}</Link>
                                    </li>

                                    <li className="active">
                                        <Link to={"#"} className={`link-style`} style={{ width: "100%" }}
                                            onClick={this.handlePerfilEditClick}>{Languaje.perfil_edit}</Link>
                                    </li>

                                    <li className="active">
                                        <Link to={"#"} className={`link-style`} style={{ width: "100%" }}
                                            onClick={this.handlePerfilPictureClick}>{Languaje.perfil_picture}</Link>
                                    </li>
                                    <li className="edit-profile">
                                        <Link to={"#"} className={`link-style`} style={{ width: "100%" }}
                                            onClick={this.handleChangePasswordClick}>{Languaje.change_password}</Link>
                                    </li>
                                </ul>
                            </div>

                            <div className="col-12 col-md-9 col-lg-9">
                                <div className="row">
                                    <div className="col-12 col-md-12 col-lg-12">
                                        <div className="collapse show" id="multiCollapsePerfilInformation">
                                            <div className="card card-body">
                                                <ul className="unstyled " >
                                                    {this.state.usuario}
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-12 col-md-12 col-lg-12">
                                        <div className="collapse " id="multiCollapsePerfilEdit">
                                            <div className="card card-body">
                                                <div className="row">
                                                    <div className="col-12 ">
                                                        <div className="alert alert-info" role="alert">
                                                            Esto cambiará los datos de su cuenta, esta sección es solo para miembros autentificados.
                                                        </div>
                                                    </div>
                                                </div>

                                                <form action="" className="contact__form needs-validation" name="formPerfil" id="formPerfil"
                                                    method="post" noValidate onSubmit={this.onSubmitFormDatos} style={{ width: '100%' }}>

                                                    <div className="row">
                                                        <div className="col-12 col-sm-12 col-md-6 col-lg-6 form-group">
                                                            <label htmlFor="datos[name]">Nombre *</label>
                                                            <input name="datos[name]" id="datos[name]" type="text" className="form-control" placeholder="Nombre"
                                                                data-parsley-minlength="3" minLength="3"
                                                                required data-parsley-required="true" pattern="[a-zA-Z À-ÿ\u00f1\u00d1]+" data-parsley-pattern="[a-zA-Z À-ÿ\u00f1\u00d1]+" />
                                                        </div>
                                                        <div className="col-12 col-sm-12 col-md-6 col-lg-6 form-group">
                                                            <label htmlFor="datos[apellido_paterno]" >Apellido Paterno *</label>
                                                            <input name="datos[apellido_paterno]" id="datos[apellido_paterno]" type="text" className="form-control" placeholder="Apellido Paterno"
                                                                data-parsley-minlength="3" minLength="3"
                                                                required data-parsley-required="true" pattern="[a-zA-Z À-ÿ\u00f1\u00d1]+" data-parsley-pattern="[a-zA-Z À-ÿ\u00f1\u00d1]+" />
                                                        </div>
                                                    </div>

                                                    <div className="row">
                                                        <div className="col-12 col-sm-12 col-md-6 col-lg-6 form-group">
                                                            <label htmlFor="datos[apellido_materno]">Apellido Materno</label>
                                                            <input name="datos[apellido_materno]" id="datos[apellido_materno]" type="text" className="form-control" placeholder="Apellido Materno"
                                                                pattern="[a-zA-Z À-ÿ\u00f1\u00d1]+" data-parsley-pattern="[a-zA-Z À-ÿ\u00f1\u00d1]+" />
                                                        </div>

                                                        <div className="col-12 col-sm-12 col-md-6 col-lg-6 form-group">
                                                            <label htmlFor="datos[ci]">Documento de Identificación *</label>
                                                            <input name="datos[ci]" id="datos[ci]" type="text" className="form-control" placeholder="CI"
                                                                data-parsley-minlength="5" minLength="5"
                                                                required data-parsley-required="true" pattern="[a-zA-Z0-9-]+" data-parsley-pattern="[a-zA-Z0-9-]+" />
                                                        </div>
                                                    </div>

                                                    <div className="row">
                                                        <div className="col-12 col-sm-12 col-md-6 col-lg-6 form-group">
                                                            <label htmlFor="datos[company]">Institución</label>
                                                            <input name="datos[company]" id="datos[company]" type="text" className="form-control" placeholder="Institución"
                                                                pattern="[a-zA-Z0-9 À-ÿ\u00f1\u00d1]+" data-parsley-pattern="[a-zA-Z0-9 À-ÿ\u00f1\u00d1]+" />
                                                        </div>

                                                        <div className="col-12 col-sm-12 col-md-6 col-lg-6 form-group">
                                                            <label htmlFor="datos[fecha_nacimiento]">Fecha de Nacimiento (DD/MM/YYYY)</label>
                                                            <DatePicker
                                                                locale="es"
                                                                dateFormat={Config[4].format}
                                                                selected={this.state.startDate}
                                                                onChange={this.handleDatePickerChange}
                                                                maxDate={Config[1].anio}
                                                                className="form-control data-toggle"
                                                                name="datos[fecha_nacimiento]"
                                                                id="datos[fecha_nacimiento]"
                                                                data-toggle="tooltip"
                                                                data-placement="left"
                                                                showMonthDropdown
                                                                showYearDropdown
                                                                dropdownMode="select"
                                                                required
                                                            />
                                                        </div>
                                                    </div>

                                                    <div className="row">
                                                        <div className="col-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                                            <label htmlFor="datos[address]">Dirección *</label>
                                                            <input name="datos[address]" id="datos[address]" type="text" className="form-control" placeholder="Dirección"
                                                                required data-parsley-required="true" pattern="[.a-zA-Z0-9 À-ÿ\u00f1\u00d1]+" data-parsley-pattern="[.a-zA-Z0-9 À-ÿ\u00f1\u00d1]+" />
                                                        </div>
                                                    </div>

                                                    <div className="row">
                                                        <div className="col-12 col-sm-12 col-md-6 col-lg-6 form-group">
                                                            <label htmlFor="datos[id_country]">Pais *</label>
                                                            <AsyncSelect
                                                                cacheOptions
                                                                loadOptions={this.loadOption}
                                                                defaultOptions
                                                                onChange={this.handleInputChange}
                                                                isClearable
                                                                isSearchable
                                                                placeholder="Buscar Pais"
                                                                data-parsley-required="true"
                                                                data-parsley-minlength="4"
                                                                minLength="4"
                                                                name="datos[id_country]"
                                                                id="datos[id_country]"
                                                                required
                                                                defaultValue={[this.state.optionsSelect[0]]}
                                                            />
                                                        </div>

                                                        <div className="col-12 col-sm-12 col-md-6 col-lg-6 form-group">
                                                            <label htmlFor="inputDatosPhone">Teléfono *</label>
                                                            <div className="input-group mb-3">
                                                                <div className="input-group-prepend">
                                                                    <span className="input-group-text" id="basic-addon-phone-code">-</span>
                                                                </div>
                                                                <input type="text" name="datos[phone]" id="inputDatosPhone" className="form-control" placeholder="Teléfono" aria-label="Teléfono"
                                                                    aria-describedby="basic-addon-phone-code" data-parsley-required="true" data-parsley-minlength="6" minLength="6" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div className="row">
                                                        <div className="col-sm-12 col-md-12 col-lg-12 form-group">
                                                            <input name="save" type="submit" className="button-style pull-right" value="Guardar Cambios" />
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-12 col-md-12 col-lg-12">
                                        <div className="collapse " id="multiCollapsePerfilPicture">
                                            <div className="card card-body">
                                                <form action="" className="contact__form needs-validation" name="formPerfilImage" id="formPerfilImage"
                                                    method="post" noValidate onSubmit={this.onSubmitFormImage} style={{ width: '100%' }}
                                                    encType="multipart/form-data" >
                                                    <div className="row">
                                                        <div className="col-12 form-group files color">
                                                            <label htmlFor="usuariofile">Sube tu archivo *</label>
                                                            <input type="file" name="usuariofile" id="usuariofile" className="form-control" multiple="" />
                                                        </div>
                                                    </div>

                                                    <div className="row">
                                                        <div className="col-sm-12 col-md-12 col-lg-12 form-group">
                                                            <input name="save" type="submit" className="button-style pull-right" value="Guardar Cambios" />
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col">
                                        <div className="collapse" id="multiCollapseChangePassword">
                                            <div className="card card-body">

                                                <div className="row">
                                                    <div className="col-12 ">
                                                        <div className="alert alert-info" role="alert">
                                                            Esto cambiará la contraseña de acceso a su cuenta, esta sección es solo para miembros autentificados.
                                                        </div>
                                                    </div>
                                                </div>

                                                <form action="" className="contact__form needs-validation" name="formChangePassword" id="formChangePassword"
                                                    method="post" noValidate onSubmit={this.onSubmitFormChangePassword} style={{ width: '100%' }}>

                                                    <div className="row">
                                                        <div className="col-12 col-sm-12 col-md-6 col-lg-6 form-group">
                                                            <label htmlFor="usuario[password_old]">Contraseña Actual *</label>
                                                            <input name="usuario[password_old]" id="usuario[password_old]" type="password" className="form-control" placeholder="Contraseña Actual"
                                                                data-parsley-required="true" data-parsley-minlength="6" minLength="6" required />
                                                        </div>
                                                    </div>

                                                    <div className="row">
                                                        <div className="col-12 col-sm-12 col-md-6 col-lg-6 form-group">
                                                            <label htmlFor="usuario_password">Nueva Contraseña *</label>
                                                            <input id="usuario_password" name="usuario[password]" type="password" className="form-control" placeholder="Contraseña"
                                                                data-parsley-required="true" data-parsley-minlength="6" minLength="6" required />
                                                        </div>
                                                    </div>

                                                    <div className="row">
                                                        <div className="col-12 col-sm-12 col-md-6 col-lg-6 form-group">
                                                            <label htmlFor="usuario[password_repeat]">Repita la Nueva Contraseña *</label>
                                                            <input name="usuario[password_repeat]" id="usuario[password_repeat]" type="password" className="form-control" placeholder="Repita la Contraseña"
                                                                data-parsley-required="true" data-parsley-minlength="6" minLength="6" data-parsley-equalto="#usuario_password" required />
                                                        </div>
                                                    </div>

                                                    <div className="row">
                                                        <div className="col-sm-12 col-md-12 col-lg-12 form-group">
                                                            <input name="save" type="submit" className="button-style pull-right" value="Cambiar Contraseña" />
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        : ""}

                    {this.state.showHistorySession === true ?
                        <div className="row" style={{ marginTop: '-60px' }}>
                            <div className="col-12 col-md-12 col-lg-12">
                                <DataTable
                                    columns={columns}
                                    data={data}
                                    progressPending={loading}
                                    pagination
                                    paginationServer
                                    paginationTotalRows={totalRows}
                                    onChangeRowsPerPage={this.handlePerRowsChange}
                                    onChangePage={this.handlePageChange}
                                    noDataComponent={Languaje.there_are_no_records_to_display}
                                    expandableRowsComponent={<HistoryExpandedComponent />}
                                    highlightOnHover
                                    expandableRows
                                />
                            </div>
                        </div>
                        : ""}

                    {this.state.showActivity === true ?
                        <div className="row" style={{ marginTop: '-60px' }}>
                            <div className="col-12 col-md-12 col-lg-12">
                                <DataTable
                                    columns={columnsActivity}
                                    data={dataActivity}
                                    progressPending={loadingActivity}
                                    pagination
                                    paginationServer
                                    paginationTotalRows={totalRowsActivity}
                                    onChangeRowsPerPage={this.handleActivityPerRowsChange}
                                    onChangePage={this.handleActivityPageChange}
                                    noDataComponent={Languaje.there_are_no_records_to_display}
                                    highlightOnHover
                                    expandableRowsComponent={<ActivityExpandedComponent />}
                                    expandableRows
                                />
                            </div>
                        </div>
                        : ""}
                </div>

                <ToastContainer enableMultiContainer containerId={'Z'}
                    position="top-right"
                    autoClose={5000}
                    hideProgressBar={false}
                    newestOnTop={false}
                    closeOnClick
                    rtl={false}
                    pauseOnVisibilityChange
                    draggable
                    pauseOnHover
                />
                <ToastContainer />

            </div>
        );
    }
}

export default Perfil;