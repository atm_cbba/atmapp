import React, { Component, createRef } from 'react';
import { Link } from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import TitlePage from '../../components/utils/TitlePage';

import AuthService from '../../components/Usuario/AuthService';
import Fetch from '../../components/utils/Fetch';

import Links from '../../data/link';
import Texto from '../../data/es';
import '../style/parsley.css';

var fetch = null
class Login extends Component {

    constructor(props, context) {
        super(props, context);

        this.Auth = new AuthService();
        this.id_form = "formLoginUser"

        this.refInputPassword = React.createRef()

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleInitSessionClick = this.handleInitSessionClick.bind(this)
        this.handleOpenAccountClick = this.handleOpenAccountClick.bind(this)
        this.handleOpenPassword = this.handleOpenPassword.bind(this)

        this.state = {
            showMessageAlert: false,
            showButtonSession: true
        };

        fetch = new Fetch();
        fetch.setToast(toast);
    }

    handleSubmit(event) {
        event.preventDefault();
        window.jQuery("#" + this.id_form).parsley().validate();

        var ul_error_dir = window.jQuery("input[name='usuario[password]']").parent().parent().find('ul');
        window.jQuery("input[name='usuario[password]']").parent().parent().find('ul').remove();
        window.jQuery("input[name='usuario[password]']").parent('div').parent('div').append( ul_error_dir );

        if (window.jQuery("#" + this.id_form).parsley().isValid()) {
            const form = new FormData(event.target);
            var target = event.target

            window.disableButton(window.jQuery(target).find("input[type=submit]")[0])

            this.Auth.login(form).then(dataJson => {

                if (dataJson.status === true) {
                    var browserInfo = window.getBrowserInfo()
                    var myip = window.myIP()

                    const form = new FormData();
                    form.append('history_login[ip]', window.myIP());
                    //form.append('history_login[country]', window.getPais(myip));
                    form.append('history_login[country]', '');
                    form.append('history_login[navegador]', browserInfo.browser);
                    form.append('history_login[device]', browserInfo.device + "_" + browserInfo.os);

                    fetch.fetchPost(form, 'api/history-login/create').then(dataJson => {
                        if (dataJson !== undefined && dataJson.status === true) {
                            console.log(dataJson.message)
                        }
                    })

                    toast.success(dataJson.message + ", " + Texto.espere_redireccionamos_pagina, {
                        position: "top-right",
                        autoClose: 5000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true
                    });

                    window.enableButton(window.jQuery(target).find("input[type=submit]")[0])

                    setTimeout(() => {
                        window.location.href = '/';
                    }, 3000);
                } else {
                    window.enableButton(window.jQuery(target).find("input[type=submit]")[0])
                    toast.warn(dataJson.message, {
                        position: "top-right",
                        autoClose: 5000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true
                    });
                }
            }).catch(error => {
                window.enableButton(window.jQuery(target).find("input[type=submit]")[0])

                var message = this.message(error)
                toast.error(message, {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true
                });
            });
        } else {
            toast.warn(Texto.campos_obligatorios, {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true
            });
        }
    }

    message(error) {
        var message = ""
        if (window.is_json(error) && error.hasOwnProperty('response')) {
            message = error.response.statusText;
        } else {
            if (window.is_json(error.message))
                message = this.jsonToString(error.message)
            else
                if (error.message.indexOf('Unexpected end of JSON') >= 0 || error.message.indexOf('Unexpected token') >= 0)
                    message = "500, Error Interno del Servidor. Vuelva a intentarlo de nuevo."
                else {
                    if (error instanceof String && error.indexOf('Unexpected end of JSON') >= 0) {
                        message = "500, Error Interno del Servidor. Vuelva a intentarlo de nuevo."
                    } else
                        message = error.message
                }
        }
        return message;
    }

    componentWillMount() {
        if (this.Auth.loggedIn()) {
            //regresamos a la pagina principal o le redireccionamos
            this.props.history.replace(Links[0].url);
        }
        else {
            this.props.history.replace(Links[4].url)
        }
    }

    handleInitSessionClick(event) {
        event.preventDefault()
        this.setState({ showButtonSession: false })
    }

    handleOpenAccountClick(event) {
        event.preventDefault()
        window.location.href = Links[5].url;
    }

    handleOpenPassword(event) {
        event.preventDefault()
        let input = this.refInputPassword.current
        let icon = input.nextSibling.firstElementChild.firstElementChild;
        if(input.type === "text"){
            input.type = "password"
            icon.classList.add("fa-eye")
            icon.classList.remove("fa-eye-slash")
        }else{
            if(input.type === "password"){
                input.type = "text"
                icon.classList.add("fa-eye-slash")
                icon.classList.remove("fa-eye")
            }
        }
    }

    render() {
        const breadcrumbs = [
            {
                title: Links[0].title,
                url: Links[0].url
            },
            {
                title: Texto.account_user,
                url: '#'
            }
        ];
        return (
            <div id="contact" className="contact paddingTop" >
                {/* Breadcrumb Area Start */}
                <TitlePage titlePage={Texto.account_user} breadcrumbs={breadcrumbs} position={'center'} />
                {/* Breadcrumb Area End */}

                <form action="" className="contact__form needs-validation center-login" name={this.id_form} id={this.id_form}
                    method="post" noValidate onSubmit={this.handleSubmit} >

                    {this.state.showButtonSession ?
                        <div className="row justify-content-md-center">
                            <div className="col-12  col-md-4 col-lg-4 button-big" onClick={this.handleInitSessionClick}>
                                <div className="left">
                                    <h3>{Texto.start_sesion}</h3>
                                    <span>{Texto.you_need_usser_and_password}</span>
                                </div>

                                <div className="right">
                                    <i className="fa fa-key" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                        : ""}

                    {this.state.showButtonSession ?
                        <div className="row justify-content-md-center">
                            <div className="col-12  col-md-4 col-lg-4 button-big" onClick={this.handleOpenAccountClick}>
                                <div className="left">
                                    <h3>{Texto.create_account}</h3>
                                    <span>{Texto.is_fast_easy}</span>
                                </div>

                                <div className="right">
                                    <i className="fa fa-user" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                        : ""}

                    {this.state.showMessageAlert ?
                        <div className="row">
                            <div className="col-0 col-sm-0 col-md-2 col-lg-4 form-group">
                            </div>
                            <div className="col-12 col-sm-12 col-md-8 col-lg-4 form-group">
                                <div class="alert alert-success" role="alert">
                                    This is a success alert—check it out!
                                </div>
                            </div>
                            <div className="col-0 col-sm-0 col-md-2 col-lg-4 form-group">
                            </div>
                        </div>
                        : ""}

                    {!this.state.showButtonSession ?
                        <>
                            <div className="row">
                                <div className="col-0 col-sm-0 col-md-2 col-lg-4 form-group">
                                </div>

                                <div className="col-12 col-sm-12 col-md-8 col-lg-4 form-group">
                                    <label htmlFor="usuario[username]">Correo Electronico</label>
                                    <input name="usuario[username]" id="usuario[username]" type="email" className="form-control" placeholder="Username"
                                        data-parsley-required="true" data-parsley-type="email" />
                                </div>

                                <div className="col-0 col-sm-0 col-md-2 col-lg-4 form-group">
                                </div>
                            </div>

                            <div className="row">
                                <div className="col-0 col-sm-0 col-md-2 col-lg-4 ">
                                </div>
                                <div className="col-12 col-sm-12 col-md-8 col-lg-4 form-group">
                                    <label htmlFor="usuario[password]">Contraseña</label>
                                    <div className="input-group mb-3">
                                        <input name="usuario[password]" id="usuario[password]" type="password" className="form-control" placeholder="Contraseña"
                                            data-parsley-required="true" data-parsley-minlength="6" minLength="6" ref={this.refInputPassword} />
                                        <div className="input-group-append">
                                            <button className="btn btn-outline-secondary" type="button" onClick={this.handleOpenPassword}><i className="fa fa-eye" aria-hidden="true"></i></button>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-0 col-sm-0 col-md-2 col-lg-4 ">
                                </div>
                            </div>

                            <div className="row">
                                <div className="col-0 col-sm-0 col-md-2 col-lg-4 ">
                                </div>
                                <div className="col-12 col-sm-12 col-md-8 col-lg-4 ">
                                    <input name="submit" type="submit" className="button-style pull-right" value={Texto.enviar} />
                                </div>
                                <div className="col-0 col-sm-0 col-md-2 col-lg-4 ">
                                </div>
                            </div>

                            <br />

                            <div className="row">
                                <div className="col-0 col-sm-0 col-md-2 col-lg-4 ">
                                </div>
                                <div className="col-12 col-sm-12 col-md-8 col-lg-4 form-group">
                                    <Link to={{ pathname: Links[5].url }} >{Texto.no_account}</Link>
                                </div>
                                <div className="col-0 col-sm-0 col-md-2 col-lg-4 ">
                                </div>
                            </div>

                            <div className="row">
                                <div className="col-0 col-sm-0 col-md-2 col-lg-4 ">
                                </div>
                                <div className="col-12 col-sm-12 col-md-8 col-lg-4 form-group">
                                    <Link to={{ pathname: Links[10].url }} >{Texto.lost_password}</Link>
                                </div>
                                <div className="col-0 col-sm-0 col-md-2 col-lg-4 ">
                                </div>
                            </div>

                            <div className="row">
                                <div className="col-0 col-sm-0 col-md-2 col-lg-4 ">
                                </div>
                                <div className="col-12 col-sm-12 col-md-8 col-lg-4 form-group">
                                    <Link to={{ pathname: Links[13].url }} >{Texto.reenviar_activacion_cuenta}</Link>
                                </div>
                                <div className="col-0 col-sm-0 col-md-2 col-lg-4 ">
                                </div>
                            </div>
                        </>
                        : ""}
                </form>

                <ToastContainer enableMultiContainer containerId={'Z'}
                    position="top-right"
                    autoClose={5000}
                    hideProgressBar={false}
                    newestOnTop={false}
                    closeOnClick
                    rtl={false}
                    pauseOnVisibilityChange
                    draggable
                    pauseOnHover
                />
                <ToastContainer />
            </div>
        );
    }
}

export default Login;