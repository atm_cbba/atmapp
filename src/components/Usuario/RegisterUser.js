import React, { Component } from 'react';
//import { Link } from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import DatePicker, { registerLocale } from 'react-datepicker';
import datepicker from "react-datepicker/dist/react-datepicker.css";
import es from "date-fns/locale/es"; // the locale you want

import AuthService from '../../components/Usuario/AuthService';
import Fetch from '../../components/utils/Fetch';
import Links from '../../data/link';
import Texto from '../../data/es';
import TitlePage from '../../components/utils/TitlePage';
import Config from '../../data/config';
import '../style/strength.css';
import '../js/strength.js';
import '../style/parsley.css';

registerLocale("es", es); // register it with the name you want
var message_register = ""
class RegisterUser extends Component {

    constructor(props, context) {
        super(props, context);

        this.Auth = new AuthService();
        this.id_form = "formCreateUser"

        this.refInputPassword = React.createRef()
        this.refInputRepeatPassword = React.createRef()

        this.fetch = new Fetch();
        this.fetch.setToast(toast)
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleDatePickerChange = this.handleDatePickerChange.bind(this);
        this.handleOnFocusEvent = this.handleOnFocusEvent.bind(this);
        this.handleOutFocusEvent = this.handleOutFocusEvent.bind(this);
        this.handleOpenPassword = this.handleOpenPassword.bind(this)
        this.handleOpenRepeatPassword = this.handleOpenRepeatPassword.bind(this)

        this.state = {
            showMessageAlert: false,
            startDate: null,
            //startDate: new Date(),
        };
    }

    componentDidMount() {
        var detector = new window.MobileDetect(window.navigator.userAgent)
        if (!Boolean(detector.mobile())) {
            window.jQuery('[data-toggle="tooltip"]').tooltip()
            window.jQuery('.data-toggle').tooltip()
        }

        window.jQuery('#usuario_password').strength({
            strengthClass: 'strength',
            strengthMeterClass: 'strength_meter',
            strengthButtonText: '',
            strengthButtonTextToggle: ''
        });
    }

    componentDidUpdate() {
        if (this.state.showMessageAlert === true)
            document.getElementById("divAlertRegisterUser").innerHTML = message_register
    }

    componentWillMount() {
        if (this.Auth.loggedIn())
            this.props.history.replace(Links[4].url)
    }

    handleSubmit(event) {
        event.preventDefault();
        window.jQuery("#" + this.id_form).parsley().validate();

        const ul_error_pass = window.jQuery("input[name='usuario[password]']").parent().parent().find('ul');
        window.jQuery("input[name='usuario[password]']").parent().parent().find('ul').remove();
        window.jQuery("input[name='usuario[password]']").parent('div').parent('div').append(ul_error_pass);

        const ul_error_pass_confirm = window.jQuery("input[name='usuario[confirm_password]']").parent().parent().find('ul');
        window.jQuery("input[name='usuario[confirm_password]']").parent().parent().find('ul').remove();
        window.jQuery("input[name='usuario[confirm_password]']").parent('div').parent('div').append(ul_error_pass_confirm);

        if (window.jQuery("#" + this.id_form).parsley().isValid()) {
            const form = new FormData(event.target);
            var self = this;
            this.fetch.fetchPost(form, 'api/usuario/create', event.target).then(dataJson => {
                if (dataJson !== undefined && dataJson.status === true) {
                    message_register = dataJson.message
                    self.setState({
                        showMessageAlert: true
                    })
                    toast.success(dataJson.message, {
                        position: "top-right",
                        autoClose: 5000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true
                    });
                }
            })
        } else {
            toast.warn(Texto.campos_obligatorios, {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true
            });
        }
    }

    handleDatePickerChange(date) {
        this.setState({
            startDate: date
        });
    }

    handleOnFocusEvent(event) {

        var detector = new window.MobileDetect(window.navigator.userAgent)
        if (Boolean(detector.mobile())) {
            event.preventDefault();
            if (event.target.name === 'datos[fecha_nacimiento]')
                event.target.parentElement.parentElement.nextElementSibling.classList.remove('d-none')
            else
                if (event.target.name === 'usuario[password]' || event.target.name === 'usuario[confirm_password]')  
                    event.target.parentElement.parentElement.lastElementChild.classList.remove('d-none')
                else
                    event.target.nextElementSibling.classList.remove('d-none')
        }
    }

    handleOutFocusEvent(event) {
        var detector = new window.MobileDetect(window.navigator.userAgent)
        if (Boolean(detector.mobile())) {
            event.preventDefault();
            if (event.target.name === 'datos[fecha_nacimiento]')
                event.target.parentElement.parentElement.nextElementSibling.classList.add('d-none')
            else
                if (event.target.name === 'usuario[password]' || event.target.name === 'usuario[confirm_password]')
                    event.target.parentElement.parentElement.lastElementChild.classList.add('d-none')
                else
                    event.target.nextElementSibling.classList.add('d-none')
        }
    }

    handleOpenPassword(event) {
        event.preventDefault()
        let input = this.refInputPassword.current
        let icon = input.nextSibling.nextSibling.firstElementChild.firstElementChild
        this.changeIconInput(input, icon)
    }

    handleOpenRepeatPassword(event) {
        event.preventDefault()
        let input = this.refInputRepeatPassword.current
        let icon = input.nextSibling.firstElementChild.firstElementChild
        this.changeIconInput(input, icon)
    }

    changeIconInput(input, icon) {
        if (input.type === "text") {
            input.type = "password"
            icon.classList.add("fa-eye")
            icon.classList.remove("fa-eye-slash")
        } else {
            if (input.type === "password") {
                input.type = "text"
                icon.classList.add("fa-eye-slash")
                icon.classList.remove("fa-eye")
            }
        }
    }

    render() {
        const breadcrumbs = [
            {
                title: Links[0].title,
                url: Links[0].url
            },
            {
                title: Texto.register_account,
                url: '#'
            }
        ];
        return (
            <div id="contact" className="contact paddingTop" >

                {/* Breadcrumb Area Start */}
                <TitlePage titlePage={Texto.register_new_account} breadcrumbs={breadcrumbs} position={'center'} />
                {/* Breadcrumb Area End */}

                {this.state.showMessageAlert ?
                    <div className="row justify-content-md-center justify-content-lg-center">
                        <div className="col-12 col-sm-12 col-md-9 col-lg-7" style={{ paddingLeft: "15px", paddingRight: "15px" }}>
                            <div className="alert alert-success" role="alert" id="divAlertRegisterUser" style={{ fontSize: '1.3rem', marginLeft: '15px', marginRight: '15px' }}>
                                This is a success alert—check it out!
                            </div>
                        </div>
                    </div>
                    : ""}
                {!this.state.showMessageAlert ?
                    <form action="" className="contact__form center-login" name={this.id_form} id={this.id_form}
                        method="post" noValidate onSubmit={this.handleSubmit} >

                        <div className="row justify-content-md-center justify-content-lg-center">
                            <div className="col-12 col-sm-12 col-md-9 col-lg-7 ">
                                <div className="row ">
                                    <div className="col-12 col-sm-12 col-md-4 col-lg-4 form-group">
                                        <label htmlFor="datos[name]">Nombre</label>
                                        <input name="datos[name]" id="datos[name]" type="text" className="form-control" placeholder="Nombre" autoComplete="false"
                                            data-parsley-minlength="3" minLength="3" required data-parsley-required="true" pattern="[a-zA-Z À-ÿ\u00f1\u00d1]+"
                                            data-parsley-pattern="[a-zA-Z À-ÿ\u00f1\u00d1]+" data-toggle="tooltip" data-placement="left" title="¿Como te llamas?"
                                            onFocus={this.handleOnFocusEvent} onBlur={this.handleOutFocusEvent} />
                                        <div className="alert alert-info d-none" role="alert" style={{ padding: ".3rem .3rem" }}>
                                            ¿Como te llamas?
                                        </div>
                                    </div>

                                    <div className="col-12 col-sm-12 col-md-4 col-lg-4 form-group">
                                        <label htmlFor="datos[apellido_paterno]">Apellido Paterno</label>
                                        <input name="datos[apellido_paterno]" id="datos[apellido_paterno]" type="text" className="form-control" placeholder="Apellido Paterno" autoComplete="false"
                                            data-parsley-minlength="3" minLength="3" required data-parsley-required="true" pattern="[a-zA-Z À-ÿ\u00f1\u00d1]+"
                                            data-parsley-pattern="[a-zA-Z À-ÿ\u00f1\u00d1]+" data-toggle="tooltip" data-placement="left" title="¿Como se apellida tu papá?"
                                            onFocus={this.handleOnFocusEvent} onBlur={this.handleOutFocusEvent} />
                                        <div className="alert alert-info d-none" role="alert" style={{ padding: ".3rem .3rem" }}>
                                            ¿Como se apellida tu papá?
                                        </div>
                                    </div>

                                    <div className="col-12 col-sm-12 col-md-4 col-lg-4 form-group">
                                        <label htmlFor="datos[apellido_materno]">Apellido Materno</label>
                                        <input name="datos[apellido_materno]" id="datos[apellido_materno]" type="text" className="form-control" placeholder="Apellido Materno" autoComplete="false"
                                            data-parsley-minlength="3" minLength="3" data-parsley-required="false" pattern="[a-zA-Z À-ÿ\u00f1\u00d1]+"
                                            data-parsley-pattern="[a-zA-Z À-ÿ\u00f1\u00d1]+" data-toggle="tooltip" data-placement="left" title="¿Como se apellida tu mamá?"
                                            onFocus={this.handleOnFocusEvent} onBlur={this.handleOutFocusEvent} />
                                        <div className="alert alert-info d-none" role="alert" style={{ padding: ".3rem .3rem" }}>
                                            ¿Como se apellida tu mamá?
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="row justify-content-md-center justify-content-lg-center">
                            <div className="col-12 col-sm-12 col-md-9 col-lg-7 ">
                                <div className="row ">
                                    <div className="col-12 col-sm-12 col-md-8 col-lg-8 form-group">
                                        <label htmlFor="usuario[username]">Correo Electronico</label>
                                        <input name="usuario[username]" id="usuario[username]" type="email" className="form-control" placeholder="Correo Electronico" autoComplete="false"
                                            data-parsley-required="true" data-parsley-type="email" data-toggle="tooltip" data-placement="left" title="Usarás esta información cuando entres a tu cuenta y si alguna vez tienes que cambiar la contraseña."
                                            onFocus={this.handleOnFocusEvent} onBlur={this.handleOutFocusEvent} />
                                        <div className="alert alert-info d-none" role="alert" style={{ padding: ".3rem .3rem" }}>
                                            Usarás esta información cuando entres a tu cuenta y si alguna vez tienes que cambiar la contraseña.
                                        </div>
                                    </div>

                                    <div className="col-12 col-sm-12 col-md-4 col-lg-4 form-group">
                                        <label htmlFor="datos[fecha_nacimiento]">Fecha de Nacimiento (DD/MM/YYYY)</label>
                                        <DatePicker
                                            locale="es"
                                            dateFormat={Config[4].format}
                                            selected={this.state.startDate}
                                            onChange={this.handleDatePickerChange}
                                            maxDate={Config[1].anio}
                                            className="form-control data-toggle"
                                            name="datos[fecha_nacimiento]"
                                            id="datos[fecha_nacimiento]"
                                            //data-parsley-required="true"
                                            data-toggle="tooltip"
                                            data-placement="left"
                                            title="¿Cuando Naciste?"
                                            showMonthDropdown
                                            showYearDropdown
                                            dropdownMode="select"
                                            required
                                            autoComplete="false"
                                            onFocus={this.handleOnFocusEvent} onBlur={this.handleOutFocusEvent}
                                        //placeholderText="¿Cuando Naciste?" 
                                        />
                                        <div className="alert alert-info d-none" role="alert" style={{ padding: ".3rem .3rem" }}>
                                            ¿Cuando Naciste?
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="row justify-content-md-center justify-content-lg-center">
                            <div className="col-12 col-sm-12 col-md-9 col-lg-7 ">
                                <div className="row ">
                                    <div className="col-12 col-sm-12 col-md-4 col-lg-4 form-group">
                                        <label htmlFor="usuario_password">Contraseña</label>
                                        <div className="input-group mb-3">
                                            <input name="usuario[password]" id="usuario_password" type="password" className="form-control" placeholder="Contraseña"
                                                data-parsley-required="true" data-parsley-minlength="6" minLength="6" autoComplete="false"
                                                data-toggle="tooltip" data-placement="left" title="Ingresa una combinación de al menos seis números, letras y signos de puntuación (como '!' y '&')."
                                                onFocus={this.handleOnFocusEvent} onBlur={this.handleOutFocusEvent} ref={this.refInputPassword} />
                                            <div className="input-group-append">
                                                <button className="btn btn-outline-secondary" type="button" onClick={this.handleOpenPassword}><i className="fa fa-eye" aria-hidden="true"></i></button>
                                            </div>
                                        </div>

                                        <div className="alert alert-info d-none" role="alert" style={{ padding: ".3rem .3rem" }}>
                                            Ingresa una combinación de al menos seis números, letras y signos de puntuación (como '!' y '&').
                                        </div>
                                    </div>

                                    <div className="col-12 col-sm-12 col-md-4 col-lg-4 form-group">
                                        <label htmlFor="usuario[confirm_password]">Repita la Contraseña</label>
                                        <div className="input-group mb-3">
                                            <input name="usuario[confirm_password]" id="usuario[confirm_password]" type="password" className="form-control" placeholder="Repita la Contraseña"
                                                data-parsley-required="true" data-parsley-minlength="6" minLength="6" data-parsley-equalto="#usuario_password"
                                                data-toggle="tooltip" data-placement="left" title="Vuelve a ingresar la misma contraseña." autoComplete="false"
                                                onFocus={this.handleOnFocusEvent} onBlur={this.handleOutFocusEvent} ref={this.refInputRepeatPassword} />
                                            <div className="input-group-append">
                                                <button className="btn btn-outline-secondary" type="button" onClick={this.handleOpenRepeatPassword}><i className="fa fa-eye" aria-hidden="true"></i></button>
                                            </div>
                                        </div>
                                        <div className="alert alert-info d-none" role="alert" style={{ padding: ".3rem .3rem" }}>
                                            Vuelve a ingresar la misma contraseña.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="row justify-content-md-center justify-content-lg-center">
                            <div className="col-12 col-sm-12 col-md-9 col-lg-7 ">
                                <input name="submit" type="submit" className="button-style pull-right" value="Enviar" />
                            </div>
                        </div>
                    </form>
                    : ""}

                <ToastContainer enableMultiContainer containerId={'Z'}
                    position="top-right"
                    autoClose={5000}
                    hideProgressBar={false}
                    newestOnTop={false}
                    closeOnClick
                    rtl={false}
                    pauseOnVisibilityChange
                    draggable
                    pauseOnHover
                />
                <ToastContainer />
            </div>
        );
    }
}

export default RegisterUser;