import React, { Component } from 'react';

class ValidateDeuda extends Component {

    constructor(props, context) {
        super(props, context);

        this.handleSearchInmuebleClick = this.handleSearchInmuebleClick.bind(this)

        this.state = {
            optionsObjetoTributario: "",
            searchData: false,
            message: ""
        };

        this.refSlcObjetoTributario = React.createRef();
        this.refInputSearchObjectTributari = React.createRef()
        this.refInputCancelObjectTributari = React.createRef()
        this.refSlcTypeSearchMap = React.createRef()
        this.refInputSearchMap = React.createRef()
    }

    componentDidMount() {
        this.loadObjetoTributario()
    }

    loadObjetoTributario = () => {
        let self = this
        const objet_tributario_res = this.props.fetch.fetchGet(`api/prescripcion/objeto-tributario/all`);
        objet_tributario_res.then(res => {
            if (res !== undefined && res !== undefined && res.status === true) {
                if (res.status === true && res.ObjetoTributario !== null) {
                    const listItems = res.ObjetoTributario.map((item, index) => {
                        return <option key={index} value={item.id} titleinput={item.description} code={item.code} >{item.name}</option>
                    });
                    self.setState({ optionsObjetoTributario: listItems });
                }
            }
        })
    }

    async handleSearchInmuebleClick(event) {

        event.preventDefault()
        let input = this.refInputSearchMap.current
        let selectSearch = this.refSlcTypeSearchMap.current
        let selectObjectTributary = this.refSlcObjetoTributario.current
        let button = this.refInputSearchObjectTributari.current
        let buttonCancel = this.refInputCancelObjectTributari.current
        this.setState({ searchData: true });

        input.setAttribute("readonly", true)
        selectSearch.setAttribute("disabled", true)
        selectObjectTributary.setAttribute("disabled", true)
        button.classList.add("btn-disabled")
        button.setAttribute("disabled", true)

        buttonCancel.classList.add("btn-disabled")
        buttonCancel.setAttribute("disabled", true)

        const respInmueble = await this.props.fetch.axiosAsyncGet(`api-ruat/inmuebles/nro-inmueble/${input.value}`)
        if (Boolean(respInmueble) && respInmueble.status === true) {
            const propietarios = respInmueble.data.resInmueble.propiedades
            const form = new FormData();
            let codigosPropiedad = ""
            propietarios.forEach(propietario => { codigosPropiedad += propietario.codigoPropiedad + ":::" });
            form.append('propiedades[codigoPropiedad]', codigosPropiedad);

            const resDeudas = await this.props.fetch.fetchPost(form, `api-ruat/inmuebles/deudas`, undefined)
            if (Boolean(resDeudas) && resDeudas.status === true) {
                if (resDeudas.pagoObligatorio) {
                    input.removeAttribute("readonly")
                    selectSearch.removeAttribute("disabled")
                    selectObjectTributary.removeAttribute("disabeybled")
                    button.classList.remove("btn-disabled")
                    button.removeAttribute("disabled")

                    buttonCancel.classList.remove("btn-disabled")
                    buttonCancel.removeAttribute("disabled")

                    this.setState({ message: resDeudas.message, searchData: false });
                } else {
                    let objJsonB64Inmueble = window.objetJsonToBase64(respInmueble.data.resInmueble)
                    let objJsonB64Tecnico = window.objetJsonToBase64(respInmueble.data.resTecnico)
                    let objJsonB64Deuda = window.objetJsonToBase64(resDeudas.data.gestionesDeuda)

                    localStorage.setItem('datosInmueble', objJsonB64Inmueble);
                    localStorage.setItem('datosTecnicos', objJsonB64Tecnico);
                    localStorage.setItem('datosDeuda', objJsonB64Deuda);

                    localStorage.setItem('objectTributary', selectObjectTributary.selectedOptions[0].value);
                    localStorage.setItem('typeSearchInmuble', selectSearch.selectedOptions[0].value);
                    localStorage.setItem('nroInmueble', input.value);

                    //escondemos a los formularios
                    this.setState({ message: resDeudas.message, searchData: false });
                    this.props.showTerminosCondiciones(true)
                }
            }
        } else {
            input.removeAttribute("readonly")
            selectSearch.removeAttribute("disabled")
            selectObjectTributary.removeAttribute("disabeybled")
            button.classList.remove("btn-disabled")
            button.removeAttribute("disabled")

            buttonCancel.classList.remove("btn-disabled")
            buttonCancel.removeAttribute("disabled")
            this.setState({ searchData: false });
        }
    }

    render() {
        return (
            <>
                <div className="row">
                    <div className="col-12 padding-right-0">
                        <img className='rounded mx-auto d-block' alt='extension-prescripcion' src={"./static/img/extincion_prescripcion.png"} width="150" />
                        <h4 className="text-center mt-3 mb-3">Objetos Tributarios</h4>
                    </div>
                </div>

                {
                    this.state.message !== "" ?
                        <div className="row">
                            <div className="col-md-12 col-lg-12 col-xl-12">
                                <div className="alert alert-danger alert-dismissible">
                                    <button type="button" className="close" data-dismiss="alert">&times;</button>
                                    <strong>Atención!</strong> {this.state.message}
                                </div>
                            </div>
                        </div>
                        : ""
                }

                <div className="row">
                    <div className="col-sm-12 mb-3">
                        <select className="form-control" name={'[id_objeto_tributario]'} required data-parsley-required="true"
                            ref={this.refSlcObjetoTributario} >
                            <option defaultValue value="">Seleccione Objeto Tributario</option>
                            {this.state.optionsObjetoTributario}
                        </select>
                    </div>

                    <div className="col-sm-12">
                        <div className="input-group mb-3">
                            <div className="input-group-prepend">
                                <select className="form-control" id="slcTypeSearchMap" ref={this.refSlcTypeSearchMap}>
                                    <option defaultValue value="nro_ruat">Nro. RUAT</option>
                                    <option value="cod_cat">Cod. Cat.</option>
                                </select>
                            </div>
                            <input name={'[numero]'} id={'[numero]'} type="text" className="form-control input-uppercase" placeholder="Número"
                                aria-label="Número" aria-describedby="button-addon1" ref={this.refInputSearchMap} />
                        </div>
                    </div>
                </div>

                {
                    this.state.searchData ?
                        <div className="row">
                            <div className="col-sm-12  mb-1">
                                <img className='rounded mx-auto d-block' alt='pulse-loading' src={"./static/img/pulse_200.svg"} width="70" />
                            </div>
                        </div> : ""}

                <div className="row mt-3">
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6 ">
                        <input name="submit" type="button" className="button-style pull-left margin-buttom-15 mb-2 margin-right-3-responsive"
                            value="Buscar" style={{ marginLeft: '0px' }} ref={this.refInputSearchObjectTributari} onClick={this.handleSearchInmuebleClick} />
                    </div>
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6  ">
                        <input type="button" className="button-style pull-right " value="Cancelar" ref={this.refInputCancelObjectTributari} onClick={this.props.handleCancelClick} />
                    </div>
                </div>
            </>
        )
    }
}
export default ValidateDeuda;