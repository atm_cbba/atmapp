import React, { Component } from 'react';

class ValidateNotification extends Component {

    constructor(props, context) {
        super(props, context);

        this.handleEnviarDatosNotificacionClick = this.handleEnviarDatosNotificacionClick.bind(this)

        this.refInputEmail = React.createRef()
    }

    handleEnviarDatosNotificacionClick(event) {
        event.preventDefault()
        this.props.validateForm();
        let email = this.refInputEmail.current
        //var ul_error_dir = email.parentElement.parentElement.querySelector('ul');
        //email.parentElement.parentElement.querySelector('ul').remove();
        //email.closest('.form-group').appendChild(ul_error_dir);
        //window.document.getElementsByClassName("parsley-required")[0].style.color = "white"
        if (window.jQuery("#formModalFur").parsley().isValid()) {
            let emailNotificacionContribuyente = window.objetJsonToBase64(email.value)
            localStorage.setItem('emailNotificacion', emailNotificacionContribuyente);
            window.jQuery('#modalFurFull').modal('hide');
        }
    }

    render() {
        return (
            <>
                <div className="row">
                    <div className="col-12 padding-right-0">
                        <img className='rounded mx-auto d-block' alt='terminos-condiciones' src={"./static/img/terms-and-conditions.png"} width="150" />
                        <h4 className="text-center mt-3 mb-3">Datos Notificación</h4>
                    </div>
                </div>

                <div className="row">
                    <div className="col-sm-12 ">
                        <div className="form-group">
                            <div className="input-group ">
                                <div className="input-group-prepend">
                                    <span className="input-group-text" id="basic-addon1">@</span>
                                </div>
                                <input name={'[email]'} id={'[email]'} type="email" className="form-control"
                                    placeholder="Correo Electronico" data-parsley-required="true" required
                                    aria-label="Correo Electronico" aria-describedby="button-addon1" ref={this.refInputEmail} />

                            </div>
                        </div>
                    </div>
                </div>

                <div className="row mt-3">
                    <div className="col-12 ">
                        <input name="button" type="button" className="button-style pull-right margin-buttom-15 mb-2"
                            value="Enviar" style={{ marginLeft: '0px' }} onClick={this.handleEnviarDatosNotificacionClick} />
                    </div>
                </div>
            </>
        )
    }
}
export default ValidateNotification;