import React, { Component } from 'react';

import Config from '../../data/config';
import Constant from '../../data/constant';
import Links from '../../data/link';
import Fetch from '../utils/Fetch';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

var _fetch = null
class ModalFur extends Component {

    constructor(props, context) {
        super(props, context);

        this.handleFurClick = this.handleFurClick.bind(this)
        this.handleFurOnChange = this.handleFurOnChange.bind(this)
        this.handleVoidEnter = this.handleVoidEnter.bind(this)
        this.handleCancelClick = this.handleCancelClick.bind(this)
        this.handleSearchInmuebleClick = this.handleSearchInmuebleClick.bind(this)
        this.handleAceptoTerminosCondicionesClick = this.handleAceptoTerminosCondicionesClick.bind(this)
        this.handleEnviarDatosNotificacionClick = this.handleEnviarDatosNotificacionClick.bind(this)

        this.state = {
            show: false,
            verifyFur: false,
            optionsObjetoTributario: "",
            showObjectTributary: false,
            searchData: false,
            showTerminosCondiciones: false,
            showFormTermCondition: false,
            message: ""
        };

        this.typeFur = this.props.typeFur
        this.textTypeDerechoAdmisión = Links[1].title

        if (this.typeFur === "PRESCRIPCION")
            this.textTypeDerechoAdmisión = Links[3].title

        _fetch = new Fetch();
        _fetch.setToast(toast);

        this.refSlcObjetoTributario = React.createRef();
        this.refSlcTypeSearchMap = React.createRef()
        this.refInputSearchMap = React.createRef()
        this.refInputSearchObjectTributari = React.createRef()
        this.refInputCancelObjectTributari = React.createRef()
        this.refInputEmail = React.createRef()
    }

    componentDidMount(){
        debugger
        localStorage.removeItem('datosInmueble');
        localStorage.removeItem('datosTecnicos');
        localStorage.removeItem('datosDeuda');
        localStorage.removeItem('inmuebleCatastro');
        localStorage.removeItem('objectTributary');
        localStorage.removeItem('typeSearchInmuble');
        localStorage.removeItem('typeSearchInmuble');
        localStorage.removeItem('emailNotificacion');
    }

    /*componentDidUpdate(prevProps, prevState, snapshot) {
        if (window.document.getElementsByClassName("parsley-required").length > 0)
            window.document.getElementsByClassName("parsley-required")[0].style.color = "white"
    }*/

    handleFurClick(event) {
        event.preventDefault();
        this.validateForm();

        if (this.state.verifyFur) {
            if (this.typeFur === "PRESCRIPCION") {
                _fetch.fetchGet('api/cobros/fur-prescripciones/' + this.state.inputFur).then(fur => {
                    if (fur !== undefined && fur.status === true && fur.Fur !== null) {  //is ok
                        this.configureFurCookie(fur, Constant[0].modules.prescripcion)
                        this.loadObjetoTributario()
                        this.setState({ showObjectTributary: true });
                    }
                })
            } else {
                _fetch.fetchGet('api/Cobros/getFur/' + this.state.inputFur).then(fur => {
                    if (fur !== undefined && fur.status === true && fur.Fur !== null) {  //is ok
                        this.configureFurCookie(fur, Constant[0].modules.actividad_economica)
                    }
                })
            }
        }
    }

    async handleSearchInmuebleClick(event) {
        event.preventDefault()
        let input = this.refInputSearchMap.current
        let selectSearch = this.refSlcTypeSearchMap.current
        let selectObjectTributary = this.refSlcObjetoTributario.current
        let button = this.refInputSearchObjectTributari.current
        let buttonCancel = this.refInputCancelObjectTributari.current
        this.setState({ searchData: true });

        input.setAttribute("readonly", true)
        selectSearch.setAttribute("disabled", true)
        selectObjectTributary.setAttribute("disabled", true)
        button.classList.add("btn-disabled")
        button.setAttribute("disabled", true)

        buttonCancel.classList.add("btn-disabled")
        buttonCancel.setAttribute("disabled", true)

        const respInmueble = await _fetch.axiosAsyncGet(`api-ruat/inmuebles/nro-inmueble/${input.value}`)
        if (Boolean(respInmueble) && respInmueble.status === true) {
            const propietarios = respInmueble.data.resInmueble.propiedades
            const form = new FormData();
            let codigosPropiedad = ""
            propietarios.forEach(propietario => { codigosPropiedad += propietario.codigoPropiedad + ":::" });
            form.append('propiedades[codigoPropiedad]', codigosPropiedad);

            const resDeudas = await _fetch.fetchPost(form, `api-ruat/inmuebles/deudas`, undefined)
            if (Boolean(resDeudas) && resDeudas.status === true) {
                if (resDeudas.pagoObligatorio) {
                    input.removeAttribute("readonly")
                    selectSearch.removeAttribute("disabled")
                    selectObjectTributary.removeAttribute("disabeybled")
                    button.classList.remove("btn-disabled")
                    button.removeAttribute("disabled")

                    buttonCancel.classList.remove("btn-disabled")
                    buttonCancel.removeAttribute("disabled")

                    this.setState({ message: resDeudas.message, searchData: false });
                } else {
                    debugger
                    let objJsonB64Inmueble = window.objetJsonToBase64(respInmueble.data.resInmueble)
                    let objJsonB64Tecnico = window.objetJsonToBase64(respInmueble.data.resTecnico)
                    let objJsonB64Deuda = window.objetJsonToBase64(resDeudas.data.gestionesDeuda)

                    localStorage.setItem('datosInmueble', objJsonB64Inmueble);
                    localStorage.setItem('datosTecnicos', objJsonB64Tecnico);
                    localStorage.setItem('datosDeuda', objJsonB64Deuda);

                    localStorage.setItem('objectTributary', selectObjectTributary.selectedOptions[0].value);
                    localStorage.setItem('typeSearchInmuble', selectSearch.selectedOptions[0].value);
                    localStorage.setItem('nroInmueble', input.value);

                    //escondemos a los formularios
                    this.setState({ message: resDeudas.message, showTerminosCondiciones: true, searchData: false });
                }
            }
        } else {
            input.removeAttribute("readonly")
            selectSearch.removeAttribute("disabled")
            selectObjectTributary.removeAttribute("disabeybled")
            button.classList.remove("btn-disabled")
            button.removeAttribute("disabled")

            buttonCancel.classList.remove("btn-disabled")
            buttonCancel.removeAttribute("disabled")
            this.setState({ searchData: false });
        }
    }

    handleAceptoTerminosCondicionesClick(event) {
        event.preventDefault()
        this.setState({ searchData: false, showFormTermCondition: true });
    }

    handleEnviarDatosNotificacionClick(event) {
        event.preventDefault()
        this.validateForm();
        let email = this.refInputEmail.current
        //var ul_error_dir = email.parentElement.parentElement.querySelector('ul');
        //email.parentElement.parentElement.querySelector('ul').remove();
        //email.closest('.form-group').appendChild(ul_error_dir);
        //window.document.getElementsByClassName("parsley-required")[0].style.color = "white"
        if (window.jQuery("#formModalFur").parsley().isValid()) {
            let emailNotificacionContribuyente = window.objetJsonToBase64(email.value)
            localStorage.setItem('emailNotificacion', emailNotificacionContribuyente);
            window.jQuery('#modalFurFull').modal('hide');
        }
    }

    configureFurCookie(fur, module) {
        //window.jQuery('#modalFurFull').modal('hide');
        var input_fur = document.getElementById("inputModalFur");
        //modificar con moment
        var current_date = new Date();
        current_date.setHours(current_date.getHours() + 1);
        var today = new Date(current_date).toUTCString();
        document.cookie = "fur=" + input_fur.value + "; expires=" + today.toString();

        if (module === Constant[0].modules.prescripcion) {
            let derecho_admision
            if (fur.derecho_admision === 'PRES_NATURAL')
                derecho_admision = Constant[0].derecho_admision.natural
            if (fur.derecho_admision === 'PRES_JURIDICO')
                derecho_admision = Constant[0].derecho_admision.juridico

            window.eliminarCookie('da')
            document.cookie = "dapres=" + derecho_admision + "; expires=" + today.toString();
        }

        if (module === Constant[0].modules.actividad_economica) {
            window.jQuery('#modalFurFull').modal('hide');
            window.eliminarCookie('dapres')
            let derecho_admision = fur.derecho_admision === 'TEMPORAL' ? Constant[0].derecho_admision.temporal : Constant[0].derecho_admision.permanente
            document.cookie = "da=" + derecho_admision + "; expires=" + today.toString();
        }
        //modificar con moment
        this.props.toast.success(fur.message, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
        });
    }

    handleCancelClick(event) {
        event.preventDefault()
        if (this.typeFur === "PRESCRIPCION")
            window.location.href = Links[3].url
        else
            window.location.href = Links[1].url;
    }

    handleVoidEnter(e) {
        e.preventDefault();
        this.validateForm();
    }

    handleFurOnChange(event) {

        event.preventDefault();
        var button = document.getElementById("btnModalSmallSubmit");

        if (event.currentTarget.value.length >= 3) {
            button.classList.remove("btn-disabled");
            this.setState({ verifyFur: true, inputFur: event.currentTarget.value });
        } else {
            button.classList.add("btn-disabled");
            this.setState({ verifyFur: false, inputFur: '' });
        }
    }

    validateForm = () => {
        window.jQuery("#formModalFur").parsley().validate();

        var li_error = window.jQuery("#inputModalFur").siblings('ul');
        window.jQuery("#inputModalFur").siblings('ul').remove();
        window.jQuery("#inputModalFur").parent('div').parent('div').append(li_error);
    }

    loadObjetoTributario = () => {
        let self = this
        const objet_tributario_res = _fetch.fetchGet(`api/prescripcion/objeto-tributario/all`);
        objet_tributario_res.then(res => {
            if (res !== undefined && res !== undefined && res.status === true) {
                if (res.status === true && res.ObjetoTributario !== null) {
                    const listItems = res.ObjetoTributario.map((item, index) => {
                        return <option key={index} value={item.id} titleinput={item.description} code={item.code} >{item.name}</option>
                    });
                    self.setState({ optionsObjetoTributario: listItems });
                }
            }
        })
    }

    render() {
        return (
            <div className="modal fade bd-example-modal-md background-cherry-dark" id="modalFurFull" tabIndex="-1" role="dialog"
                aria-labelledby="exampleModalCenterTitle" aria-hidden="true" data-backdrop="static" data-keyboard="false">
                <div className="modal-dialog modal-dialog-atm modal-md" style={{ boxShadow: "0px 0px 50px 0px rgba(0,0,0,0.75)" }}>
                    <div className="modal-content background-silver-pink">
                        <div className="modal-header text-white justify-content-center">
                            {!this.state.showObjectTributary ?
                                <h4 className="modal-title " id="exampleModalLongTitle" style={{ fontWeight: 600 }}>VALIDACIÓN</h4>
                                : <h4 className="modal-title " id="exampleModalLongTitle" style={{ fontWeight: 600 }}>PRESCRIPCION</h4>}

                        </div>
                        <div className="modal-body text-white">
                            <form className="contact__form" style={{ margin: "0.5rem" }} onSubmit={this.handleVoidEnter} id="formModalFur" >
                                {
                                    !this.state.showObjectTributary ?
                                        <div className="row">
                                            <div className="col-md-12 col-lg-12 col-xl-12 padding-right-0 text-center mt-4 mb-4">
                                                <i className="fa fa-file-text-o " aria-hidden="true" style={{ fontSize: '8.5rem' }}></i>
                                                <h4 className="text-center mt-4">Derecho de Admisión</h4>
                                            </div>
                                            <div className="col-12 padding-right-0">
                                                2 Declaraciones Juradas por cuenta.
                                            </div>
                                        </div>
                                        :
                                        ""
                                }

                                {
                                    !this.state.showTerminosCondiciones && this.state.showObjectTributary ?
                                        <div className="row">
                                            <div className="col-12 padding-right-0">
                                                <img className='rounded mx-auto d-block' alt='extension-prescripcion' src={"./static/img/extincion_prescripcion.png"} width="150" />
                                                <h4 className="text-center mt-3 mb-3">Objetos Tributarios</h4>
                                            </div>
                                        </div>
                                        :
                                        ""
                                }

                                {
                                    this.state.showTerminosCondiciones && !this.state.showFormTermCondition ?
                                        <div className="row">
                                            <div className="col-12 padding-right-0">
                                                <img className='rounded mx-auto d-block' alt='terminos-condiciones' src={"./static/img/terms-and-conditions.png"} width="150" />
                                                <h4 className="text-center mt-3 mb-3">Términos y Condiciones de Uso</h4>
                                            </div>
                                        </div>
                                        :
                                        ""
                                }

                                {
                                    this.state.showFormTermCondition ?
                                        <div className="row">
                                            <div className="col-12 padding-right-0">
                                                <img className='rounded mx-auto d-block' alt='terminos-condiciones' src={"./static/img/terms-and-conditions.png"} width="150" />
                                                <h4 className="text-center mt-3 mb-3">Datos Notificación</h4>
                                            </div>
                                        </div>
                                        :
                                        ""
                                }

                                {
                                    !this.state.showTerminosCondiciones && this.state.message !== "" ?
                                        <div className="row">
                                            <div className="col-md-12 col-lg-12 col-xl-12">
                                                <div className="alert alert-danger alert-dismissible">
                                                    <button type="button" className="close" data-dismiss="alert">&times;</button>
                                                    <strong>Atención!</strong> {this.state.message}
                                                </div>
                                            </div>
                                        </div>
                                        : ""
                                }

                                {
                                    !this.state.showTerminosCondiciones && !this.state.showObjectTributary ?
                                        <div className="row">
                                            <div className="col-12 col-md-12 col-lg-12">
                                                <div className="form-group">
                                                    <div className="input-group mb-3">
                                                        <div className="input-group-prepend">
                                                            <span className="input-group-text" id="basic-addon1">Nº FUR</span>
                                                        </div>
                                                        <input type="text" className="form-control" placeholder="FUR" aria-label="Fur" id="inputModalFur"
                                                            aria-describedby="basic-addon1" onChange={this.handleFurOnChange} data-parsley-required="true" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div> : ""
                                }

                                {
                                    !this.state.showTerminosCondiciones && this.state.showObjectTributary ?
                                        <div className="row">
                                            <div className="col-sm-12 mb-3">
                                                <select className="form-control" name={'[id_objeto_tributario]'} required data-parsley-required="true"
                                                    ref={this.refSlcObjetoTributario} >
                                                    <option defaultValue value="">Seleccione Objeto Tributario</option>
                                                    {this.state.optionsObjetoTributario}
                                                </select>
                                            </div>

                                            <div className="col-sm-12">
                                                <div className="input-group mb-3">
                                                    <div className="input-group-prepend">
                                                        <select className="form-control" id="slcTypeSearchMap" ref={this.refSlcTypeSearchMap}>
                                                            <option defaultValue value="nro_ruat">Nro. RUAT</option>
                                                            <option value="cod_cat">Cod. Cat.</option>
                                                        </select>
                                                    </div>
                                                    <input name={'[numero]'} id={'[numero]'} type="text" className="form-control input-uppercase" placeholder="Número"
                                                        aria-label="Número" aria-describedby="button-addon1" ref={this.refInputSearchMap} />
                                                </div>
                                            </div>
                                        </div>
                                        : ""
                                }

                                {
                                    this.state.showTerminosCondiciones && !this.state.showFormTermCondition ?
                                        <>
                                            <div className="row pt-3">
                                                <div className="col-12 block-terminos-condiciones-modal">
                                                    <p className="text-justify">Manifiesto haber tomado conocimiento y haber aceptado, todas y cada una de las siguientes condiciones
                                                    de uso y acceso para la Solicitud de Prescripción en la plataforma ATM, a través del cual procederé al seguimiento del tramite
                                                        administrativo de  Prescripción y la notificación mediante medios electrónicos declarados y aceptados en la presente. </p>

                                                    <p className="text-justify">
                                                        De conformidad al Art. 78 de la Ley Nº 164 Ley General de Telecomunicaciones, Tecnología de Información y Comunicación,
                                                        establece que: "Tiene Validez y probatoria:
                                                        1. El acto o negocio jurídico realizado por persona natural o jurídica en documento
                                                        digital y aprobado por las partes a través de firma digital, celebrado por medio electrónico u otro de mayor avance
                                                        tecnológico.
                                                        2. El mensaje electrónico de datos. 3. La firma digital."
                                                        </p>

                                                    <p className="text-justify">
                                                        De conformidad al Paragrafo III, del Art. 2 y el Art. 3 de la Ley Nº 812 de fecha 1 de julio de 2016, de Modificación a
                                                        la Ley Nº 2492 de 2 de agosto de 2003, “Código Tributario Boliviano”,  Los actos y actuaciones de la Administración
                                                        Tributaria Municipal del Gobierno Autonomo Municipal de Cochabamba se notificarán por medios electronicos declarados y
                                                        proporcionados por el sujeto pasivo dentro los Términos y Condiciones de la presente.
                                                        </p>

                                                    <p className="text-justify">
                                                        De conformidad al Art. 7 del Decreto Supremo Nro 27310 de fecha 9 de Enero de 2004, Reglamento a la Ley Nro. 2492 Código
                                                        Tributario Boliviano que señala “Las operaciones electrónicas realizadas y registradas en el sistema informático de la
                                                        Administración Tributaria por un usuario autorizado surten efectos jurídicos. La información generada, enviada, recibida,
                                                        almacenada o comunicada a través de los sistemas informáticos o medios electrónicos, por cualquier usuario autorizado que de
                                                        cómo resultado un registro electrónico, tiene validez probatoria.”
                                                        </p>

                                                    <p className="text-justify">
                                                        Los datos consignados en el formulario de Solicitud de Prescripción proporcionados por los Sistemas Informáticos de la
                                                        Administración Tributaría Municipal, condicen la verdad material de los datos personales y los objetos tributarios asociados
                                                        a mi persona, en caso que no corresponder ME COMPROMETO a proceder a la actualización de los mismos en los registros
                                                        autorizados para tal efecto.
                                                        </p>

                                                    <p className="text-justify">
                                                        En relación al servicio de carácter administrativo, habilitado en la plataforma de la Administración Tributaria Municipal
                                                        ATM, cualquier información que se solicite, será requerida en apego absoluto a la verdad, dado que estos formularios estarán
                                                        consignados como declaraciones juradas, en virtud de lo dispuesto en normativa vigente, adquiriendo plena validez legal,
                                                        incluyendo cualquier requisito documental, que deba ser digitalizado y cargado en la plataforma.
                                                        </p>

                                                    <p className="text-justify">
                                                        Asimismo, el espacio virtual que se me asigne dentro de la plataforma ATM, se constituirá en el medio oficial, mediante el cual,
                                                        la Administración Tributaria Municipal del GAMC podrá notificarle sobre actos y actuaciones administrativas referidas a la
                                                        solicitud de Prescripción, en mérito a lo cual asumo entera responsabilidad y me comprometo a realizar el seguimiento continuo y
                                                        permanente a cualquier alerta o notificación que pudiera recibir por este medio.
                                                        </p>
                                                </div>
                                            </div>
                                        </>
                                        : ""
                                }

                                {
                                    this.state.showFormTermCondition ?
                                        <div className="row">
                                            <div className="col-sm-12 ">
                                                <div className="form-group">
                                                    <div className="input-group ">
                                                        <div className="input-group-prepend">
                                                            <span className="input-group-text" id="basic-addon1">@</span>
                                                        </div>
                                                        <input name={'[email]'} id={'[email]'} type="email" className="form-control"
                                                            placeholder="Correo Electronico" data-parsley-required="true" required
                                                            aria-label="Correo Electronico" aria-describedby="button-addon1" ref={this.refInputEmail} />

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        : ""
                                }

                                {
                                    this.state.searchData ?
                                        <div className="row">
                                            <div className="col-sm-12  mb-1">
                                                <img className='rounded mx-auto d-block' alt='pulse-loading' src={"./static/img/pulse_200.svg"} width="70" />
                                            </div>
                                        </div> : ""}

                                {
                                    !this.state.showTerminosCondiciones && !this.state.showObjectTributary ?
                                        <div className="row mt-3">
                                            <div className="col-12 col-sm-6 col-md-6 col-lg-6 ">
                                                <input name="submit" type="button" className="button-style btn-disabled pull-left mb-3" id="btnModalSmallSubmit"
                                                    value="Verificar" style={{ marginLeft: '0px' }} onClick={this.handleFurClick} />
                                            </div>
                                            <div className="col-12 col-sm-6 col-md-6 col-lg-6  ">
                                                <input type="button" className="button-style pull-right " value="Cancelar" onClick={this.handleCancelClick} />
                                            </div>
                                        </div>
                                        :
                                        ""
                                }

                                {
                                    !this.state.showTerminosCondiciones && this.state.showObjectTributary ?
                                        <div className="row mt-3">
                                            <div className="col-12 col-sm-6 col-md-6 col-lg-6 ">
                                                <input name="submit" type="button" className="button-style pull-left margin-buttom-15 mb-2 margin-right-3-responsive"
                                                    value="Buscar" style={{ marginLeft: '0px' }} ref={this.refInputSearchObjectTributari} onClick={this.handleSearchInmuebleClick} />
                                            </div>
                                            <div className="col-12 col-sm-6 col-md-6 col-lg-6  ">
                                                <input type="button" className="button-style pull-right " value="Cancelar" ref={this.refInputCancelObjectTributari} onClick={this.handleCancelClick} />
                                            </div>
                                        </div>
                                        :
                                        ""
                                }

                                {
                                    this.state.showTerminosCondiciones && !this.state.showFormTermCondition ?
                                        <div className="row mt-3">
                                            <div className="col-12 col-sm-6 col-md-6 col-lg-6 ">
                                                <input name="submit" type="button" className="button-style pull-left margin-buttom-15 mb-2 margin-right-3-responsive"
                                                    value="Acepto" style={{ marginLeft: '0px' }} onClick={this.handleAceptoTerminosCondicionesClick} />
                                            </div>
                                            <div className="col-12 col-sm-6 col-md-6 col-lg-6  ">
                                                <input type="button" className="button-style pull-right " value="No Acepto"
                                                    onClick={this.handleCancelClick} />
                                            </div>
                                        </div>
                                        :
                                        ""
                                }

                                {
                                    this.state.showFormTermCondition ?
                                        <div className="row mt-3">
                                            <div className="col-12 ">
                                                <input name="button" type="button" className="button-style pull-right margin-buttom-15 mb-2"
                                                    value="Enviar" style={{ marginLeft: '0px' }} onClick={this.handleEnviarDatosNotificacionClick} />
                                            </div>
                                        </div>
                                        :
                                        ""
                                }

                            </form>
                        </div>
                    </div>
                </div>
                <div className="force-footer">
                    <div className="row">
                        <div className="col-12 col-md-6">
                            <p>Version 1.0</p>
                        </div>

                        <div className="col-12 col-md-6 text-right">
                            <p>Copyright @ ATM, Todos los derechos reservados. <img src={"/static/img/logo_atm.png"} className="rounded-circle z-depth-0 logo-user" alt="avatar image" /></p>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default ModalFur;