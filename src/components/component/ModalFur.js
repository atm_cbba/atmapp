import React, { Component } from 'react';

import Config from '../../data/config';
import Constant from '../../data/constant';
import Links from '../../data/link';
import Fetch from '../utils/Fetch';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import ValidateFur from './ValidateFur'
import ValidateDeuda from './ValidateDeuda'
import ValidateTerminosCondiciones from './ValidateTerminosCondiciones';
import ValidateNotification from './ValidateNotification';

var _fetch = null
class ModalFur extends Component {

    constructor(props, context) {
        super(props, context);

        this.handleVoidEnter = this.handleVoidEnter.bind(this)
        this.handleCancelClick = this.handleCancelClick.bind(this)

        this.state = {
            showObjectTributary: false,
            showTerminosCondiciones: false,
            showFormTermCondition: false,
        };

        this.typeFur = this.props.typeFur
        this.textTypeDerechoAdmisión = Links[1].title

        if (this.typeFur === "PRESCRIPCION")
            this.textTypeDerechoAdmisión = Links[3].title

        _fetch = new Fetch();
        _fetch.setToast(toast);
    }

    componentDidMount() {
        localStorage.removeItem('datosInmueble');
        localStorage.removeItem('datosTecnicos');
        localStorage.removeItem('datosDeuda');
        localStorage.removeItem('inmuebleCatastro');
        localStorage.removeItem('objectTributary');
        localStorage.removeItem('typeSearchInmuble');
        localStorage.removeItem('typeSearchInmuble');
        localStorage.removeItem('emailNotificacion');
    }


    handleCancelClick(event) {
        event.preventDefault()
        if (this.typeFur === "PRESCRIPCION")
            window.location.href = Links[3].url
        else
            window.location.href = Links[1].url;
    }

    handleVoidEnter(e) {
        e.preventDefault();
        this.validateForm();
    }

    validateForm = () => {
        window.jQuery("#formModalFur").parsley().validate();
        var li_error = window.jQuery("#inputModalFur").siblings('ul');
        window.jQuery("#inputModalFur").siblings('ul').remove();
        window.jQuery("#inputModalFur").parent('div').parent('div').append(li_error);
    }

    toogleShowObjectTributary = () => {
        this.setState(state => ({ showObjectTributary: !state.showObjectTributary }));
    }

    toogleShowTerminosCondiciones = () => {
        this.setState(state => ({ showTerminosCondiciones: !state.showTerminosCondiciones }));
    }

    toogleShowFormTermCondition = () => {
        this.setState(state => ({ showFormTermCondition: !state.showFormTermCondition }));
    }

    render() {
        return (
            <div className="modal fade bd-example-modal-md background-cherry-dark" id="modalFurFull" tabIndex="-1" role="dialog"
                aria-labelledby="exampleModalCenterTitle" aria-hidden="true" data-backdrop="static" data-keyboard="false">
                <div className="modal-dialog modal-dialog-atm modal-md" style={{ boxShadow: "0px 0px 50px 0px rgba(0,0,0,0.75)" }}>
                    <div className="modal-content background-silver-pink">
                        <div className="modal-header text-white justify-content-center">
                            {!this.state.showObjectTributary ?
                                <h4 className="modal-title " id="exampleModalLongTitle" style={{ fontWeight: 600 }}>VALIDACIÓN</h4>
                                : <h4 className="modal-title " id="exampleModalLongTitle" style={{ fontWeight: 600 }}>PRESCRIPCION</h4>}

                        </div>
                        <div className="modal-body text-white">
                            <form className="contact__form" style={{ margin: "0.5rem" }} onSubmit={this.handleVoidEnter} id="formModalFur" >

                                {
                                    !this.state.showObjectTributary ?
                                        <ValidateFur handleCancelClick={this.handleCancelClick} fetch={_fetch} validateForm={this.validateForm}
                                            typeFur={this.typeFur} showObjectTributary={this.toogleShowObjectTributary} />
                                        : ""
                                }

                                {
                                    !this.state.showTerminosCondiciones && this.state.showObjectTributary ?
                                        <ValidateDeuda handleCancelClick={this.handleCancelClick} fetch={_fetch} validateForm={this.validateForm}
                                            showTerminosCondiciones={this.toogleShowTerminosCondiciones}
                                        />
                                        : ""
                                }

                                {
                                    this.state.showTerminosCondiciones && !this.state.showFormTermCondition ?
                                        <ValidateTerminosCondiciones handleCancelClick={this.handleCancelClick} fetch={_fetch}
                                            showFormTermCondition={this.toogleShowFormTermCondition}
                                        />
                                        : ""
                                }

                                {
                                    this.state.showFormTermCondition ?
                                        <ValidateNotification fetch={_fetch} validateForm={this.validateForm}
                                        />
                                        : ""
                                }
                            </form>
                        </div>
                    </div>
                </div>
                <div className="force-footer">
                    <div className="row">
                        <div className="col-12 col-md-6">
                            <p>Version 1.0</p>
                        </div>

                        <div className="col-12 col-md-6 text-right">
                            <p>Copyright @ ATM, Todos los derechos reservados. <img src={"/static/img/logo_atm.png"} className="rounded-circle z-depth-0 logo-user" alt="avatar image" /></p>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default ModalFur;