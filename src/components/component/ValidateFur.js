import React, { Component } from 'react';
import Constant from '../../data/constant';

class ValidateFur extends Component {

    constructor(props, context) {
        super(props, context);

        this.handleFurClick = this.handleFurClick.bind(this)
        this.handleFurOnChange = this.handleFurOnChange.bind(this)

        this.state = {
            verifyFur: false,
            inputFur: ""
        };
    }

    handleFurOnChange(event) {

        event.preventDefault();
        var button = document.getElementById("btnModalSmallSubmit");

        if (event.currentTarget.value.length >= 3) {
            button.classList.remove("btn-disabled");
            this.setState({ verifyFur: true, inputFur: event.currentTarget.value });
        } else {
            button.classList.add("btn-disabled");
            this.setState({ verifyFur: false, inputFur: '' });
        }
    }

    handleFurClick(event) {
        event.preventDefault();
        this.props.validateForm();

        if (this.state.verifyFur) {
            if (this.props.typeFur === "PRESCRIPCION") {
                this.props.fetch.fetchGet('api/cobros/fur-prescripciones/' + this.state.inputFur).then(fur => {
                    if (fur !== undefined && fur.status === true && fur.Fur !== null) {  //is ok
                        this.configureFurCookie(fur, Constant[0].modules.prescripcion)
                        this.props.showObjectTributary(true)
                    }
                })
            } else {
                this.props.fetch.fetchGet('api/Cobros/getFur/' + this.state.inputFur).then(fur => {
                    if (fur !== undefined && fur.status === true && fur.Fur !== null) {  //is ok
                        this.configureFurCookie(fur, Constant[0].modules.actividad_economica)
                    }
                })
            }
        }
    }

    configureFurCookie(fur, module) {
        //window.jQuery('#modalFurFull').modal('hide');
        var input_fur = document.getElementById("inputModalFur");
        //modificar con moment
        var current_date = new Date();
        current_date.setHours(current_date.getHours() + 1);
        var today = new Date(current_date).toUTCString();
        document.cookie = "fur=" + input_fur.value + "; expires=" + today.toString();

        if (module === Constant[0].modules.prescripcion) {
            let derecho_admision
            if (fur.derecho_admision === 'PRES_NATURAL')
                derecho_admision = Constant[0].derecho_admision.natural
            if (fur.derecho_admision === 'PRES_JURIDICO')
                derecho_admision = Constant[0].derecho_admision.juridico

            window.eliminarCookie('da')
            document.cookie = "dapres=" + derecho_admision + "; expires=" + today.toString();
        }

        if (module === Constant[0].modules.actividad_economica) {
            window.jQuery('#modalFurFull').modal('hide');
            window.eliminarCookie('dapres')
            let derecho_admision = fur.derecho_admision === 'TEMPORAL' ? Constant[0].derecho_admision.temporal : Constant[0].derecho_admision.permanente
            document.cookie = "da=" + derecho_admision + "; expires=" + today.toString();
        }
        //modificar con moment
        this.props.fetch.toast.success(fur.message, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
        });
    }

    render() {
        return (
            <>
                <div className="row">
                    <div className="col-md-12 col-lg-12 col-xl-12 padding-right-0 text-center mt-4 mb-4">
                        <i className="fa fa-file-text-o " aria-hidden="true" style={{ fontSize: '8.5rem' }}></i>
                        <h4 className="text-center mt-4">Derecho de Admisión</h4>
                    </div>
                    <div className="col-12 padding-right-0">
                        2 Declaraciones Juradas por cuenta.
                    </div>
                </div>

                <div className="row">
                    <div className="col-12 col-md-12 col-lg-12">
                        <div className="form-group">
                            <div className="input-group mb-3">
                                <div className="input-group-prepend">
                                    <span className="input-group-text" id="basic-addon1">Nº FUR</span>
                                </div>
                                <input type="text" className="form-control" placeholder="FUR" aria-label="Fur" id="inputModalFur"
                                    aria-describedby="basic-addon1" onChange={this.handleFurOnChange} data-parsley-required="true" />
                            </div>
                        </div>
                    </div>
                </div>

                <div className="row mt-3">
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6 ">
                        <input name="submit" type="button" className="button-style btn-disabled pull-left mb-3" id="btnModalSmallSubmit"
                            value="Verificar" style={{ marginLeft: '0px' }} onClick={this.handleFurClick} />
                    </div>
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6  ">
                        <input type="button" className="button-style pull-right " value="Cancelar" onClick={this.props.handleCancelClick} />
                    </div>
                </div>
            </>
        )
    }
}
export default ValidateFur;