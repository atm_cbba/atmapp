import React, { Component } from 'react';

class ValidateTerminosCondiciones extends Component {

    constructor(props, context) {
        super(props, context);

        this.handleAceptoTerminosCondicionesClick = this.handleAceptoTerminosCondicionesClick.bind(this)
    }

    handleAceptoTerminosCondicionesClick(event) {
        event.preventDefault()
        this.props.showFormTermCondition(true)
    }

    render() {
        return (
            <>
                <div className="row">
                    <div className="col-12 padding-right-0">
                        <img className='rounded mx-auto d-block' alt='terminos-condiciones' src={"./static/img/terms-and-conditions.png"} width="150" />
                        <h4 className="text-center mt-3 mb-3">Términos y Condiciones de Uso</h4>
                    </div>
                </div>

                <div className="row pt-3">
                    <div className="col-12 block-terminos-condiciones-modal">
                        <p className="text-justify">Manifiesto haber tomado conocimiento y haber aceptado, todas y cada una de las siguientes condiciones
                        de uso y acceso para la Solicitud de Prescripción en la plataforma ATM, a través del cual procederé al seguimiento del tramite
                                                        administrativo de  Prescripción y la notificación mediante medios electrónicos declarados y aceptados en la presente. </p>

                        <p className="text-justify">
                            De conformidad al Art. 78 de la Ley Nº 164 Ley General de Telecomunicaciones, Tecnología de Información y Comunicación,
                            establece que: "Tiene Validez y probatoria:
                            1. El acto o negocio jurídico realizado por persona natural o jurídica en documento
                            digital y aprobado por las partes a través de firma digital, celebrado por medio electrónico u otro de mayor avance
                            tecnológico.
                            2. El mensaje electrónico de datos. 3. La firma digital."
                                                        </p>

                        <p className="text-justify">
                            De conformidad al Paragrafo III, del Art. 2 y el Art. 3 de la Ley Nº 812 de fecha 1 de julio de 2016, de Modificación a
                            la Ley Nº 2492 de 2 de agosto de 2003, “Código Tributario Boliviano”,  Los actos y actuaciones de la Administración
                            Tributaria Municipal del Gobierno Autonomo Municipal de Cochabamba se notificarán por medios electronicos declarados y
                            proporcionados por el sujeto pasivo dentro los Términos y Condiciones de la presente.
                                                        </p>

                        <p className="text-justify">
                            De conformidad al Art. 7 del Decreto Supremo Nro 27310 de fecha 9 de Enero de 2004, Reglamento a la Ley Nro. 2492 Código
                            Tributario Boliviano que señala “Las operaciones electrónicas realizadas y registradas en el sistema informático de la
                            Administración Tributaria por un usuario autorizado surten efectos jurídicos. La información generada, enviada, recibida,
                            almacenada o comunicada a través de los sistemas informáticos o medios electrónicos, por cualquier usuario autorizado que de
                            cómo resultado un registro electrónico, tiene validez probatoria.”
                                                        </p>

                        <p className="text-justify">
                            Los datos consignados en el formulario de Solicitud de Prescripción proporcionados por los Sistemas Informáticos de la
                            Administración Tributaría Municipal, condicen la verdad material de los datos personales y los objetos tributarios asociados
                            a mi persona, en caso que no corresponder ME COMPROMETO a proceder a la actualización de los mismos en los registros
                            autorizados para tal efecto.
                                                        </p>

                        <p className="text-justify">
                            En relación al servicio de carácter administrativo, habilitado en la plataforma de la Administración Tributaria Municipal
                            ATM, cualquier información que se solicite, será requerida en apego absoluto a la verdad, dado que estos formularios estarán
                            consignados como declaraciones juradas, en virtud de lo dispuesto en normativa vigente, adquiriendo plena validez legal,
                            incluyendo cualquier requisito documental, que deba ser digitalizado y cargado en la plataforma.
                                                        </p>

                        <p className="text-justify">
                            Asimismo, el espacio virtual que se me asigne dentro de la plataforma ATM, se constituirá en el medio oficial, mediante el cual,
                            la Administración Tributaria Municipal del GAMC podrá notificarle sobre actos y actuaciones administrativas referidas a la
                            solicitud de Prescripción, en mérito a lo cual asumo entera responsabilidad y me comprometo a realizar el seguimiento continuo y
                            permanente a cualquier alerta o notificación que pudiera recibir por este medio.
                                                        </p>
                    </div>
                </div>

                <div className="row mt-3">
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6 ">
                        <input name="submit" type="button" className="button-style pull-left margin-buttom-15 mb-2 margin-right-3-responsive"
                            value="Acepto" style={{ marginLeft: '0px' }} onClick={this.handleAceptoTerminosCondicionesClick} />
                    </div>
                    <div className="col-12 col-sm-6 col-md-6 col-lg-6  ">
                        <input type="button" className="button-style pull-right " value="No Acepto"
                            onClick={this.props.handleCancelClick} />
                    </div>
                </div>
            </>
        )
    }
}
export default ValidateTerminosCondiciones;