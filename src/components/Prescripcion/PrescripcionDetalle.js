import React, { useEffect, useState } from 'react'
import Fetch from '../../components/utils/Fetch'
import AuthService from '../../components/Usuario/AuthService'

import DataPropietario from './DataPropietario'
import DataTecnicos from './DataTecnicos'
import DataLiquidacion from './DataLiquidacion'

import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

import Links from '../../data/link'
import TitlePage from '../../components/utils/TitlePage'
import Texto from '../../data/es'
import DataHistorial from './DataHistorial'

const PrescripcionDetalle = (props) => {

    const fetch = new Fetch()
    fetch.setToast(toast)
    const auth = new AuthService()

    const [dataInmueble, setDataInmueble] = useState(undefined)
    const [dataTecnicosInmueble, setDataTecnicosInmueble] = useState(undefined)
    const [dataDeudaInmueble, setDataDeudaInmueble] = useState(undefined)
    const [dataInmuebleCatastro, setDataInmuebleCatastro] = useState(undefined)
    const [dataHistorial, setDataHistorial] = useState(undefined)

    useEffect(() => {
        loadInmueble()
    }, []);

    const loadInmueble = async () => {
        const respInmueble = await fetch.axiosAsyncGet(`api-ruat/inmuebles/nro-inmueble/${100109}`)
        if (Boolean(respInmueble) && respInmueble.status === true) {
            const propietarios = respInmueble.data.resInmueble.propiedades
            const form = new FormData();
            let codigosPropiedad = ""
            propietarios.forEach(propietario => { codigosPropiedad += propietario.codigoPropiedad + ":::" });
            form.append('propiedades[codigoPropiedad]', codigosPropiedad);

            const tokenPrescripcion = localStorage.getItem('tokenPrescripcion');
            const resDeudas = await fetch.fetchGet(`api/prescripcion/objetos-tributarios-gestion/token-prescripcion/${tokenPrescripcion}`)
            if (Boolean(resDeudas) && resDeudas.status === true) {

                setDataInmueble(respInmueble.data.resInmueble)
                setDataTecnicosInmueble(respInmueble.data.resTecnico)

                if(resDeudas.Data.ObjetoTributarioGestion ){
                    const tempGestion = resDeudas.Data.ObjetoTributarioGestion.map((ObjetoTributarioGestion) => {
                        ObjetoTributarioGestion.gestion = ObjetoTributarioGestion.gestion.name
                        ObjetoTributarioGestion.monto = ObjetoTributarioGestion.deuda
                        ObjetoTributarioGestion.message = ObjetoTributarioGestion.observaciones
                        ObjetoTributarioGestion.classCss = ""
                        ObjetoTributarioGestion.checked = true
                        return ObjetoTributarioGestion
                    })
                    setDataDeudaInmueble(tempGestion)
                }
            }

            const resHistorial = await fetch.fetchPost(form, `api-ruat/inmuebles/historial`, undefined)
            if (Boolean(resHistorial) && resHistorial.status === true) {
                if (resHistorial.data.length > 0)
                    setDataHistorial(resHistorial.data[0])
            }
        }
    }

    const breadcrumbs = [
        {
            title: Links[0].title,
            url: Links[0].url
        },
        {
            title: Links[3].title,
            url: Links[3].url
        },
        {
            title: 'Nuevo',
            url: '#'
        }
    ];
    return (
        <div id="contact" className="contact paddingTop" >

            <TitlePage titlePage={Texto.prescripcion} breadcrumbs={breadcrumbs} position={'left'} />

            <div className="container" style={{ marginTop: '-50px' }}>
                {dataInmueble ?
                    <DataPropietario dataInmueble={dataInmueble} />
                    : ""
                }

                {dataTecnicosInmueble !== undefined ?
                    <DataTecnicos dataInmueble={dataInmueble} dataTecnicosInmueble={dataTecnicosInmueble}
                        dataInmuebleCatastro={dataInmuebleCatastro} fetch={fetch} nameFormInmueble={"inmueble"} nameFormConstruccion={"construccion"}
                        nameFormConstruccionAdicional={"construccionAdicional"} nameFormDireccionInmueble={"inmueble_direccion"} />
                    : ""
                }

                {dataDeudaInmueble !== undefined ?
                    <DataLiquidacion dataDeudaInmueble={dataDeudaInmueble} />
                    : ""}

                {dataHistorial !== undefined ?
                    <DataHistorial dataHistorial={dataHistorial} type={"propietario"} title={"Propietarios Anteriores"} />
                    : ""}

                {dataHistorial !== undefined ?
                    <DataHistorial dataHistorial={dataHistorial} type={"transferencia"} title={"Transferencias"} />
                    : ""}
            </div>
        </div>

    )
}
export default PrescripcionDetalle