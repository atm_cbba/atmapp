import React, { Component, useEffect, useState, useRef, useCallback, useMemo } from 'react';
import PasosNavigationPrescripcion from '../utils/PasosNavigationPrescripcion';

import { Link } from 'react-router-dom';
import MapOpenLayer from '../component/MapOpenLayer'
import MapCatastroPublic from '../component/MapCatastroPublic'
import ModalSearchDireccion from '../component/ModalSearchDireccion'
import Links from '../../data/link';

import AsyncSelect from 'react-select/async';
import Fetch from '../utils/Fetch';
import AuthService from '../Usuario/AuthService';
import Languaje from '../../data/es';
import TableDeudas from './TableDeudas';
import TableColindantes from './TableColindantes';

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


const DataTecnicos = (props) => {

    const castrato_map = new MapCatastroPublic()

    const { dataInmueble, dataTecnicosInmueble, dataInmuebleCatastro, fetch, nameFormInmueble, nameFormDireccionInmueble, nameFormConstruccion, nameFormConstruccionAdicional } = props

    const refInputComuna = useRef()
    const refInputDistrito = useRef()
    const refInputSubDistrito = useRef()

    const refInputPredio = useRef()
    //const refInputCatastro = useRef()
    //const refInputZona = useRef()
    //const refInputNumeroRuat = useRef()
    const refInputLatitud = useRef()
    const refInputLongitud = useRef()
    const refInputCoordinate = useRef()

    const url_root_search = 'http://192.168.105.219:6080/arcgis/rest/services/'
    //const url_root_search_catastro = 'http://186.121.246.218:6080/arcgis/rest/services/'
    //http://186.121.246.218:6080/arcgis/rest/services/
    //https://catastroapi.cochabamba.bo/arcgis/rest/services/

    const [showColindantes, setShowColindantes] = useState(false)
    const [image64, setImage64] = useState(false)
    const [codigoCatastral, setCodigoCatastral] = useState(false)

    const handleEventClickColindantes = (event) => {
        event.preventDefault()
        setShowColindantes(!showColindantes)
    }

    useEffect(() => {
        debugger
        if (dataInmueble.claseInmueble === "PROPIEDAD HORIZONTAL") {
            let array_codigo_cat = dataInmueble.codigoCatastral.split('-')
            let value = array_codigo_cat[0] + "" + array_codigo_cat[1] + "" + array_codigo_cat[2]
            setCodigoCatastral(value)
            paintMap(`catastro/predios_cba/MapServer/0/query?f=json&where=UPPER(CodCat)%20LIKE%20%27%25${value}%25%27&returnGeometry=true&spatialRel=esriSpatialRelIntersects&maxAllowableOffset=0.13229193125052918&outFields=*&outSR=32719&resultRecordCount=12`)
        } else {
            let value = (dataInmueble.codigoCatastral).replace(/-/g, '');
            setCodigoCatastral(value)
            paintMap(`catastro/predios_cba/MapServer/0/query?f=json&where=UPPER(CodCat)%20LIKE%20%27%25${value}%25%27&returnGeometry=true&spatialRel=esriSpatialRelIntersects&maxAllowableOffset=0.13229193125052918&outFields=*&outSR=32719&resultRecordCount=12`)
        }
    }, []);

    const hanldEventClickOpenMap = (event) => {
        event.preventDefault()
        if (codigoCatastral !== "") {
            const predios_ = fetch.fetchGetExternal(`${url_root_search}catastro/predios_cba/MapServer/0/query?f=json&where=UPPER(CodCat)%20LIKE%20%27%25${codigoCatastral}%25%27&returnGeometry=true&spatialRel=esriSpatialRelIntersects&maxAllowableOffset=0.13229193125052918&outFields=*&outSR=32719&resultRecordCount=12`);  //old version
            predios_.then(res => {
                if (res.features !== undefined && res.features.length > 0) {
                    let coordinate = window.getCenterCoorderdanada(res.features[0].geometry.rings[0])
                    window.jQuery('#modalMapCatastro').modal('show');
                    castrato_map.handleChangePosition(event, res.features[0].geometry.rings[0])
                }
            })
        }

    }

    const paintMap = (relative_url) => {
        const predios_ = fetch.fetchGetExternal(`${Links[30].url}${relative_url}`);
        predios_.then(res => {
            if (res !== undefined && res.hasOwnProperty('features')) {
                if (res.features.length > 0) {
                    let features = res.features[0]
                    let center = window.getCenterCoorderdanada(features.geometry.rings[0])

                    refInputComuna.current.value = features.attributes.comuna
                    refInputDistrito.current.value = features.attributes.distrito
                    refInputSubDistrito.current.value = features.attributes.Sbdistrito
                    refInputPredio.current.value = features.attributes.Nro_predio
                    refInputCoordinate.current.value = center[0] + "," + center[1]

                    /*document.getElementById("spanDistrito").innerHTML = features.attributes.distrito
                    document.getElementById("spanSubDistrito").innerHTML = features.attributes.Sbdistrito
                    document.getElementById("spanComuna").innerHTML = features.attributes.comuna
                    document.getElementById("spanZonaTributaria").innerHTML = features.attributes.ZTributari*/

                    let _mapHidden = window.createMap(center, 17, function (longitud_latitud) {
                        window.paintIcon(center, _mapHidden)
                        refInputLatitud.current.value = longitud_latitud[1]
                        refInputLongitud.current.value = longitud_latitud[0]
                        setTimeout(() => {
                            window.getImage(_mapHidden, function (img64) {
                                if (Boolean(img64)) {
                                    debugger
                                    setImage64(img64)
                                } //else
                                //window.jQuery.preloader.stop();
                            });
                        }, 2000);
                    });
                }
            }
        })
    }

    let cont_constr = 0
    let cont_colindante = 0
    let cont_constr_adicionales = 0

    return (
        <>
            <div className="card widget widget-simple" >
                <div className="card-body ">

                    <div className="row ">
                        <div className="col-12 widget-header">
                            <h4 className="">{Languaje.datos_tecnicos_generales}</h4>
                        </div>
                    </div>

                    <div className="widget-content">
                        <div className="widget-body">

                            <div className="row">
                                <div className="col-sm-12 col-md-4 col-lg-4 padding-left-right-0">
                                    <p className="text-left"><strong>Código Catastral: </strong><span>{dataInmueble ? dataInmueble.codigoCatastral : ""}</span></p>
                                    <input type="hidden" name={nameFormInmueble + "[catastro]"} value={dataInmueble ? dataInmueble.codigoCatastral : ""} />
                                </div>

                                <div className="col-sm-12 col-md-4 col-lg-4 padding-left-right-0">
                                    <p className="text-left"><strong>Nro. Inmueble: </strong><span>{dataInmueble !== undefined ? dataInmueble.numeroInmueble : ""}</span></p>
                                    <input type="hidden" name={nameFormInmueble + "[numero_ruat]"} value={dataInmueble !== undefined ? dataInmueble.numeroInmueble : ""} />
                                </div>

                                <div className="col-sm-12 col-md-4 col-lg-4 padding-left-right-0">
                                    <p className="text-left"><strong>Zona Tributaria: </strong> <span>{dataTecnicosInmueble !== undefined ? dataTecnicosInmueble.datosGenerales.zonaTributaria : ""}</span></p>
                                    <input type="hidden" name={nameFormInmueble + "[zona]"} value={dataTecnicosInmueble ? dataTecnicosInmueble.datosGenerales.zonaTributaria : ""} />
                                </div>
                            </div>

                            <div className="row">
                                <div className="col-sm-12 col-md-4 col-lg-4 padding-left-right-0">
                                    <p className="text-left"><strong>Clase: </strong><span>
                                        {dataTecnicosInmueble ? dataTecnicosInmueble.datosGenerales.clase : ""}
                                    </span></p>
                                    <input type="hidden" name={nameFormInmueble + "[clase]"} value={dataTecnicosInmueble ? dataTecnicosInmueble.datosGenerales.clase : ""} />
                                </div>

                                <div className="col-sm-12 col-md-4 col-lg-4 padding-left-right-0">
                                    <p className="text-left"><strong>Area: </strong><span>{dataTecnicosInmueble !== undefined ? dataTecnicosInmueble.datosGenerales.area : ""} </span></p>
                                    <input type="hidden" name={nameFormInmueble + "[area]"} value={dataTecnicosInmueble ? dataTecnicosInmueble.datosGenerales.area : ""} />
                                </div>

                                <div className="col-sm-12 col-md-4 col-lg-4 padding-left-right-0">
                                    <p className="text-left"><strong>Tipo Propiedad: </strong><span>{dataTecnicosInmueble !== undefined ? dataTecnicosInmueble.datosGenerales.tipoPropiedad : ""}</span></p>
                                    <input type="hidden" name={nameFormInmueble + "[tipo_propiedad]"} value={dataTecnicosInmueble ? dataTecnicosInmueble.datosGenerales.tipoPropiedad : ""} />
                                </div>
                            </div>

                            <div className="row">
                                <div className="col-sm-12 col-md-4 col-lg-4 padding-left-right-0">
                                    <p className="text-left"><strong>Terreno m2: </strong><span>
                                        {dataTecnicosInmueble !== undefined ? dataTecnicosInmueble.terreno.superficeM2 : ""}
                                        <input type="hidden" name={nameFormInmueble + "[superficie]"} value={dataTecnicosInmueble !== undefined ? dataTecnicosInmueble.terreno.superficeM2 : ""} />
                                    </span></p>
                                </div>

                                <div className="col-sm-12 col-md-4 col-lg-4 padding-left-right-0">
                                    <p className="text-left">
                                        <strong>Construcciones: </strong>
                                        <span>{dataTecnicosInmueble ? dataTecnicosInmueble.construcciones.length : 0} </span>
                                        {dataTecnicosInmueble ?
                                            <Link to="#" className="h6 ml-2" title={`Total construcciones - ${dataTecnicosInmueble.construcciones.length}`}
                                                data-toggle="modal" data-target=".modal-contrucciones" >
                                                <i className="fa fa-building-o" aria-hidden="true"></i>
                                            </Link>
                                            : ""}
                                    </p>
                                </div>

                                <div className="col-sm-12 col-md-4 col-lg-4 padding-left-right-0">
                                    <p className="text-left"><strong>Construcciones Adcionales: </strong><span>{dataTecnicosInmueble && dataTecnicosInmueble.construccionesAdicionales ? dataTecnicosInmueble.construccionesAdicionales.length : 0}</span>
                                        {dataTecnicosInmueble && dataTecnicosInmueble.construccionesAdicionales ?
                                            <Link to="#" className="h6 ml-2" title={`Total construcciones adicionales - ${dataTecnicosInmueble.construccionesAdicionales.length}`}
                                                data-toggle="modal" data-target=".modal-contrucciones-adicionales" >
                                                <i className="fa fa-building-o" aria-hidden="true"></i>
                                            </Link>
                                            : ""}
                                    </p>
                                </div>
                            </div>

                            <div className="row">
                                <div className="col-sm-12 col-md-8 col-lg-8 padding-left-right-0">
                                    {
                                        dataInmuebleCatastro ? <p className="text-left"><strong>Matrícula Computarizada DD.RR: </strong><span>{dataInmuebleCatastro.informesLegal[0].matricula}</span></p> : ""
                                    }
                                    <input type="hidden" name={nameFormInmueble + "[matricula]"} value={dataInmuebleCatastro ? dataInmuebleCatastro.informesLegal[0].matricula : ""} />
                                </div>

                                <div className="col-sm-12 col-md-4 col-lg-4 padding-left-right-0">
                                    {
                                        dataInmuebleCatastro !== undefined ? <p className="text-left"><strong>Asiento: </strong><span>{dataInmuebleCatastro.informesLegal[0].asiento}</span></p> : ""
                                    }
                                    <input type="hidden" name={nameFormInmueble + "[asiento]"} value={dataInmuebleCatastro ? dataInmuebleCatastro.informesLegal[0].asiento : ""} />
                                </div>
                            </div>

                            <div className="row">
                                <div className="col-sm-12 col-md-12 col-lg-12 padding-left-right-0">
                                    <p className="text-left"><strong>Ubicación: </strong><span>{dataTecnicosInmueble.datosGenerales !== undefined ? dataTecnicosInmueble.datosGenerales.ubicacion : ""}</span></p>
                                    <input type="hidden" name={nameFormDireccionInmueble + "[ubicacion]"} value={dataTecnicosInmueble.datosGenerales !== undefined ? dataTecnicosInmueble.datosGenerales.ubicacion : ""} />
                                </div>
                            </div>

                            <div className="row">
                                <div className="col-sm-12 col-md-12 col-lg-12 padding-left-right-0">
                                    <p className="text-left"><strong>Referencia Ubicación: </strong><span>{dataTecnicosInmueble.datosGenerales !== undefined ? dataTecnicosInmueble.datosGenerales.referenciaUbicacion : ""}</span></p>
                                    <input type="hidden" name={nameFormDireccionInmueble + "[adyacente_entre]"} value={dataTecnicosInmueble.datosGenerales !== undefined ? dataTecnicosInmueble.datosGenerales.referenciaUbicacion : ""} />
                                </div>
                            </div>

                            <div className="row">
                                <div className="col-sm-12 col-md-4 col-lg-4 padding-left-right-0">
                                    <p className="text-left">
                                        <strong >Colindantes: </strong>
                                        {
                                            dataInmuebleCatastro !== undefined ?
                                                <Link to="#" className="h6 ml-2" onClick={handleEventClickColindantes} title={`Visualizar las colindancias`}  >
                                                    <i className="fa fa-th-large" aria-hidden="true"></i>
                                                </Link>
                                                : ""
                                        }
                                    </p>
                                </div>

                                <div className="col-sm-12 col-md-4 col-lg-4 padding-left-right-0">
                                    <p className="text-left">
                                        <strong >Ubicación Geoespacial: </strong>
                                        {
                                            <Link to="#" className="h6 ml-2" onClick={hanldEventClickOpenMap} title={`Visualizar ubicación  del domicilio`}  >
                                                <i className="fa fa-home" aria-hidden="true"></i>
                                            </Link>
                                        }

                                        {
                                            image64 ? <i className="fa fa-map-marker ml-2" aria-hidden="true" title={`Imagen del mapa capturado`}></i>
                                                : ""
                                        }
                                    </p>

                                    <input name={nameFormInmueble + '[predio]'} type="hidden" ref={refInputPredio} />
                                    <input name={nameFormInmueble + '[comuna]'} type="hidden" ref={refInputComuna} />
                                    <input name={nameFormInmueble + '[distrito]'} type="hidden" ref={refInputDistrito} />
                                    <input name={nameFormInmueble + '[sub_distrito]'} type="hidden" ref={refInputSubDistrito} />

                                    <input name={nameFormDireccionInmueble + '[latitud]'} type="hidden" ref={refInputLatitud} />
                                    <input name={nameFormDireccionInmueble + '[longitud]'} type="hidden" ref={refInputLongitud} />
                                    <input name={nameFormDireccionInmueble + '[coordinate]'} type="hidden" ref={refInputCoordinate} />
                                    <input name={nameFormDireccionInmueble + '[image]'} type="hidden" value={image64} />
                                </div>
                            </div>

                            {showColindantes ?
                                dataInmuebleCatastro !== undefined ? <TableColindantes data={dataInmuebleCatastro.informesTecnico[0].informesTecnicoColindancia} /> : ""
                                : ""}

                            {
                                dataInmuebleCatastro !== undefined ?
                                    dataInmuebleCatastro.informesTecnico[0].informesTecnicoColindancia.map(({ descripcionColindancia, orientacion }) => {
                                        return <input key={`${cont_colindante++}`} type="hidden" name={"colindante[][colindante]"} value={`${orientacion}:::${descripcionColindancia}`} />
                                    })
                                    : ""
                            }
                        </div>
                    </div>
                </div>
            </div>

            <div className="modal fade modal-contrucciones" tabIndex="-1" role="dialog" id="idModalContrucciones" aria-labelledby="modalContrucciones" aria-hidden="true" >
                <div className="modal-dialog modal-lg">
                    <div className="modal-content ">

                        <div className="modal-header">
                            <h5 className="modal-title" id="modalContrucciones">Construcciones</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                        <div className="modal-body">
                            <div className="row content-overflow">
                                {dataTecnicosInmueble !== undefined ?
                                    dataTecnicosInmueble.construcciones.map((construccion) => {

                                        return <div key={`${cont_constr++}`} className="col-12 col-md-6">
                                            <div className="card widget widget-simple" >
                                                <div className="card-body ">

                                                    <div className="row ">
                                                        <div className="col-4 widget-header">
                                                            <h4 className="">{construccion.bloque}</h4>
                                                        </div>
                                                        <div className="col-8 widget-header">
                                                            <h6 className="float-right">{construccion.usoConstruccion}</h6>
                                                        </div>
                                                    </div>

                                                    <div className="widget-content">
                                                        <div className="widget-body">

                                                            <div className="row">
                                                                <div className="col-6 col-md-6 col-lg-6 padding-left-right-0">
                                                                    <p className="text-left"><strong>Año de Contrucción: </strong><span>{construccion.anioConstruccion}</span></p>
                                                                </div>

                                                                <div className="col-6 col-md-6 col-lg-6 padding-left-right-0">
                                                                    <p className="text-right"><strong>Antiguedad: </strong><span>{` ${construccion.antiguedad} años`}</span></p>
                                                                </div>
                                                            </div>

                                                            <div className="row">
                                                                <div className="col-6 col-md-6 col-lg- padding-left-right-0">
                                                                    <p className="text-left"><strong>Superficie(m2): </strong><span>{construccion.superficieM2}</span></p>
                                                                </div>

                                                                <div className="col-6 col-md-6 col-lg-6 padding-left-right-0">
                                                                    <p className="text-right"><strong>Gestión: </strong> <span>{construccion.gestion}</span></p>
                                                                </div>
                                                            </div>

                                                            <div className="row">
                                                                <div className="col-12 padding-left-right-0">
                                                                    <p className="text-left"><strong>Construcción: </strong><span>{construccion.tipoConstruccion}</span></p>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <input type="hidden" name={nameFormConstruccion + "[][construccion]"} value={`${construccion.anioConstruccion}:::${construccion.antiguedad}:::${construccion.bloque}:::${construccion.gestion}:::${construccion.superficieM2}:::${construccion.tipoConstruccion}:::${construccion.usoConstruccion}`} />
                                                </div>
                                            </div>
                                        </div>
                                    })
                                    : ""}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div className="modal fade modal-contrucciones-adicionales" tabIndex="-1" role="dialog" id="idModalContruccionesAdcionales" aria-labelledby="modalContruccionesAdicionales" aria-hidden="true" >
                <div className="modal-dialog modal-lg">
                    <div className="modal-content ">

                        <div className="modal-header">
                            <h5 className="modal-title" id="modalContruccionesAdicionales">Construcciones Adicionales</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                        <div className="modal-body">
                            <div className="row content-overflow">

                                {dataTecnicosInmueble && dataTecnicosInmueble.construccionesAdicionales ?
                                    dataTecnicosInmueble.construccionesAdicionales.map((construccion) => {

                                        return <div key={`${cont_constr_adicionales++}`} className="col-12 col-md-6">
                                            <div className="card widget widget-simple" >
                                                <div className="card-body ">

                                                    <div className="row ">
                                                        <div className="col-12 widget-header">
                                                            <h6 className="">{construccion.tipoEdificacion}</h6>
                                                        </div>
                                                    </div>

                                                    <div className="widget-content">
                                                        <div className="widget-body">

                                                            <div className="row">
                                                                <div className="col-6 col-md-6 col-lg-6 padding-left-right-0">
                                                                    <p className="text-left"><strong>Año de Contrucción: </strong><span>{construccion.anioConstruccion}</span></p>
                                                                </div>

                                                                <div className="col-6 col-md-6 col-lg-6 padding-left-right-0">
                                                                    <p className="text-right"><strong>Antiguedad: </strong><span>{` ${construccion.antiguedad} años`}</span></p>
                                                                </div>
                                                            </div>

                                                            <div className="row">
                                                                <div className="col-6 col-md-6 col-lg- padding-left-right-0">
                                                                    <p className="text-left"><strong>Superficie(m2): </strong><span>{construccion.superficieM2}</span></p>
                                                                </div>

                                                                <div className="col-6 col-md-6 col-lg-6 padding-left-right-0">
                                                                    <p className="text-right"><strong>Gestión: </strong> <span>{construccion.gestion}</span></p>
                                                                </div>
                                                            </div>

                                                            <div className="row">
                                                                <div className="col-12 padding-left-right-0">
                                                                    <p className="text-left"><strong>Construcción: </strong><span>{construccion.tipoConstruccion}</span></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <input type="hidden" name={nameFormConstruccionAdicional + "[][adicional]"} value={`${construccion.anioConstruccion}:::${construccion.antiguedad}:::${construccion.gestion}:::${construccion.puntaje}:::${construccion.superficieM2}:::${construccion.tipoConstruccion}:::${construccion.tipoEdificacion}`} />
                                                </div>
                                            </div>
                                        </div>
                                    })
                                    : ""}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <MapCatastroPublic module={'prescripcion'} nameForm={"domicilio_actividad_economica"} mode={"basic"} />
            <MapOpenLayer module={'prescripcion'} /> {/* mapa global para extraer la imagen*/}
        </>
    )
}
export default DataTecnicos