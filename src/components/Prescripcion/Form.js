import React, { Component, useState, useRef } from 'react';

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Fetch from '../../components/utils/Fetch';
import AuthService from '../../components/Usuario/AuthService';
import Constant from '../../data/constant';
import Texto from '../../data/es';
import FormObjetoTributario from './FormObjetoTributario'
import FormRequisito from './FormRequisito'
import VistaPrevia from './VistaPrevia'
import PrescripcionDTO from '../utils/PrescripcionDTO';
import Config from '../../data/config';

const Form = (props) => {

    const fetch = new Fetch();
    fetch.setToast(toast);
    const auth = new AuthService();

    //const prescripcionDTO = new PrescripcionDTO();

    const [showBottomPreview, setShowBottomPreview] = useState(true);
    let { derecho_admision } = props

    const handleSubmitForm = (event, properties = undefined) => {
        event.preventDefault();
        window.jQuery("#" + event.target.getAttribute('id')).parsley().validate();

        let form_html = event.target;  //event.target.getAttribute('id');
        const form = new FormData(event.target);
        var target = event.target

        debugger

        //if (form_html.getAttribute('id') == 'formPresObjetoTributario') {
        //    submitFormObjetoTributario(form, properties)
        //} else {
        if (window.jQuery("#" + form_html.getAttribute('id')).parsley().isValid()) {

            window.createBootBoxCustom("Esta Seguro de Continuar.", false, function (result) {
                debugger
                if (result === true) {
                    debugger
                    switch (form_html.getAttribute('id')) {
                        case "formPresSolicitante":  //segundo formulario
                            //submitFormPresSolicitante(form, target)
                            break;
                        case "formPresDomicilioSolicitante":  //tercer formulario
                            //submitFormPresDomicilioSolicitante(form, properties, target)
                            break;
                        case "formPresObjetoTributario":
                            submitFormObjetoTributario(form, target)
                            break;
                        case "formPresRequisito":
                            //submitFormRequisito(form, properties, target)
                            break;
                        default:    //el reporte
                            break;
                    }
                }
            })
        } else {
            toast.warn('El formulario tiene valores obligatorios', {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true
            });
        }
        //}
    }

    const submitFormObjetoTributario = (form, target) => {
        debugger
        const inmuble_ruat = localStorage.getItem('inmueble');
        const inmuble_catastro = localStorage.getItem('inmuebleCatastro');
        const objectTributary = localStorage.getItem('objectTributary');

        form.append('pres_prescripcion[fur]', props.fur);
        form.append('pres_solicitante[contribuyente]', props.derecho_admision);
        form.append('pres_objeto[id_objeto_tributario]', objectTributary);
        form.append('pres_objeto[objeto_ruat_base64]', inmuble_ruat);
        form.append('pres_objeto[objeto_catastro_base64]', inmuble_catastro);

        fetch.fetchPost(form, 'api/prescripcion/create', target).then(dataJson => {
            debugger
            if (dataJson !== undefined && dataJson.status === true) {
                let token = dataJson.data.prescripcion.token
                localStorage.setItem('tokenPrescripcion', token)
                //descargar el pdf
                window.location.href = Config[0].url + `report/prescripcion-preview/${token}/?auth=` + auth.getToken();
                setShowBottomPreview(false)
                toast.success(dataJson.message, {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true
                });
            }
        })
    }
    /*
        const submitFormRequisito = (form, properties, target) => {
    
            form.append('pres_prescripcion[token]', properties.prescripcion.token);
    
            fetch.fetchPost(form, 'api/prescripcion/requisito/create', target).then(dataJson => {
                debugger
                if (dataJson !== undefined && dataJson.status === true) {
                    if (Boolean(dataJson.PrescripcionRequisito)) {
                        toast.success(dataJson.message, {
                            position: "top-right",
                            autoClose: 5000,
                            hideProgressBar: false,
                            closeOnClick: true,
                            pauseOnHover: true,
                            draggable: true
                        });
    
                        setShowFormSolicitante(false)
                        setShowFormDomicilioSolicitante(false)
                        setShowFormObjetoTributario(false)
                        setShowFormObjetoTributario(false)
                        setShowFormRequisito(false)
                        setShowFormVistaPrevia(true)
                    } else {
                        toast.warn(dataJson.message, {
                            position: "top-right",
                            autoClose: 5000,
                            hideProgressBar: false,
                            closeOnClick: true,
                            pauseOnHover: true,
                            draggable: true
                        });
                    }
                }
            })
        }
    */
    /*if (parseInt(derecho_admision) === parseInt(Constant[0].derecho_admision.natural)) {
        titulo_navigation = Texto.prescripcion_contribuyente_natural + " - " + props.fur
    }

    if (parseInt(derecho_admision) === parseInt(Constant[0].derecho_admision.juridico)) {
        titulo_navigation = Texto.prescripcion_contribuyente_juridica + " - " + props.fur
    }*/

    return (
        <>
            <FormObjetoTributario toast={toast} onSubmitForm={handleSubmitForm} buttonName={'Siguiente'}
                nameForm={"inmueble"} nameFormTribGestion={"pres_objeto_tributario_gestion"} fur={props.fur} 
                derecho_admision={props.derecho_admision} nameFormDireccion={"inmueble_direccion"} showBottomPreview={showBottomPreview} />

            <ToastContainer enableMultiContainer containerId={'Z'}
                position="top-right"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnVisibilityChange
                draggable
                pauseOnHover
            />
        </>
    )
}

export default Form;