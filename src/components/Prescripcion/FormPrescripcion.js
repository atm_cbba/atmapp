import React, { Component } from 'react';
import PasosNavigationPrescripcion from '../../components/utils/PasosNavigationPrescripcion';
import Constant from '../../data/constant';
import Languaje from '../../data/es';

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Fetch from '../../components/utils/Fetch';
//import Links from '../../data/link';
import Texto from '../../data/es';

let titulo_navigation ="";
const FormPrescripcion = (props) => {


    if(parseInt(props.derecho_admision) === parseInt(Constant[0].derecho_admision.natural) ){
        titulo_navigation = Languaje.prescripcion_contribuyente_natural 
    }

    if(parseInt(props.derecho_admision) === parseInt(Constant[0].derecho_admision.juridico) ){
        titulo_navigation = Languaje.prescripcion_contribuyente_juridica 
    }

    if(parseInt(props.derecho_admision) === parseInt(Constant[0].derecho_admision.inmuebles) ){
        titulo_navigation = Languaje.prescripcion_inmuebles 
    }

    if(parseInt(props.derecho_admision) === parseInt(Constant[0].derecho_admision.actividad_economica) ){
        titulo_navigation = Languaje.prescripcion_persona_natural 
    }

    if(parseInt(props.derecho_admision) === parseInt(Constant[0].derecho_admision.vehiculos) ){
        titulo_navigation = Languaje.prescripcion_persona_natural 
    }
    return (
        
        <div className="row">
            <PasosNavigationPrescripcion titulo_paso1={titulo_navigation}
                paso1_active={true} paso2_active={false} paso3_active={false} paso4_active={false} paso5_active={false} />

            <form action="" className="contact__form needs-validation" name="formPrescripcion" id="formPrescripcion"
                method="post" noValidate onSubmit={props.onSubmitForm} style={{ width: '100%' }}>
                <div className="row">
                    <div className="col-sm-12 col-md-4 col-lg-4 form-group">
                        <label htmlFor="inputFur" >FUR *</label>
                        <input id="inputFur" name="inputFor" type="text" className="form-control" placeholder="Fur"
                            disabled data-parsley-required="true" />
                    </div>
                </div>
            </form>
        </div>
    )
}

export default FormPrescripcion;