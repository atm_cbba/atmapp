import React, { Component, useState, useEffect } from 'react';
import Fetch from '../../components/utils/Fetch';
import Constant from '../../data/constant';
import { Link } from 'react-router-dom';
import Config from '../../data/config';
import AuthService from '../../components/Usuario/AuthService';
import DataTable from 'react-data-table-component';
import styled from 'styled-components';  //styled for data table
import Links from '../../data/link';
import TitlePage from '../../components/utils/TitlePage';
import Texto from '../../data/es';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import CustomLoader from '../utils/SpinnerDataTable'
import ModalPdf from '../utils/ModalPdf';

const IndexFiscalizacion = (props) => {

    const constant = Constant[0];
    const fetch = new Fetch();
    fetch.setToast(toast);
    const auth = new AuthService();

    const titlePage = Texto.prescripcion
    //const profile = auth.getProfile();

    const modalPdf = new ModalPdf()
    modalPdf.setToast(toast)

    const [data, setData] = useState([])
    const [loading, setLoading] = useState(false)
    const [totalRows, setTotalRows] = useState(0)
    const [perPage, setPerPage] = useState(10)
    const [typeSearch, setTypeSearch] = useState(0)
    const [showSearch, setShowSearch] = useState(true)

    const columns = [
        {
            name: Texto.contribuyente,
            sortable: true,
            cell: row => <div>
                {row.contribuyente === "1" ? <span>{Constant[0].contribuyente.natural}</span> : <span>{Constant[0].contribuyente.juridico}</span>}
            </div>,
            grow: 1,
            hide: 'sm',
            maxWidth: '150px',
        },
        {
            name: Texto.numero_orden,
            selector: 'numero',
            sortable: true,
            maxWidth: '130px'
        },
        {
            name: Texto.numero_fur,
            selector: 'fur',
            sortable: true,
            hide: 'sm',
            maxWidth: '100px'
        },
        //aqui debemos mostrar objeto que ha de ser prescrito
        /* {
             name: Texto.usuario,
             selector: 'username',
             sortable: true,
             hide: 'md',
             maxWidth: '150px',
         },*/
        {
            name: Texto.aprobado,
            selector: 'fecha_aprobacion',
            sortable: true,
            hide: 'md',
            maxWidth: '140px',
        },
        {
            name: Texto.creado,
            selector: 'created_at',
            sortable: true,
            hide: 'md',
            maxWidth: '140px',
        },
        {
            name: Texto.estado,
            center: true,
            sortable: true,
            maxWidth: '50px',
            cell: row => <div>
                {row.code_estado === Constant[0].estado.en_proceso ? <span title={row.code_estado}><i className="fa fa-cog" aria-hidden="true" ></i></span> : ""}
                {row.code_estado === Constant[0].estado.completado ? <span title={row.code_estado}><i className="fa fa-file-text-o" aria-hidden="true"></i></span> : ""}
                {row.code_estado === Constant[0].estado.aprobado ? <span title={row.code_estado}><i className="fa fa-check" aria-hidden="true"></i></span> : ""}
                {row.code_estado === Constant[0].estado.eliminado ? <span title={row.code_estado}><i className="fa fa-ban" aria-hidden="true"></i></span> : ""}
                {row.code_estado === Constant[0].estado.consolidado_ruat ? <span title={row.code_estado}><i className="fa fa-handshake-o" aria-hidden="true"></i></span> : ""}
            </div>,
            ignoreRowClick: true,
            allowOverflow: true,
        },
        {
            name: '',
            sortable: true,
            cell: row => <div>
                <Link to={{ pathname: Links[42].url+"/" +row.token}} aria-label={Links[42].title} style={{ fontSize: '20px', marginRight: '10px' }} title={`Detalle de la Prescripción - ` + row.numero}  >
                    <i className="fa fa-pencil-square-o" aria-hidden="true"></i>
                </Link>
                
                <Link to={{ pathname: Links[41].url+"/" +row.token}} aria-label={Links[41].title} style={{ fontSize: '20px', marginRight: '10px' }} title={`Consolidación de la  Prescripción - ${row.numero}`}  >
                    <i className="fa fa-certificate" aria-hidden="true"></i>
                </Link> 

                <Link to="#" onClick={() => handleDownloadPdfClick(row)} style={{ fontSize: '20px', marginRight: '10px' }}
                    title={`Prescripción: ${row.numero}`} >
                    <i className="fa fa-file-pdf-o" aria-hidden="true"></i>
                </Link>
            </div>,
            ignoreRowClick: true,
            allowOverflow: true,
            button: true,
        }
    ];

    const SampleStyle = styled.div`
      padding: 10px;
      display: block;
      width: 90%;
      p {
        font-size: 12px;
        word-break: break-all;
      }
    `;

    const IntentExpandedComponent = ({ data }) => {
        let object_trib = JSON.parse(data.obj_trib_gestion)

        let row_table = null
        if (object_trib !== null) {
            row_table = object_trib.map((obj_trib, index) =>
                <tr key={index}>
                    <td scope="row">{obj_trib.code}</td>
                    <td>{obj_trib.numero}</td>
                    <td>{obj_trib.array_to_string}</td>
                </tr>
            );
        }

        return <SampleStyle>
            <div className="row">
                <div className="col-12 col-md-6">
                    <p><strong>{Texto.contribuyente}: </strong>{data.contribuyente === "1" ? Constant[0].contribuyente.natural : Constant[0].contribuyente.juridico}</p>
                    <p><strong>{Texto.numero_orden}: </strong>{data.numero}</p>
                    <p><strong>{Texto.numero_fur}: </strong>{data.fur}</p>
                    <p><strong>{Texto.usuario}: </strong>{data.username}</p>
                    <p><strong>{Texto.creado}: </strong>{data.created_at}</p>
                    <p><strong>{Texto.aprobado}: </strong>{data.fecha_aprobacion}</p>
                </div>
                <div className="col-12 col-md-6">
                    <table className="table" >
                        <thead>
                            <tr >
                                <th scope="col">{Texto.objeto_tributario}</th>
                                <th scope="col">{Texto.numero}</th>
                                <th scope="col">{Texto.gestion}</th>
                            </tr>
                        </thead>
                        <tbody>
                            {row_table}
                        </tbody>
                    </table>
                </div>
            </div>
        </SampleStyle>
    }

    const handleDownloadPdfClick = (row) => {
        modalPdf.showPdf(`${Config[0].url}report/prescripcion/${row.token}/?auth=${auth.getToken()}`, row.prescripcion, row.token)
    }

    const handleOpeContribuyenteClick = (row) => {

        const reaperturar_lic = fetch.fetchGet(`api/prescripcion/get-estado-datos/${row.token}`);
        reaperturar_lic.then(res => {
            if (res !== undefined && res.status === true) {
                window.editPrescripcionBootbox(Links[37].url, row.contribuyente, row.token, row.numero,
                    res.is_complete_data_contribuyente ? true : false, res.is_complete_data_domicilio ? true : false,
                    res.is_complete_data_domicilio_actividad_economica ? true : false, res.is_complete_data_requisitos ? true : false,
                );
            } else {
                window.editPrescripcionBootbox(Links[37].url, row.contribuyente, row.token, row.numero, false, false, false);
            }
        })
    }

    useEffect(() => {
        if (auth.loggedIn()) {
            loadPrescriptions()
        } else
            props.history.replace(Links[4].url)

        window.jQuery(".sc-kAzzGY").remove()  //pertenece al datatable
    }, []);

    const hanldeSearchPrescripciones = async (event, type_search) => {
        setLoading(true)

        const response = await fetch.axiosAsyncGet(`api/prescripcion/fisca-per-page/1/${perPage}/${type_search}`);
        if (response !== null && response.status === true) {

            setData(response.data)
            setTotalRows(response.total)
            setLoading(false)
            setTypeSearch(type_search)

            toast.success(response.message, {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true
            });
        }
    }

    const handleRedirectUrlClick = (event, url) => {
        event.preventDefault();
        window.location.href = url;
    }

    const loadPrescriptions = async () => {
        setLoading(true)
        const response = await fetch.axiosAsyncGet(`api/prescripcion/fisca-per-page/1/${perPage}/${typeSearch}`);
        if (response !== null && response.status === true) {
            setData(response.data)
            setTotalRows(response.total)
            setLoading(false)

            document.getElementById("pAprobado").innerHTML = "Pendientes - " + response.aprobados
            document.getElementById("pAprobadoFisca").innerHTML = "Aprobados - " + response.aprobados_fisca

            toast.success(response.message, {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true
            });
        }
    }

    const handlePageChange = async (page) => {

        setLoading(true)
        const response = await fetch.axiosAsyncGet(`api/prescripcion/fisca-per-page/${page}/${perPage}/${typeSearch}`)
        setLoading(false)
        setData(response.data)
    };

    const handlePerRowsChange = async (perPage, page) => {
        setLoading(true)
        const response = await fetch.axiosAsyncGet(`api/prescripcion/fisca-per-page/${page}/${perPage}/${typeSearch}`)

        setLoading(false)
        setData(response.data)
    };
    const breadcrumbs = [
        {
            title: Links[0].title,
            url: Links[0].url
        },
        {
            title: Links[43].title,
            url: Links[43].url
        }
    ];

    return (
        <div className="paddingTop30" id="services" >
            {/* Breadcrumb Area Start */}
            <TitlePage titlePage={titlePage} breadcrumbs={breadcrumbs} position={'left'} leftfull={false} />
            {/* Breadcrumb Area End */}

            <div className="container features">
                {/* Menu navegacion */}
                <section className="panel-menu-info">
                    <div className="panel-menu-info-content">
                        <div className="row">
                            <div className="col-4 col-md-2 col-lg-2">
                                <div className="single-contact-info pointer" onClick={e => hanldeSearchPrescripciones(e, 3)}>
                                    <i className="fa fa-cog" aria-hidden="true"></i>
                                    <p id="pAprobado">0</p>
                                </div>
                            </div>
                            <div className="col-4 col-md-2 col-lg-2">
                                <div className="single-contact-info pointer" onClick={e => hanldeSearchPrescripciones(e, 4)}>
                                    <i className="fa fa-file-text-o" aria-hidden="true"></i>
                                    <p id="pAprobadoFisca">0</p>
                                </div>
                            </div>
                            <div className="col-4 col-md-2 col-lg-2">
                            </div>

                            <div className="col-4 col-md-2 col-lg-2">
                            </div>

                            <div className="col-4 col-md-2 col-lg-2">
                            </div>

                            <div className="col-4 col-md-2 col-lg-2">
                                <div className="single-contact-info pointer" onClick={e => handleRedirectUrlClick(e, Links[8].url)}>
                                    <i className="fa fa-user-o" aria-hidden="true"></i>
                                    <p>Mi Cuenta</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                {/* Menu navegacion end*/}

                <DataTable
                    title={titlePage}
                    columns={columns}
                    data={data}
                    progressPending={loading}
                    pagination
                    paginationServer
                    paginationTotalRows={totalRows}
                    onChangeRowsPerPage={handlePerRowsChange}
                    onChangePage={handlePageChange}
                    highlightOnHover
                    noDataComponent={Texto.there_are_no_records_to_display}
                    progressComponent={<CustomLoader />}
                    expandableRowsComponent={<IntentExpandedComponent />}
                    expandableRows
                />
            </div>

            <ToastContainer enableMultiContainer containerId={'Z'}
                position="top-right"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnVisibilityChange
                draggable
                pauseOnHover
            />
            <ToastContainer />
        </div>
    );
}
export default IndexFiscalizacion;