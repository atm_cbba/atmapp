import React, { useEffect, useState } from 'react';
import TitlePage from '../../components/utils/TitlePage';
import Constant from '../../data/constant';
import Links from '../../data/link';
import Fetch from '../../components/utils/Fetch';
import AuthService from '../../components/Usuario/AuthService';
import FormContribuyente from '../../components/Prescripcion/FormSolicitante';
import FormDomicilioContribuyente from '../../components/Prescripcion/FormDomicilioContribuyente';
import FormObjetoTributario from '../../components/Prescripcion/FormObjetoTributario';
import FormRequisito from './FormRequisito'
import VistaPrevia from './VistaPrevia'

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import '../style/parsley.css';

import Texto from '../../data/es';

const PrescripcionUpdate = (props) => {

    //const form_edit = window.getParameterByName("edit")
    //const token_prescrip = window.getParameterByName("token")
    //const call = window.getParameterByName("call") //pagina donde debe regresar

    const fetch = new Fetch();
    const Auth = new AuthService();
    fetch.setToast(toast);
    const form_edit = window.getParameterByName("edit")
    const token_prescripcion = window.getParameterByName("token")
    const call = window.getParameterByName("call") //pagina donde debe regresar

    const [initial_state, setInitialState] = useState(false);
    const [contribuyenteShow, setContribuyenteShow] = useState(false);
    const [domicilioContribuyenteShow, setDomicilioContribuyenteShow] = useState(false);
    const [objetosTributariosShow, setObjetosTributariosShow] = useState(false)
    const [requisitosShow, setRequisitosShow] = useState(false)
    const [vistaPreviaShow, setVistaPreviaShow] = useState(false)

    const [prescripcionDb, setPrescripcionDb] = useState(undefined)
    const [solicitanteDb, setSolicitanteDb] = useState(undefined)
    const [domicilioDb, setDomicilioDb] = useState(undefined)
    const [personaDb, setPersonaDb] = useState(undefined)
    const [prescripcionRequisitoDb, setPrescripcionRequisitoDb] = useState(undefined)
    const [objetoTributarioGestion, setObjetoTributarioGestion] = useState(undefined)
    const [inmuebleDomicilio, setInmuebleDomicilio] = useState(undefined)
    const [inmueble, setInmueble] = useState(undefined)
    const [tituloNavigation, setTituloNavigation] = useState("")
    const [tituloNavigation2, setTituloNavigation2] = useState("")

    useEffect(() => {
        if (!Auth.loggedIn()) {
            props.history.replace(Links[4].url)
        }

        loadDataContribuyente(form_edit, token_prescripcion)
        loadDataDomicilioContribuyente(form_edit, token_prescripcion)
        loadDataObjetosTributarios(form_edit, token_prescripcion)

        loadVistaPrevia(form_edit, token_prescripcion)
        loadDataRequisitos(form_edit, token_prescripcion)

        window.addEventListener("beforeunload", onUnload);
        return () => window.removeEventListener("beforeunload", onUnload);

    }, [initial_state]);

    const onUnload = e => {
        e.preventDefault();
        e.returnValue = '';
    }

    const loadDataContribuyente = async (form, token) => {
        if (form === 'sol' || form === 'pjrl') {
            const response = await fetch.axiosAsyncGet(`api/prescripcion/by-token/${token}`);
            if (response !== null && response.status === true) {
                debugger
                let prescripcion = response.data.Prescripcion
                setPrescripcionDb(response.data)
                console.log(response.data.Solicitante.contribuyente)

                if (parseInt(response.data.Solicitante.contribuyente) === parseInt(Constant[0].derecho_admision.natural))
                    setTituloNavigation(Texto.prescripcion_contribuyente_natural + " - " + prescripcion.fur)

                if (parseInt(response.data.Solicitante.contribuyente) === parseInt(Constant[0].derecho_admision.juridico)){
                    setTituloNavigation2("Domicilio del Representante Legal")
                    setTituloNavigation(Texto.prescripcion_contribuyente_juridica + " - " + prescripcion.fur)
                }

                setContribuyenteShow(true)
                setDomicilioContribuyenteShow(false)
                setObjetosTributariosShow(false)
                setRequisitosShow(false)
                setVistaPreviaShow(false)
            }
        }
    }

    const loadDataDomicilioContribuyente = async (form, token) => {

        if (form === 'domcon') {
            const response = await fetch.axiosAsyncGet(`api/prescripcion/domicilio/token/${token}`);
            if (response !== null && response.status === true) {
                debugger
                let prescripcion = response.Prescripcion

                if (parseInt(response.Solicitante.contribuyente) === parseInt(Constant[0].derecho_admision.natural)){
                    setTituloNavigation(Texto.prescripcion_contribuyente_natural + " - " + prescripcion.fur)
                    if( response.PersonaRL !== null && response.PersonaRL !== undefined){
                        setTituloNavigation2("Domicilio del Representante Legal")
                    }
                }

                if (parseInt(response.Solicitante.contribuyente) === parseInt(Constant[0].derecho_admision.juridico)){
                    setTituloNavigation(Texto.prescripcion_contribuyente_juridica + " - " + prescripcion.fur)
                    setTituloNavigation2("Domicilio del Representante Legal")
                }

                setPrescripcionDb(response.Prescripcion)
                setDomicilioDb(response.Domicilio)
                setPersonaDb(response.Persona)
                setContribuyenteShow(false)
                setDomicilioContribuyenteShow(true)
                setObjetosTributariosShow(false)
                setRequisitosShow(false)
                setVistaPreviaShow(false)
            }
        }
    }

    const loadDataObjetosTributarios = async (form, token) => {

        if (form === 'objtri') {
            const response = await fetch.axiosAsyncGet(`api/prescripcion/objetos-tributarios-gestion/token-prescripcion/${token}`);
            if (response !== null && response.status === true) {
                debugger
                let prescripcion = response.Data.Prescripcion
                let solicitante = response.Data.Solicitante
                let personaRL = response.Data.PersonaRL || undefined

                if (parseInt(solicitante.contribuyente) === parseInt(Constant[0].derecho_admision.natural)){
                    setTituloNavigation(Texto.prescripcion_contribuyente_natural + " - " + prescripcion.fur)
                    if( personaRL !== undefined){
                        setTituloNavigation2("Domicilio del Representante Legal")
                    }
                }

                if (parseInt(solicitante.contribuyente) === parseInt(Constant[0].derecho_admision.juridico)){
                    setTituloNavigation(Texto.prescripcion_contribuyente_juridica + " - " + prescripcion.fur)
                    setTituloNavigation2("Domicilio del Representante Legal")
                }

                prescripcion.contribuyente = solicitante.contribuyente
                setPrescripcionDb(prescripcion)
                setObjetoTributarioGestion(response.Data.ObjetoTributarioGestion)
                setInmueble(response.Data.Inmueble)
                setInmuebleDomicilio(response.Data.InmuebleDomicilio)

                setContribuyenteShow(false)
                setDomicilioContribuyenteShow(false)
                setObjetosTributariosShow(true)
                setRequisitosShow(false)
                setVistaPreviaShow(false)
            }
        }
    }

    const loadDataRequisitos = async (form, token) => {
        if (form === 'cumpreq') {
            const response = await fetch.axiosAsyncGet(`api/prescripcion/prescripcion-requisito/token/${token}`);
            if (response !== null && response.status === true) {
                debugger
                let prescripcion = response.Data.Prescripcion
                let solicitante = response.Data.Solicitante
                let personaRL = response.Data.PersonaRL || undefined

                if (parseInt(solicitante.contribuyente) === parseInt(Constant[0].derecho_admision.natural)){

                    setTituloNavigation(Texto.prescripcion_contribuyente_natural + " - " + prescripcion.fur)
                    if( personaRL !== undefined){
                        setTituloNavigation2("Domicilio del Representante Legal")
                    }
                }

                if (parseInt(solicitante.contribuyente) === parseInt(Constant[0].derecho_admision.juridico)){
                    setTituloNavigation(Texto.prescripcion_contribuyente_juridica + " - " + prescripcion.fur)
                    setTituloNavigation2("Domicilio del Representante Legal")
                }

                prescripcion.contribuyente = solicitante.contribuyente
                setPrescripcionDb(prescripcion)
                setPrescripcionRequisitoDb(response.Data.PrescripcionRequisito)

                setContribuyenteShow(false)
                setDomicilioContribuyenteShow(false)
                setObjetosTributariosShow(false)
                setRequisitosShow(true)
                setVistaPreviaShow(false)
            }
        }
    }

    const loadVistaPrevia = async (form, token) => {
        if (form === 'preview') {
            const response = await fetch.axiosAsyncGet(`api/prescripcion/by-token/${token}`);
            if (response !== null && response.status === true) {
                if (response !== undefined && response.status === true) {
                    let prescripcion = response.data.Prescripcion
                    let personaRL = response.data.PersonaRL || undefined
                    setPrescripcionDb(prescripcion)
                    setSolicitanteDb(response.data.Solicitante)
                    debugger

                    if (parseInt(response.data.Solicitante.contribuyente) === parseInt(Constant[0].derecho_admision.natural)){
                        setTituloNavigation(Texto.prescripcion_contribuyente_natural + " - " + prescripcion.fur)
                        if( personaRL !== undefined){
                            setTituloNavigation2("Domicilio del Representante Legal")
                        }
                    }

                    if (parseInt(response.data.Solicitante.contribuyente) === parseInt(Constant[0].derecho_admision.juridico)){
                        setTituloNavigation2("Domicilio del Representante Legal")
                        setTituloNavigation(Texto.prescripcion_contribuyente_juridica + " - " + prescripcion.fur)
                    }
                    setContribuyenteShow(false)
                    setDomicilioContribuyenteShow(false)
                    setObjetosTributariosShow(false)
                    setRequisitosShow(false)
                    setVistaPreviaShow(true)
                }
            }
        }
    }

    const handleSubmitEditForm = (event) => {

        event.preventDefault();
        window.jQuery("#" + event.target.getAttribute('id')).parsley().validate();

        let form_html = event.target;  //event.target.getAttribute('id');
        const form = new FormData(event.target);
        var target = event.target

        infoErrorParsleConfigure()

        if (window.jQuery("#" + form_html.getAttribute('id')).parsley().isValid()) {
            window.createBootbox("Esta Seguro de Continuar.", function (result) {
                if (result === true) {
                    debugger
                    switch (form_html.getAttribute('id')) {
                        case "formPresSolicitante":
                            submitFormContribuyente(form, target)
                            break;
                        case "formPresDomicilioSolicitante":  //tercer formulario
                            submitFormDomicilio(form, target)
                            break;
                        case "formPresObjetoTributario":
                            submitFormPresObjetoTributario(form, target)  //formPresObjetoTributario
                            break;
                        case "formPresRequisito":
                            submitFormRequisito(form, target)
                            break;
                    }
                }
            })
        } else {
            toast.warn('El formulario tiene valores obligatorios', {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true
            });
        }
    }

    const submitFormContribuyente = (form, target) => {

        form.append('pres_prescripcion[token]', token_prescripcion);

        fetch.fetchPost(form, 'api/prescripcion/solicitante/update', target).then(dataJson => {
            debugger
            if (dataJson !== undefined && dataJson.status === true) {
                if (Boolean(dataJson.Persona)) {
                    toast.success(dataJson.message, {
                        position: "top-right",
                        autoClose: 5000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true
                    });

                    let param_num = window.getParameterByName("num")

                    if (Boolean(call) && call === 'pw' && Boolean(token_prescripcion) && Boolean(param_num))
                        window.redirect(Links[20].url + '?edit=preview&token=' + token_prescripcion + "&num=" + param_num);
                    else
                        window.redirect(Links[3].url);
                } else {
                    toast.warn(dataJson.message, {
                        position: "top-right",
                        autoClose: 5000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true
                    });
                }
            }
        })

    }

    const submitFormPresObjetoTributario = (form, target) => {

        form.append('pres_prescripcion[token]', token_prescripcion);
        form.delete('inmueble_direccion[direccion]');
        fetch.fetchPost(form, 'api/prescripcion/objeto-tributario-gestion/update', target).then(dataJson => {
            debugger
            if (dataJson !== undefined && dataJson.status === true) {
                if (Boolean(dataJson.ObjetoTributarioGestion)) {
                    debugger
                    toast.success(dataJson.message, {
                        position: "top-right",
                        autoClose: 5000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true
                    });
                } else {
                    toast.warn(dataJson.message, {
                        position: "top-right",
                        autoClose: 5000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true
                    });
                }
            }
        })
    }

    const submitFormDomicilio = (form, target) => {

        if (form.get('domicilio[latitud]') !== "" && form.get('domicilio[longitud]') !== "") {
            fetch.fetchPost(form, 'api/prescripcion/domicilio/update', target).then(dataJson => {
                if (dataJson !== undefined && dataJson.status === true) {
                    if (Boolean(dataJson.Domicilio)) {
                        toast.success(dataJson.message + ". " + Texto.espere_redireccionamos_pagina, {
                            position: "top-right",
                            autoClose: 5000,
                            hideProgressBar: false,
                            closeOnClick: true,
                            pauseOnHover: true,
                            draggable: true
                        });

                        /*if (Boolean(self.call) && self.call === 'pw' && Boolean(self.token_dj) && Boolean(self.numero_dj))
                            window.redirect(Links[6].url + '?edit=preview&token=' + self.token_dj + "&num=" + self.numero_dj);
                        else
                            window.redirect(Links[1].url);*/
                    } else {
                        toast.warn(dataJson.message, {
                            position: "top-right",
                            autoClose: 5000,
                            hideProgressBar: false,
                            closeOnClick: true,
                            pauseOnHover: true,
                            draggable: true
                        });
                    }
                }
            })
        } else {
            toast.warn(Texto.mapa_required, {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true
            });
        }
    }

    const submitFormRequisito = (form, target) => {

        form.append('pres_prescripcion[token]', token_prescripcion);
        fetch.fetchPost(form, 'api/prescripcion/requisito/update', target).then(dataJson => {
            if (dataJson !== undefined && dataJson.status === true) {
                if (Boolean(dataJson.PrescripcionRequisito)) {
                    toast.success(dataJson.message + ". " + Texto.espere_redireccionamos_pagina, {
                        position: "top-right",
                        autoClose: 5000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true
                    });

                    /*if (Boolean(self.call) && self.call === 'pw' && Boolean(self.token_dj) && Boolean(self.numero_dj))
                        window.redirect(Links[6].url + '?edit=preview&token=' + self.token_dj + "&num=" + self.numero_dj);
                    else
                        window.redirect(Links[1].url);*/
                } else {
                    toast.warn(dataJson.message, {
                        position: "top-right",
                        autoClose: 5000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true
                    });
                }
            }
        })
    }

    const infoErrorParsleConfigure = () => {

        var ul_error_ci = window.jQuery("input[name='persona[numero_documento]']").parent().parent().find('ul');
        window.jQuery("input[name='persona[numero_documento]']").parent().parent().find('ul').remove();
        window.jQuery("input[name='persona[numero_documento]']").parent('div').parent('div').append(ul_error_ci);

        var ul_error_ci_rl = window.jQuery("input[name='pres_representante_legal[numero_documento]']").parent().parent().find('ul');
        window.jQuery("input[name='pres_representante_legal[numero_documento]']").parent().parent().find('ul').remove();
        window.jQuery("input[name='pres_representante_legal[numero_documento]']").parent('div').parent('div').append(ul_error_ci_rl);

        //numero de mnit
        var ul_error_nit = window.jQuery("input[name='pres_persona_juridica[nit]']").parent().parent().find('ul');
        window.jQuery("input[name='pres_persona_juridica[nit]']").parent().parent().find('ul').remove();
        window.jQuery("input[name='pres_persona_juridica[nit]']").parent('div').parent('div').append(ul_error_nit);

        var ul_error_nit_rl = window.jQuery("input[name='pres_persona_juridica_r[nit]']").parent().parent().find('ul');
        window.jQuery("input[name='pres_persona_juridica_r[nit]']").parent().parent().find('ul').remove();
        window.jQuery("input[name='pres_persona_juridica_r[nit]']").parent('div').parent('div').append(ul_error_nit_rl);

        var ul_error_mail = window.jQuery("input[name='domicilio[email]']").parent().parent().find('ul');
        window.jQuery("input[name='domicilio[email]']").parent().parent().find('ul').remove();
        window.jQuery("input[name='domicilio[email]']").parent('div').parent('div').append(ul_error_mail);

        var ul_error_mail = window.jQuery("input[name='domicilio_representante_legal[email]']").parent().parent().find('ul');
        window.jQuery("input[name='domicilio_representante_legal[email]']").parent().parent().find('ul').remove();
        window.jQuery("input[name='domicilio_representante_legal[email]']").parent('div').parent('div').append(ul_error_mail);
    }

    const breadcrumbs = [
        {
            title: Links[0].title,
            url: Links[0].url
        },
        {
            title: Links[3].title,
            url: Links[3].url
        },
        {
            title: 'Edición',
            url: '#'
        }
    ];

    return (
        <div id="contact" className="contact paddingTop" >

            <TitlePage titlePage={Texto.prescripcion} breadcrumbs={breadcrumbs} position={'left'} />

            <div className="container">
                {contribuyenteShow ?
                    <FormContribuyente prescripcion={null} toast={toast} prescripcionDb={prescripcionDb} domicilioDb={domicilioDb}
                        onSubmitForm={handleSubmitEditForm} buttonName={'Siguiente'} derecho_admision={prescripcionDb.Solicitante.contribuyente}
                        titleNavegacion={tituloNavigation} titleNavegacion2={tituloNavigation2} />
                    : ""}

                {domicilioContribuyenteShow ?
                    <FormDomicilioContribuyente prescripcion={null} toast={toast} prescripcionDb={prescripcionDb} domicilioDb={domicilioDb} personaDb={personaDb}
                        onSubmitForm={handleSubmitEditForm} buttonName={'Siguiente'}
                        titleNavegacion={tituloNavigation} titleNavegacion2={tituloNavigation2} nameForm="domicilio" />
                    : ""}

                {objetosTributariosShow ?
                    <FormObjetoTributario prescripcion={prescripcionDb} objetoTributarioGestion={objetoTributarioGestion} inmueble={inmueble}
                        inmuebleDomicilio={inmuebleDomicilio}
                        toast={toast} onSubmitForm={handleSubmitEditForm} buttonName={'Siguiente'}
                        titleNavegacion={tituloNavigation} titleNavegacion2={tituloNavigation2} nameForm={"inmueble"}
                        nameFormTribGestion={"pres_objeto_tributario_gestion"} nameFormDireccion={"inmueble_direccion"} />
                    : ""}

                {requisitosShow ?
                    <FormRequisito prescripcion={prescripcionDb} toast={toast} onSubmitForm={handleSubmitEditForm} buttonName={'Siguiente'}
                        requisitoDb={setPrescripcionRequisitoDb} prescripcionRequisitoDb={prescripcionRequisitoDb}
                        titleNavegacion={tituloNavigation} titleNavegacion2={tituloNavigation2}
                        nameForm={"inmueble"} nameFormRequisito={'pres_prescripcion_requisito'} />
                    : ""}

                {vistaPreviaShow ?
                    <VistaPrevia prescripcion={prescripcionDb} data={prescripcionDb} solicitante={solicitanteDb} toast={toast}
                        titleNavegacion={tituloNavigation} titleNavegacion2={tituloNavigation2}
                        nameForm={'vista_previa'} />
                    : ""}
            </div>

            <ToastContainer enableMultiContainer containerId={'Z'}
                position="top-right"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnVisibilityChange
                draggable
                pauseOnHover
            />
            <ToastContainer />
        </div>
    )
}

export default PrescripcionUpdate;