import React, { useState, createRef, useEffect, useRef } from 'react';

import Fetch from '../../components/utils/Fetch';
import AuthService from '../../components/Usuario/AuthService';
import Constant from '../../data/constant';
import Config from '../../data/config';

import DatePicker, { registerLocale } from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import Texto from '../../data/es';
import Languaje from '../../data/es';

//var derecho_admision = 0
const FormRepresentanteLegal = (props) => {

    const refInputRazonSocial = useRef()
    const refInputFechaConstitucion = useRef()
    const refSlcTipoSociedad = useRef()
    const refInputNit = useRef()

    const fetch = new Fetch();
    fetch.setToast(toast);
    const auth = new AuthService();

    const { natural, personaJuridica } = props

    const [startDateJuridico, setStartDateJuridico] = useState(null)
    const [optionsTipoSociedad, setOptionsTipoSociedad] = useState("")

    useEffect(() => {
        loadTipoSociedad()
        loadForm(personaJuridica)
    }, []);

    const handleSearchByNitOnClick = (event) => {
        event.preventDefault();
        let nit = "";
        debugger
        if (event.target.tagName === 'I')
            nit = event.target.parentElement.parentElement.parentElement.firstElementChild.value;
        else
            nit = event.target.parentElement.parentElement.firstElementChild.value;

        if (nit !== '') {
            window.jQuery("input[codeForm='" + props.codeform + "']:not(input[name='persona[numero_documento]']):not(input[name='persona_juridica[nit]'])").val("")
            window.jQuery("select[codeForm='" + props.codeform + "']:not(select[name='persona[id_tipo_documento]'])").prop('selectedIndex', 0);
            const search_persona = fetch.fetchGet(`api/recaudaciones/persona-by-nit/${nit}`);
            search_persona.then(dataJson => {
                if (dataJson != undefined && dataJson.status === true && dataJson.persona_natural !== null) {  //is ok


                    //self.loadDataForm(dataJson.representante_legal, self, dataJson.representante_legal.recaudacion === true ? false : true);

                    //document.getElementsByName("persona[numero_documento]")[0].value = dataJson.representante_legal.ci !== undefined ? dataJson.representante_legal.ci : dataJson.representante_legal.numero_documento;
                    //document.getElementsByName("persona_juridica[razon_social]")[0].value = dataJson.persona_juridica.razon_social;
                    refInputRazonSocial.current.value = dataJson.persona_juridica.razon_social;
                    let nameTipoSociedades = refSlcTipoSociedad.current
                    if (dataJson.persona_juridica.fecha_constitucion !== null) {
                        let fecha = dataJson.persona_juridica.fecha_constitucion.replace(/-/g, '/');
                        //this.setState({ startDateJuridico: new Date(fecha) });
                        setStartDateJuridico(new Date(fecha))
                    }

                    if (dataJson.persona_juridica.sociedad !== undefined && dataJson.persona_juridica.sociedad !== null) {
                        let options = nameTipoSociedades.options
                        for (let i = 0; i < options.length; i++)
                            if (options[i].hasAttribute("code") && options[i].getAttribute("code") === dataJson.persona_juridica.sociedad)
                                nameTipoSociedades.value = options[i].value
                        //window.jQuery("select[name='persona_juridica[id_tipo_sociedad]'] option[code='" + dataJson.persona_juridica.sociedad + "']").prop('selected', true);
                    } else {
                        window.jQuery("select[name='persona_juridica[id_tipo_sociedad]']").val(parseInt(dataJson.persona_juridica.id_tipo_sociedad)).trigger('change');  //datos db local
                    }

                    /*if (dataJson.representante_legal.num_sec_tipo_documento != undefined && dataJson.representante_legal.num_sec_tipo_documento === 1)
                        window.jQuery("select[name='representante_legal[id_tipo_documento]']").val(dataJson.representante_legal.num_sec_tipo_documento).trigger('change');
                    else
                        if(dataJson.representante_legal.id_tipo_documento !== undefined && dataJson.representante_legal.id_tipo_documento !== null) //datos db local
                            window.jQuery("select[name='representante_legal[id_tipo_documento]']").val(dataJson.representante_legal.id_tipo_documento).trigger('change');
                        else
                            window.jQuery("select[name='representante_legal[id_tipo_documento]']").val(2).trigger('change');
*/
                    props.toast.success(dataJson.message, {
                        position: "top-right",
                        autoClose: 5000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true
                    });
                }
                window.inputConvertUpperCaseForm('formSolicitante')
            })
        }
    }

    const handleDatePickerDatosJuridicosChange = (date) => {
        setStartDateJuridico(date)
    }

    const loadTipoSociedad = () => {
        const tipo_sociedad_resp = fetch.fetchGet(`api/tipo-sociedad/all`);
        tipo_sociedad_resp.then(res => {
            if (res !== undefined && res.status === true && res.TipoSociedad !== null) {  //is ok
                const listGenero = res.TipoSociedad.map((item, index) => {
                    return <option key={index} value={item.id} code={item.code}>{item.name}</option>
                });
                setOptionsTipoSociedad(listGenero)

                if (personaJuridica !== undefined && personaJuridica !== null)
                    refSlcTipoSociedad.current.value = parseInt(personaJuridica.id_tipo_sociedad)
            }
        })
    }

    const loadForm = (personaJuridica) => {
        if (personaJuridica !== undefined && personaJuridica !== null) {
            refInputRazonSocial.current.value = personaJuridica.razon_social
            refInputNit.current.value = personaJuridica.nit
            setStartDateJuridico(new Date(personaJuridica.fecha_constitucion))
        }
    }

    return (
        <>
            {natural === false ?
                <div className="row">
                    <div className="col-sm-12 col-md-4 col-lg-4 form-group">
                        <label htmlFor={props.nameForm + '[nit]'}>NIT *</label>
                        <div className="input-group mb-3">
                            <input type="text" className="form-control input-uppercase" name={props.nameForm + '[nit]'} placeholder="Número de Nit"
                                aria-label="Número de Nit" aria-describedby="basic-addon2" required data-parsley-required="true" 
                                codeform={props.codeform} ref={refInputNit}  />
                            <div className="input-group-append">
                                <button className="btn btn-outline-secondary" type="button" onClick={handleSearchByNitOnClick}><i className="fa fa-search" aria-hidden="true" ></i></button>
                            </div>
                        </div>
                    </div>

                    <div className="col-sm-12 col-md-4 col-lg-4 form-group">
                        <label htmlFor={props.nameForm + '[razon_social]'} >Nombre o Razón Social *</label>
                        <input name={props.nameForm + '[razon_social]'} type="text" className="form-control input-uppercase"
                            placeholder="Razón Social" required data-parsley-required="true" codeform={props.codeform} ref={refInputRazonSocial} />
                    </div>

                    <div className="col-sm-12 col-md-4 col-lg-4 ">
                        <label htmlFor={props.nameForm + '[fecha_constitucion]'}>Fecha de Constitución (DD/MM/YYYY) *</label>
                        <DatePicker
                            locale="es"
                            dateFormat={Config[4].format}
                            selected={startDateJuridico}
                            onChange={handleDatePickerDatosJuridicosChange}
                            maxDate={new Date()}
                            className="form-control"
                            name={props.nameForm + '[fecha_constitucion]'}
                            data-parsley-required="true"
                            showYearDropdown
                            showMonthDropdown
                            dropdownMode="select"
                            required
                            codeform={props.codeform}
                            ref={refInputFechaConstitucion} />

                    </div>
                </div>
                : ""}

            {natural === false ?
                <div className="row">
                    <div className="col-sm-12 col-md-4 col-lg-4 form-group">
                        <label htmlFor={props.nameForm + '[id_tipo_sociedad]'}>Tipo de Sociedad:</label>
                        <select className="form-control" name={props.nameForm + '[id_tipo_sociedad]'} required data-parsley-required="true"
                            codeform={props.codeform} ref={refSlcTipoSociedad} >
                            <option defaultValue value="">Tipo de Sociedad</option>
                            {optionsTipoSociedad}
                        </select>
                    </div>
                    <br />
                </div>
                : ""}
        </>
    )
}


export default FormRepresentanteLegal;