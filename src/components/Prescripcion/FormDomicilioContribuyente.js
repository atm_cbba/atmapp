import React, { useEffect, useState, useRef } from 'react';
import PasosNavigationPrescripcion from '../../components/utils/PasosNavigationPrescripcion';
import Constant from '../../data/constant';
//import Languaje from '../../data/es';
import { Link } from 'react-router-dom';
import MapOpenLayer from '../component/MapOpenLayer'

const FormDomicilioContribuyente = (props) => {

    const constant = Constant[0];
    const { prescripcionDb, domicilioDb, personaDb } = props

    const [coordinate, setCoordinate] = useState("")
    const [showMapa, setShowMapa] = useState(false)

    const refInputHiddenLatitud = useRef()
    const refInputHiddenLongitud = useRef()
    const refInputHiddenCoordinate = useRef()
    const refInputHiddenPersonaId = useRef()
    const refInputHiddenPrescripcionToken = useRef()
    const refImgMapa = useRef()

    const handleMapOnClick = (event) => {
        window.jQuery('#modalMapDomicilio').modal('show');
        if (showMapa === false)
            setShowMapa(true)
    }

    useEffect(() => {
        if (prescripcionDb !== undefined && domicilioDb !== undefined) {
            if (domicilioDb.latitud !== null && domicilioDb.longitud !== null) {
                refInputHiddenLatitud.current.value = domicilioDb.latitud
                refInputHiddenLongitud.current.value = domicilioDb.longitud
                refInputHiddenCoordinate.current.value = domicilioDb.coordinate

                if (refImgMapa.current !== undefined)
                    refImgMapa.current.setAttribute("src", "data:image/png;base64, " + domicilioDb.image)

                setShowMapa(true)
                setCoordinate(domicilioDb.coordinate)
            }
        }
    }, [showMapa]);

    //aqui ha de existir un cambio esta dirección ha de tomar de acuerdo a si existe o no el representate legal, 
    let id_persona = 0
    if (props.prescripcion !== null && props.prescripcion !== undefined) {
        if (props.prescripcion.personaRL !== null && props.prescripcion.personaRL !== undefined && props.prescripcion.personaRL !== "") {
            id_persona = props.prescripcion.personaRL
        } else
            id_persona = props.prescripcion.persona
    }else{
        id_persona = personaDb.id
    }

    return (
        <div className="row">
            <PasosNavigationPrescripcion titulo_paso1={props.titleNavegacion} titulo_paso2={props.titleNavegacion2}
                paso1_active={true} paso2_active={true} paso3_active={false} paso4_active={false} paso5_active={false} />
            <form action="" className="contact__form needs-validation" name="formPresDomicilioSolicitante" id="formPresDomicilioSolicitante"
                method="post" noValidate onSubmit={(e) => props.onSubmitForm(e, props)} style={{ width: '100%' }}>

                <div className="row">
                    <div className="col-12 col-sm-12 col-md-12 col-lg-12 form-group text-center" >
                        {showMapa ? <img id={props.nameForm + "[image]"} className='img-thumbnail rounded mx-auto d-block' alt='img-domicilio'
                            src={""} ref={refImgMapa} /> :
                            <div className="folded-corner service_tab_1 folded-corner-rounded">
                                <Link to={'#'} title="Click Aqui Para Ubicar tu Domicilio" onClick={handleMapOnClick}>
                                    <div className="text">
                                        <i className="fa fa-map fa-5x fa-icon-image"></i>
                                    </div>
                                </Link>
                                <i className="fa fa-arrow-up" aria-hidden="true" style={{ display: 'block', fontSize: '3em', color: '#de6225' }}></i>
                                <Link to={'#0'} title="Click Aqui" className="item-title" onClick={handleMapOnClick}>Click Aqui</Link>
                            </div>
                        }
                        <input type="hidden" name={props.nameForm + "[image]"} />
                    </div>
                </div>

                <div className="row">
                    <input name={props.nameForm + "[latitud]"} type="hidden" ref={refInputHiddenLatitud} />
                    <input name={props.nameForm + "[longitud]"} type="hidden" ref={refInputHiddenLongitud} />
                    <input name={props.nameForm + "[coordinate]"} type="hidden" ref={refInputHiddenCoordinate} />
                    <input name="persona[id]" type="hidden" value={id_persona} ref={refInputHiddenPersonaId} />
                    <input name="pres_prescripcion[token]" type="hidden" value={props.prescripcion !== null ? props.prescripcion.token : prescripcionDb.token} ref={refInputHiddenPrescripcionToken} />
                </div>

                <div className="row">
                    <div className="col-sm-12 col-md-12 col-lg-12 form-group">
                        <button className="button-style btn-disabled pull-left margin-buttom-15" type="button" id={'btn_domicilio_mapa'} onClick={handleMapOnClick}>
                            <i className="fa fa-map-marker" aria-hidden="true" ></i> Ubica tú Domicilio en el Mapa</button>
                        <input name="Siguiente" type="submit" className="button-style pull-right" value={props.buttonName} />
                    </div>
                </div>
            </form>

            <MapOpenLayer coordinate={coordinate} module={'prescripcion'} />
        </div>
    )
}

export default FormDomicilioContribuyente