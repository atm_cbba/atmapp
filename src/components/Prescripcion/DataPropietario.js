import React, { Component, useEffect, useState, useRef, useCallback, useMemo } from 'react';
import PasosNavigationPrescripcion from '../../components/utils/PasosNavigationPrescripcion';

import { Link } from 'react-router-dom';
import MapOpenLayer from '../component/MapOpenLayer'
import MapCatastroPublic from '../component/MapCatastroPublic'
import ModalSearchDireccion from '../../components/component/ModalSearchDireccion'
import Links from '../../data/link';

import AsyncSelect from 'react-select/async';
import Fetch from '../../components/utils/Fetch';
import AuthService from '../../components/Usuario/AuthService';
import Languaje from '../../data/es';
import TableDeudas from './TableDeudas';
import TableColindantes from './TableColindantes';

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


const DataPropietario = (props) => {

    const { dataInmueble } = props
    const [showCopropietarios, setShowCopropietarios] = useState(false)
    const [propietario, setPropietario] = useState(false)
    const [pmc, setPmc] = useState("")
    const [tipoContribuyente, setTipoContribuyente] = useState("")

    const handleEventClickCopropietario = (event) => {
        event.preventDefault()
        setShowCopropietarios(!showCopropietarios)
    }

    useEffect(() => {
        let cont = 1
        if (!dataInmueble.accionesYDerechos) {

            let natural = dataInmueble.propiedades.map(({ nombrePropietario, fechaInicioPropiedad, porcentaje, propietario }) => {
                let contribuyente = propietario.natural
                return dataPropietario(cont++, { nombrePropietario, fechaInicioPropiedad, porcentaje, contribuyente })
            })
            setPropietario(natural)
            setPmc(dataInmueble.propiedades[0].propietario.pmc)
            if (dataInmueble.propiedades[0].propietario.natural)
                setTipoContribuyente("NATURAL")
        } else {
            let juridico = dataInmueble.propiedades.map(({ nombrePropietario, fechaInicioPropiedad, porcentaje, propietario }) => {
                let contribuyente = propietario.juridico
                return dataPropietario(cont++, { nombrePropietario, fechaInicioPropiedad, porcentaje, contribuyente })
            })
            setPropietario(juridico)
        }
    }, []);

    const dataPropietario = (identifiador, dataPropietario) => {
        return <div key={identifiador} >
            <div className="row ">
                <div className="col-sm-12 col-md-4 col-lg-4 padding-left-right-0">
                    <p className="text-left" ><strong >Tipo de documento: </strong><span>{dataPropietario.contribuyente.tipoDocumento}</span></p>
                    <input type="hidden" name={`persona[${identifiador}][id_tipo_documento]`} value={`${dataPropietario.contribuyente.tipoDocumento}`} />
                </div>

                <div className="col-sm-12 col-md-4 col-lg-4 padding-left-right-0">
                    <p className="text-left"><strong >Documento Identificación: </strong><span>{dataPropietario.contribuyente.numeroDocumento}</span></p>
                    <input type="hidden" name={`persona[${identifiador}][numero_documento]`} value={`${dataPropietario.contribuyente.numeroDocumento}`} />
                </div>

                <div className="col-sm-12 col-md-4 col-lg-4 padding-left-right-0">
                    <p className="text-left"><strong >Expedido En: </strong> <span>{dataPropietario.contribuyente.expedido}</span></p>
                    <input type="hidden" name={`persona[${identifiador}][expedido_en]`} value={`${dataPropietario.contribuyente.expedido}`} />
                </div>
            </div>

            <div className="row">
                <div className="col-sm-12 col-md-12 col-lg-12 padding-left-right-0">
                    <p className="text-left">
                        <strong >Nombre: </strong><span>{dataPropietario.nombrePropietario}</span>
                        <input type="hidden" name={`persona[${identifiador}][nombre]`} value={`${dataPropietario.nombrePropietario}`} />
                    </p>
                </div>
            </div>

            <div className="row">
                <div className="col-sm-12 col-md-4 col-lg-4 padding-left-right-0">
                    <p className="text-left"><strong >Género: </strong><span>{dataPropietario.contribuyente.genero}</span></p>
                    <input type="hidden" name={`persona[${identifiador}][genero]`} value={`${dataPropietario.contribuyente.genero}`} />
                </div>

                <div className="col-sm-12 col-md-4 col-lg-4 padding-left-right-0">
                    <p className="text-left"> <strong >Estado Civil: </strong><span>{dataPropietario.contribuyente.estadoCivil}</span></p>
                    <input type="hidden" name={`persona[${identifiador}][estado_civil]`} value={`${dataPropietario.contribuyente.estadoCivil}`} />
                </div>

                <div className="col-sm-12 col-md-4 col-lg-4 padding-left-right-0">
                    <p className="text-left"><strong>Fecha Inicio Propiedad: </strong><span>{dataPropietario.contribuyente.fechaInicioPropiedad}</span></p>
                    <input type="hidden" name={`contribuyente_natural[${identifiador}][fecha_inicio_propiedad]`} value={`${dataPropietario.contribuyente.fechaInicioPropiedad}`} />
                </div>
            </div>

            <div className="row">
                <div className="col-sm-12 col-md-12 col-lg-12 padding-left-right-0">
                    <p className="text-left">
                        <strong >Copropietarios: </strong>
                    </p>
                </div>
            </div>
        </div>
    }

    return (
        <div className="card widget widget-simple" >
            <div className="card-body ">

                <div className="row ">
                    <div className="col-4 widget-header">
                        <h4 className="">{Languaje.datos_propietario}</h4>
                    </div>

                    <div className="col-4 widget-header">
                        <h6 className="mt-2">{tipoContribuyente}</h6>
                    </div>
                    <div className="col-4 widget-header">
                        <h6 className="mt-2">{pmc}</h6>
                    </div>
                </div>

                <div className="widget-content">
                    <div className="widget-body">
                        {propietario}
                    </div>
                </div>
            </div>
        </div>
    )
}
export default DataPropietario