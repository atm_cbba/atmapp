import React, {  } from 'react';

import Languaje from '../../data/es';
import TableDeudas from './TableDeudas';

import 'react-toastify/dist/ReactToastify.css';

const DataLiquidacion = (props) => {

    const { dataDeudaInmueble } = props

    return (
        <div className="card widget widget-simple" >
            <div className="card-body ">

                <div className="row ">
                    <div className="col-12 widget-header">
                        <h4 className="">{Languaje.gestiones_a_prescribir}</h4>
                    </div>
                </div>

                <div className="widget-content">
                    <div className="widget-body">
                        <div className="row" style={{marginLeft: '-15px', marginRight: '0px'}}>
                            <div className="col-12 padding-left-right-0">
                                <TableDeudas data={dataDeudaInmueble} nameFormObjectoGestion = {"pres_objeto_tributario_gestion"} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default DataLiquidacion