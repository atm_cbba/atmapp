import React, { Component, useState, useEffect, createRef, useRef, useContext } from 'react';
import PasosNavigationPrescripcion from '../../components/utils/PasosNavigationPrescripcion';
import FormRepresentanteLegal from './FormRepresentanteLegal';
import Languaje from '../../data/es';
import Constant from '../../data/constant';
import Config from '../../data/config';
import Fetch from '../../components/utils/Fetch';
import AuthService from '../../components/Usuario/AuthService';

import DatePicker, { registerLocale } from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { FormPersonaContext } from '../utils/FormPersonaContext';
//import Links from '../../data/link';

let titulo_navigation = "";

const FormPersona = (props) => {

    const { persona, domicilio, personaJuridica } = props

    const fetch = new Fetch();
    fetch.setToast(toast);
    const auth = new AuthService();

    //let { derecho_admision } = props

    const refSlcTipoDocumento = useRef();
    const refInputNumeroDoc = useRef()
    const refDropDownExpedido = useRef()
    const refSlcNacionalidad = useRef()
    const refInputNombre = useRef()
    const refInputApPaterno = useRef()
    const refInputApMaterno = useRef()
    const refSlcEstadoCivil = useRef()
    const refInputApCasada = useRef()
    const refSlcGenero = useRef()
    const refInputFechaNacimiento = useRef()
    const refInputTelefono = useRef()
    const refInputCelular = useRef()
    const refInputTipoExpedido = useRef()
    const refInputEmail = useRef()
    const refCheckBoxEmail = useRef()

    const [optionsTipoDocumento, setOptionsTipoDocumento] = useState("");
    const [optionsNacionalidad, setOptionsNacionalidad] = useState("")
    const [optionsEstadoCivil, setOptionsEstadoCivil] = useState("")
    const [optionsGenero, setOptionsGenero] = useState("")
    const [showApellidoCasada, setShowApellidoCasada] = useState(false)
    const [optionsTipoSociedad, setOptionsTipoSociedad] = useState("")
    const [ciExpedido, setCiExpedido] = useState('EX')

    const [startDatePersona, setStartDatePersona] = useState(null)

    useEffect(() => {
        loadTipoDocumento()
        loadEstadoCivil()
        loadTipoSociedad()
        loadNacionalidad()
        loadGenero()

        addEventExpedido()
        loadDataPerson(persona, domicilio);
    }, [ciExpedido]);

    const loadTipoDocumento = () => {
        const tipo_doc_res = fetch.fetchGet(`api/TipoDocumento/getAll`);
        tipo_doc_res.then(res => {

            if (res !== undefined && res !== undefined && res.status === true) {
                if (res.status === true && res.TipoDocumento !== null) {  //is ok
                    const listItems = res.TipoDocumento.map((item, index) => {
                        return <option key={index} value={item.id} code={item.code}>{item.name}</option>
                    });
                    debugger

                    if (persona !== undefined) {
                        if (persona !== null && Boolean(persona.id_tipo_documento)) {

                            setOptionsTipoDocumento(listItems)
                            //setCiExpedido(persona.expedido_en)

                            if(refInputNumeroDoc.current !== null)
                                refInputNumeroDoc.current.value = persona.numero_documento
                            if(refSlcTipoDocumento.current)
                                refSlcTipoDocumento.current.value = persona.id_tipo_documento
                            if(refDropDownExpedido.current){
                                let dropdownExpedidoEn = refDropDownExpedido.current
                                dropdownExpedidoEn.parentElement.firstElementChild.innerText = persona.expedido_en
                            }
                            if(refInputTipoExpedido.current)
                                refInputTipoExpedido.current.value = persona.expedido_en
                        } else
                            setOptionsTipoDocumento(listItems)
                    } else
                        setOptionsTipoDocumento(listItems)
                }
            }
        })
    }

    const loadEstadoCivil = () => {
        const estado_civil_resp = fetch.fetchGet(`api/list/estado-civil`);
        estado_civil_resp.then(res => {
            if (res !== undefined && res.status === true && res.estadoCivil !== null) {  //is ok

                const listEC = res.estadoCivil.map((item, index) => {
                    return <option key={index} value={item.id} code={item.code}>{item.name}</option>
                });
                setOptionsEstadoCivil(listEC)
                if (persona !== undefined && persona !== null) {
                    refSlcEstadoCivil.current.value = persona.estado_civil
                    var event = new Event('onchange', {
                        bubbles: true,
                        cancelable: true,
                    });
                    refSlcEstadoCivil.current.dispatchEvent(event);
                    handleEstadoCivilOnChange(event)
                }
            }
        })
    }

    const loadNacionalidad = () => {
        const nacionalidad_resp = fetch.fetchGet(`api/list/nacionalidad`);
        nacionalidad_resp.then(res => {
            if (res !== undefined && res.status === true && res.Nacionalidad !== null) {  //is ok
                const listNacionlidad = res.nacionalidad.map((item, index) => {
                    return <option key={index} value={item.id} code={item.code}>{item.name}</option>
                });
                setOptionsNacionalidad(listNacionlidad)
                if (persona !== undefined && persona !== null)
                    refSlcNacionalidad.current.value = persona.nacionalidad;
            }
        })
    }

    const loadTipoSociedad = () => {
        const tipo_sociedad_resp = fetch.fetchGet(`api/tipo-sociedad/all`);
        tipo_sociedad_resp.then(res => {
            if (res !== undefined && res.status === true && res.TipoSociedad !== null) {  //is ok
                const listItems = res.TipoSociedad.map((item, index) => {
                    return <option key={index} value={item.id} code={item.code}>{item.name}</option>
                });

                setOptionsTipoSociedad(listItems)
                /*if (prescripcionDb !== null)
                    if (this.props.solicitanteDb.datos_juridicos !== null && this.props.solicitanteDb.datos_juridicos !== undefined)
                        window.jQuery("select[name='datos_juridicos[id_tipo_sociedad]']").val(this.props.solicitanteDb.datos_juridicos.id_tipo_sociedad).trigger('change');
                */
            }
        })
    }

    const loadGenero = () => {
        const genero_resp = fetch.fetchGet(`api/list/genero`);
        genero_resp.then(res => {
            if (res !== undefined && res.status === true && res.Genero !== null) {  //is ok
                const listGenero = res.genero.map((item, index) => {
                    return <option key={index} value={item.id} code={item.code}>{item.name}</option>
                });
                setOptionsGenero(listGenero)
                if (persona !== undefined && persona !== null)
                    refSlcGenero.current.value = parseInt(persona.genero)
                //window.jQuery("select[name='persona[genero]']").val(this.props.solicitanteDb.persona.genero).trigger('change');
            }
        })
    }

    const handleTipoDocumentoOnChange = (event) => {
        /*event.preventDefault();
        debugger
        const option_selected = event.target.selectedOptions[0]
        if (option_selected.getAttribute('code') === 'CI' || option_selected.getAttribute('code') === 'CIE')
            setShowFormPersona(true)
        else
            setShowFormPersona(false)*/
    }

    const handleSearchByCiOnClick = (event) => {
        event.preventDefault();
        var ci = "";
        var self = this;
        let input_search = "";
        if (event.target.tagName === 'I') {
            ci = event.target.parentElement.parentElement.parentElement.firstElementChild.value;
            input_search = event.target.parentElement.parentElement.parentElement.firstElementChild.getAttribute('name');
        } else {
            ci = event.target.parentElement.parentElement.firstElementChild.value;
            input_search = event.target.parentElement.parentElement.firstElementChild.getAttribute('name');
        }

        if (ci !== '') {
            window.jQuery("input[codeForm='" + props.codeform + "']:not(input[name='" + props.nameForm + "[numero_documento]']):not(input[name*='[nit]']):not(input[name*='[razon_social]']):not(input[type=hidden])").val("")
            window.jQuery("select[codeForm='" + props.codeform + "']:not(select[name='" + props.nameForm + "[id_tipo_documento]']):not(select[name*='[id_tipo_sociedad]'])").prop('selectedIndex', 0);
            const search_persona = fetch.fetchGet(`api/recaudaciones/persona-by-ci/${ci}`);
            search_persona.then(dataJson => {
                debugger
                if (dataJson != undefined && dataJson.status === true && dataJson.persona_natural !== null) {  //is ok

                    if (input_search.indexOf('persona') >= 0 || input_search.indexOf('pres_representante_legal') >= 0) {
                        loadDataForm(dataJson.persona_natural, self, dataJson.persona_natural.recaudacion === true ? false : true);
                    }

                    props.toast.success(dataJson.message, {
                        position: "top-right",
                        autoClose: 5000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true
                    });
                }
                window.inputConvertUpperCaseForm('formSolicitante')
            })
        }
    }

    const handleEstadoCivilOnChange = (event) => {
        event.preventDefault();
        debugger
        if (event.target.selectedOptions.length > 0) {
            var constant = Constant[0];
            switch (event.target.selectedOptions[0].value) {
                case constant.estado_civil.casado:
                    setShowApellidoCasada(true)
                    break;
                case constant.estado_civil.viudo:
                    setShowApellidoCasada(true)
                    break;
                default:
                    setShowApellidoCasada(false)
                    break;
            }
        }
    }

    const handleDatePickerPersonaChange = (date) => {
        setStartDatePersona(date)
    }

    const addEventExpedido = () => {
        debugger
        var $_dropdown_menu_per_sol = refDropDownExpedido.current
        var $_dropdown_menu_per_sol = window.jQuery(refDropDownExpedido.current);
        if ($_dropdown_menu_per_sol.length > 0) {
            window.event_drop_down($_dropdown_menu_per_sol, function (code_ci_exp, target) {
                if (ciExpedido === 'EX')
                    refInputTipoExpedido.current.value = code_ci_exp
            });
        }

        /*var $_dropdown_menu_per_sol_rl = window.jQuery("#persona-dropdown-solicitante-tipo-doc-perNatRLJ");
        if ($_dropdown_menu_per_sol_rl.length > 0) {
            window.event_drop_down($_dropdown_menu_per_sol_rl, function (code_ci_exp, target) {
                if (ciExpedido === 'EX')
                refInputTipoExpedido.current.value = code_ci_exp
            });
        }*/

        if (ciExpedido !== 'EX') {
            switch (ciExpedido) {
                case Constant[0].ci_expedido.cbba.old:
                    $_dropdown_menu_per_sol.parent().children(":first").text(Constant[0].ci_expedido.cbba.new);
                    document.getElementsByName("persona[expedido_en]")[0].value = Constant[0].ci_expedido.cbba.new;
                    break;
                case Constant[0].ci_expedido.lapaz.old:
                    $_dropdown_menu_per_sol.parent().children(":first").text(Constant[0].ci_expedido.lapaz.new);
                    document.getElementsByName("persona[expedido_en]")[0].value = Constant[0].ci_expedido.lapaz.new;
                    break;
                case Constant[0].ci_expedido.santacruz.old:
                    $_dropdown_menu_per_sol.parent().children(":first").text(Constant[0].ci_expedido.santacruz.new);
                    document.getElementsByName("persona[expedido_en]")[0].value = Constant[0].ci_expedido.santacruz.new;
                    break;
                case Constant[0].ci_expedido.oruro.old:
                    $_dropdown_menu_per_sol.parent().children(":first").text(Constant[0].ci_expedido.oruro.new);
                    document.getElementsByName("persona[expedido_en]")[0].value = Constant[0].ci_expedido.oruro.new;
                    break;
                case Constant[0].ci_expedido.potosi.old:
                    $_dropdown_menu_per_sol.parent().children(":first").text(Constant[0].ci_expedido.potosi.new);
                    document.getElementsByName("persona[expedido_en]")[0].value = Constant[0].ci_expedido.potosi.new;
                    break;
                case Constant[0].ci_expedido.pando.old:
                    $_dropdown_menu_per_sol.parent().children(":first").text(Constant[0].ci_expedido.pando.new);
                    document.getElementsByName("persona[expedido_en]")[0].value = Constant[0].ci_expedido.pando.new;
                    break;
                case Constant[0].ci_expedido.beni.old:
                    $_dropdown_menu_per_sol.parent().children(":first").text(Constant[0].ci_expedido.beni.new);
                    document.getElementsByName("persona[expedido_en]")[0].value = Constant[0].ci_expedido.beni.new;
                    break;
                case Constant[0].ci_expedido.tarija.old:
                    $_dropdown_menu_per_sol.parent().children(":first").text(Constant[0].ci_expedido.tarija.new);
                    document.getElementsByName("persona[expedido_en]")[0].value = Constant[0].ci_expedido.tarija.new;
                    break;
                case Constant[0].ci_expedido.sucre.old:
                    $_dropdown_menu_per_sol.parent().children(":first").text(Constant[0].ci_expedido.sucre.new);
                    document.getElementsByName("persona[expedido_en]")[0].value = Constant[0].ci_expedido.sucre.new;
                    break;
                default:
                    if (Boolean(ciExpedido)) {
                        $_dropdown_menu_per_sol.parent().children(":first").text(ciExpedido);
                        document.getElementsByName("persona[expedido_en]")[0].value = ciExpedido;
                    } else
                        $_dropdown_menu_per_sol.parent().children(":first").text("EX");
                    break;
            }
            //setCiExpedido('EX')
        }
    }

    /**
     * 
     * @param {*} persona 
     * @param {*} self 
     * @param {*} local cuando se ha de cargar datos desde la base de datos
     */
    const loadDataForm = (persona, self, local) => {

        debugger
        if (local) {
            let persona_db = undefined;
            if (persona.hasOwnProperty('Persona')) {
                if (persona.Persona !== null)
                    persona_db = persona.Persona;
            } else
                persona_db = persona;

            if (persona_db !== undefined) {
                refInputNombre.current.value = persona_db.nombre
                refInputApPaterno.current.value = persona_db.apellido_paterno
                refInputApMaterno.current.value = persona_db.apellido_materno

                /*if (Boolean(persona_db.apellido_casada)) {
                    self.setState({ showApellidoCasada: true, startDatePersona: new Date(persona_db.fecha_nacimiento), 
                        apellidoCasada: persona_db.apellido_casada, ciExpedido: persona_db.expedido_en });
                    window.jQuery("select[name='persona[estado_civil]']").val(Constant[0].estado_civil.casado).trigger('change');
                }else{*/
                //self.setState({ startDatePersona: new Date(persona_db.fecha_nacimiento), ciExpedido: persona_db.expedido_en })
                setStartDatePersona(new Date(persona_db.fecha_nacimiento))
                setCiExpedido(persona_db.expedido_en)
                //}
                debugger

                if (persona_db.nacionalidad !== undefined) {
                    refSlcNacionalidad.current.value = parseInt(persona_db.nacionalidad)
                    //refSlcNacionalidad.current.trigger('change')
                }
                //window.jQuery("select[name='persona[nacionalidad]']").val(parseInt(pe//refSlcNacionalidad.current.value = parseInt(persona_db.nacionalidad)

                if (persona_db.genero !== undefined)
                    refSlcGenero.current.value = parseInt(persona_db.genero)
                //window.jQuery("select[name='persona[genero]']").val(parseInt(persona_db.genero) ).trigger('change');

                if (persona_db.estado_civil !== undefined && !Boolean(persona_db.apellido_casada))
                    refSlcEstadoCivil.current.value = parseInt(persona_db.estado_civil)
                //window.jQuery("select[name='persona[estado_civil]']").val(parseInt(persona_db.estado_civil) ).trigger('change');
            }

            if (persona.datos_juridicos !== null && persona.datos_juridicos !== undefined) {
                window.jQuery("input[name='persona_juridica[nit]']").val(persona.datos_juridicos.nit);
                window.jQuery("input[name='persona_juridica[razon_social]']").val(persona.datos_juridicos.razon_social);

                //self.setState({ startDateJuridico: new Date(persona.datos_juridicos.fecha_constitucion) })
            }
        } else {
            refInputNombre.current.value = persona.nombre
            refInputApPaterno.current.value = persona.apellido_paterno
            refInputApMaterno.current.value = persona.apellido_materno

            //document.getElementsByName("persona[nombre]")[0].value = persona.nombres;
            //document.getElementsByName("persona[apellido_paterno]")[0].value = persona.ap_paterno;
            //document.getElementsByName("persona[apellido_materno]")[0].value = persona.ap_materno;

            /*if (Boolean(persona.ap_casada)) {
                self.setState({ showApellidoCasada: true, ciExpedido: persona.lugar_ci });
                window.jQuery("select[name='persona[estado_civil]']").val(Constant[0].estado_civil.casado).trigger('change');
                document.getElementsByName("persona[apellido_casada]")[0].value = persona.ap_casada;
            }else
                self.setState({ ciExpedido: persona.lugar_ci, startDatePersona: null, showApellidoCasada: false });
                */
        }
    }

    const loadDataPerson = (persona, domicilio) => {
        if (persona !== undefined && domicilio !== undefined) {
            //let persona = prescripcionDb.Persona
            //let domicilio = prescripcionDb.Domicilio
            refInputNombre.current.value = persona.nombre
            refInputApPaterno.current.value = persona.apellido_paterno
            refInputApMaterno.current.value = persona.apellido_materno

            setStartDatePersona(new Date(persona.fecha_nacimiento))
            setCiExpedido(persona.expedido_en)

            if (Boolean(persona.apellido_casada)) {
                debugger
                if (refInputApCasada.current !== undefined)
                    refInputApCasada.current.value = persona.apellido_casada
                else
                    setShowApellidoCasada(true)
            }

            refInputTelefono.current.value = domicilio.telefono
            refInputCelular.current.value = domicilio.celular

            if (domicilio.email !== "") {
                refInputEmail.current.value = domicilio.email
                refCheckBoxEmail.current.checked = true

                var event = new Event('onchange', {
                    bubbles: true,
                    cancelable: true,
                });
                refCheckBoxEmail.current.dispatchEvent(event);
                handleEmailOnchange(event)
            }
        }
    }

    const handleEmailOnchange = (event) => {
        if (event.target.checked) {
            refInputEmail.current.readOnly = false
            refInputEmail.current.setAttribute('data-parsley-required', true)
        } else {
            refInputEmail.current.readOnly = true
            refInputEmail.current.value = ''
            refInputEmail.current.setAttribute('data-parsley-required', false)
        }
    }

    if (parseInt(props.derecho_admision) === parseInt(Constant[0].derecho_admision.natural)) {
        titulo_navigation = Languaje.prescripcion_contribuyente_natural
    }

    if (parseInt(props.derecho_admision) === parseInt(Constant[0].derecho_admision.juridico)) {
        titulo_navigation = Languaje.prescripcion_contribuyente_juridica
    }

    if (parseInt(props.derecho_admision) === parseInt(Constant[0].derecho_admision.inmuebles)) {
        titulo_navigation = Languaje.prescripcion_inmuebles
    }

    if (parseInt(props.derecho_admision) === parseInt(Constant[0].derecho_admision.actividad_economica)) {
        titulo_navigation = Languaje.prescripcion_persona_natural
    }

    if (parseInt(props.derecho_admision) === parseInt(Constant[0].derecho_admision.vehiculos)) {
        titulo_navigation = Languaje.prescripcion_persona_natural
    }

    return (
        <>
            {props.codeform === 'perJur' ?
                !props.natural ?   //solicitante
                    <FormRepresentanteLegal nameForm={"pres_persona_juridica"} toast={props.toast} natural={false}
                        personaJuridica={personaJuridica} codeform={props.codeform} />
                    : ""
                : ""}

            {props.codeform === 'perNatRLJ' ?
                !props.natural ?   //representante legal
                    <FormRepresentanteLegal nameForm={"pres_persona_juridica_r"} toast={props.toast} natural={false}
                        personaJuridica={personaJuridica} codeform={props.codeform} />
                    : ""
                : ""}

            <div className="row">
                <div className="col-sm-12 col-md-4 col-lg-4 form-group">
                    <label htmlFor={props.nameForm + '[id_tipo_documento]'} >Tipo Documento *</label>
                    <select className="form-control" name={props.nameForm + '[id_tipo_documento]'}
                        onChange={handleTipoDocumentoOnChange} data-parsley-required="true" required ref={refSlcTipoDocumento} codeform={props.codeform}>
                        <option defaultValue value="">Seleccione Tipo Documento</option>
                        {optionsTipoDocumento}
                    </select>
                </div>


                <div className="col-sm-12 col-md-4 col-lg-4 ">
                    <label htmlFor={props.nameForm + '[numero_documento]'}>Número de Documento *</label>
                    <div className="input-group">
                        <input type="text" className="form-control input-uppercase"
                            name={props.nameForm + '[numero_documento]'} placeholder="Número Documento"
                            data-parsley-required="true" required pattern="[a-zA-Z0-9-]+" data-parsley-pattern="[a-zA-Z0-9-]+" ref={refInputNumeroDoc} codeform={props.codeform} />
                        <div className="input-group-append">
                            <button type="button" className="btn btn-outline-secondary">EX</button>
                            <input type="hidden" name={props.nameForm + '[expedido_en]'} ref={refInputTipoExpedido} />
                            <button type="button" className="btn btn-outline-secondary dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span className="sr-only">Toggle Dropdown</span>
                            </button>
                            <div className="dropdown-menu " id={props.nameForm + '-dropdown-solicitante-tipo-doc-' + props.codeform} ref={refDropDownExpedido}>
                                <a className="dropdown-item" href="#" code="CB">Cochabamba</a>
                                <a className="dropdown-item" href="#" code="LP">La Paz</a>
                                <a className="dropdown-item" href="#" code="SC">Santa Cruz</a>
                                <a className="dropdown-item" href="#" code="OR">Oruro</a>
                                <a className="dropdown-item" href="#" code="CH">Chuquisaca</a>
                                <a className="dropdown-item" href="#" code="PO">Potosi</a>
                                <a className="dropdown-item" href="#" code="BN">Beni</a>
                                <a className="dropdown-item" href="#" code="PD">Pando</a>
                                <a className="dropdown-item" href="#" code="TJ">Tarija</a>
                            </div>
                            <button className="btn btn-outline-secondary btn-dark" type="button" data-toggle="tooltip" data-placement="top"
                                title="Buscar por CI" onClick={handleSearchByCiOnClick}><i className="fa fa-search" aria-hidden="true" ></i></button>
                        </div>
                    </div>
                </div>



                <div className="col-sm-12 col-md-4 col-lg-4 form-group">
                    <label htmlFor={props.nameForm + '[nacionalidad]'}>Nacionalidad *</label>
                    <select className="form-control" name={props.nameForm + '[nacionalidad]'} required data-parsley-required="true"
                        ref={refSlcNacionalidad} codeform={props.codeform}>
                        <option defaultValue value="">Seleccione Nacionalidad</option>
                        {optionsNacionalidad}
                    </select>
                </div>

            </div>


            <div className="row">
                <div className="col-sm-12 col-md-4 col-lg-4 form-group">
                    <label htmlFor={props.nameForm + '[nombre]'}>Nombre *</label>
                    <input name={props.nameForm + '[nombre]'} type="text" className="form-control input-uppercase" placeholder="Nombre"
                        required data-parsley-required="true" pattern="[a-zA-Z À-ÿ\u00f1\u00d1]+" data-parsley-pattern="[a-zA-Z À-ÿ\u00f1\u00d1]+"
                        ref={refInputNombre} codeform={props.codeform} />
                </div>
                <div className="col-sm-12 col-md-4 col-lg-4 form-group">
                    <label htmlFor={props.nameForm + '[apellido_paterno]'} >Apellido Paterno *</label>
                    <input name={props.nameForm + '[apellido_paterno]'} type="text" className="form-control input-uppercase" placeholder="Apellido Paterno"
                        required data-parsley-required="true" pattern="[a-zA-Z À-ÿ\u00f1\u00d1]+" data-parsley-pattern="[a-zA-Z À-ÿ\u00f1\u00d1]+"
                        ref={refInputApPaterno} codeform={props.codeform} />
                </div>
                <div className="col-sm-12 col-md-4 col-lg-4 form-group">
                    <label htmlFor={props.nameForm + '[apellido_materno]'}>Apellido Materno:</label>
                    <input name={props.nameForm + '[apellido_materno]'} type="text" className="form-control input-uppercase" placeholder="Apellido Materno"
                        pattern="[a-zA-Z À-ÿ\u00f1\u00d1]+" data-parsley-pattern="[a-zA-Z À-ÿ\u00f1\u00d1]+" ref={refInputApMaterno} codeform={props.codeform} />
                </div>
            </div>



            <div className="row">
                <div className="col-sm-12 col-md-4 col-lg-4 form-group">
                    <label htmlFor={props.nameForm + '[estado_civil]'}>Estado Civil *</label>
                    <select className="form-control" name={props.nameForm + '[estado_civil]'} required
                        onChange={handleEstadoCivilOnChange} ref={refSlcEstadoCivil} codeform={props.codeform}>
                        <option defaultValue value="">Estado Civil</option>
                        {optionsEstadoCivil}
                    </select>
                </div>
                {showApellidoCasada ?
                    <div className="col-sm-12 col-md-4 col-lg-4 form-group ">
                        <label htmlFor={props.nameForm + '[apellido_casada]'} >Apellido Casada:</label>
                        <input name={props.nameForm + '[apellido_casada]'} type="text" className="form-control input-uppercase" placeholder="Apellido Casada"
                            pattern="[a-zA-Z À-ÿ\u00f1\u00d1]+" data-parsley-pattern="[a-zA-Z À-ÿ\u00f1\u00d1]+" codeform={props.codeform} ref={refInputApCasada} />
                    </div>
                    : ""}

                <div className="col-sm-12 col-md-4 col-lg-4 form-group">
                    <label htmlFor={props.nameForm + '[genero]'} >Género *</label>
                    <select className="form-control" name={props.nameForm + '[genero]'} required data-parsley-required="true" codeform={props.codeform} ref={refSlcGenero}>
                        <option defaultValue value="">Género</option>
                        {optionsGenero}
                    </select>
                </div>
            </div>



            <div className="row">
                <div className="col-sm-12 col-md-4 col-lg-4 form-group">
                    <label htmlFor={props.nameForm + '[fecha_nacimiento]'} >Fecha de Nacimiento ( > 18, DD/MM/YYYY) *</label>
                    <DatePicker
                        locale="es"
                        dateFormat={Config[4].format}
                        selected={startDatePersona}
                        onChange={handleDatePickerPersonaChange}
                        maxDate={Config[1].anio}
                        className="form-control"
                        name={props.nameForm + '[fecha_nacimiento]'}
                        ref={refInputFechaNacimiento}
                        data-parsley-required="true"
                        showYearDropdown
                        showMonthDropdown
                        dropdownMode="select"
                        required />
                </div>

                <div className="col-sm-12 col-md-4 col-lg-4 form-group ">
                    <label htmlFor={props.nameFormDomicilio + '[telefono]'} >Teléfono fijo:</label>
                    <input name={props.nameFormDomicilio + '[telefono]'} type="text" className="form-control input-uppercase" placeholder="Teléfono fijo"
                        data-parsley-required="false" data-parsley-minlength="7" minLength="7" data-parsley-maxlength="7" maxLength="7" data-parsley-pattern="[0-9]+"
                        codeform={props.codeform} ref={refInputTelefono} />
                </div>

                <div className="col-sm-12 col-md-4 col-lg-4 form-group ">
                    <label htmlFor={props.nameFormDomicilio + '[celular]'} >Teléfono móvil:</label>
                    <input name={props.nameFormDomicilio + '[celular]'} type="text" className="form-control input-uppercase" placeholder="Teléfono Móvil"
                        data-parsley-required="true" required data-parsley-minlength="8" data-parsley-maxlength="8" minLength="8" maxLength="8" data-parsley-pattern="[0-9]+"
                        required codeform={props.codeform} ref={refInputCelular} />
                </div>
            </div>



            <div className="row">
                <div className="col-sm-12 col-md-4 col-lg-4 form-group">
                    <label htmlFor={props.nameFormDomicilio + '[email]'}>Correo Electrónico</label>
                    <div className="input-group mb-3">
                        <div className="input-group-prepend">
                            <div className="input-group-text">
                                <input type="checkbox" id="checkMail" onChange={handleEmailOnchange} ref={refCheckBoxEmail} />
                            </div>
                        </div>
                        <input type="text" className="form-control input-uppercase" name={props.nameFormDomicilio + '[email]'}
                            placeholder="Correo de notificación" data-parsley-required="false"
                            pattern="/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/"
                            data-parsley-pattern="/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/"
                            readOnly ref={refInputEmail} />
                    </div>
                </div>
            </div>

        </>
    )
}

export default FormPersona;