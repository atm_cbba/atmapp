import React, { useEffect, useState, useContext } from 'react';
import PasosNavigationPrescripcion from '../../components/utils/PasosNavigationPrescripcion';
import Constant from '../../data/constant';
import FormPersona from './FormPersona';
import { FormPersonaContext } from '../utils/FormPersonaContext'

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Fetch from '../../components/utils/Fetch';
import Texto from '../../data/es';

//let titulo_navigation = "";
const FormSolicitante = (props) => {

    const { derecho_admision, prescripcionDb } = props
    const [showCheckRepresentanteLegal, setShowCheckRepresentanteLegal] = useState(false)
    const [showDatosRepresentanteLegal, setShowDatosRepresentanteLegal] = useState(false)
    const [showDatosRepresentanteLegalJuridico, setShowDatosRepresentanteLegalJuridico] = useState(false)

    useEffect(() => {
        window.jQuery(function () { window.jQuery('.toogle-input').bootstrapToggle() });  //input[type=checkbox]
        eventCheckBox();
        if (prescripcionDb !== undefined) {
            if (prescripcionDb.PersonaRL !== null || prescripcionDb.PresPersonaJuridicaRL !== null) {
                window.jQuery("#CheckRepresentanteLegal").bootstrapToggle('on')
                setShowCheckRepresentanteLegal(true)
            }

            if (prescripcionDb.PersonaRL !== null)
                window.jQuery("#checkRepresentanteLegalCi").bootstrapToggle('on')

            if (prescripcionDb.PresPersonaJuridicaRL !== null)
                window.jQuery("#checkRepresentanteLegalNit").bootstrapToggle('on')
        }
    }, [showCheckRepresentanteLegal, showDatosRepresentanteLegal, showDatosRepresentanteLegalJuridico]);

    const eventCheckBox = () => {
        let checkRepresentanteLegal = document.getElementById("CheckRepresentanteLegal")
        if (checkRepresentanteLegal !== null) {
            let events = window.jQuery._data(checkRepresentanteLegal, "events");
            if (events === undefined || events.change.length === 0) {
                window.jQuery("#CheckRepresentanteLegal").change(function (event) {
                    if (window.jQuery(event.target).prop('checked') === true) {
                        setShowCheckRepresentanteLegal(true)
                    } else {
                        window.jQuery("#checkRepresentanteLegalCi").bootstrapToggle('off')
                        window.jQuery("#checkRepresentanteLegalNit").bootstrapToggle('off')
                        setShowCheckRepresentanteLegal(false)
                    }
                })
            }
        }

        if (document.getElementById("checkRepresentanteLegalCi") !== null) {
            let events_check_ci = window.jQuery._data(document.getElementById("checkRepresentanteLegalCi"), "events");
            if (events_check_ci === undefined || events_check_ci.change.length === 0) {
                window.jQuery("#checkRepresentanteLegalCi").change(function (event) {
                    if (window.jQuery(event.target).prop('checked') === true) {
                        window.jQuery("#checkRepresentanteLegalNit").bootstrapToggle('off')
                        setShowDatosRepresentanteLegal(true)
                        setShowDatosRepresentanteLegalJuridico(false)
                    } else {
                        setShowDatosRepresentanteLegal(false)
                    }
                })
            }
        }

        if (document.getElementById("checkRepresentanteLegalNit") !== null) {
            let events_check_nit = window.jQuery._data(document.getElementById("checkRepresentanteLegalNit"), "events");
            if (events_check_nit === undefined || events_check_nit.change.length === 0) {
                window.jQuery("#checkRepresentanteLegalNit").change(function (event) {
                    if (window.jQuery(event.target).prop('checked') === true) {
                        window.jQuery("#checkRepresentanteLegalCi").bootstrapToggle('off')
                        setShowDatosRepresentanteLegalJuridico(true)
                        setShowDatosRepresentanteLegal(true)
                    } else {
                        setShowDatosRepresentanteLegalJuridico(false)
                        setShowDatosRepresentanteLegal(false)
                    }
                })
            }
        }
    }
    
    /*
        const onSubmitForm = (event) => {
    
            props.onSubmitForm(event)
    
            //numero del documento
            var ul_error_ci = window.jQuery("input[name='persona[numero_documento]'][codeForm='perNat']").parent().parent().find('ul');
            ul_error_ci.remove();
            window.jQuery("input[name='persona[numero_documento]'][codeForm='perNat']").parent('div').parent('div').append(ul_error_ci);
    
            //numero de nit
            if (window.jQuery("input[name='persona_juridica[nit]'][codeForm='perNat']").length > 0) {
                var ul_error_nit = window.jQuery("input[name='persona_juridica[nit]'][codeForm='perNat']").parent().parent().find('ul');
                ul_error_nit.remove();
                window.jQuery("input[name='persona_juridica[nit]'][codeForm='perNat']").parent('div').parent('div').append(ul_error_nit);
            }
    
            //representante legal
            if (window.jQuery("input[name='persona[numero_documento]'][codeForm='perNatRLJ']").length > 0) {
                var ul_error_ci_rl = window.jQuery("input[name='persona[numero_documento]'][codeForm='perNatRLJ']").parent().parent().find('ul');
                ul_error_ci_rl.remove();
                window.jQuery("input[name='persona[numero_documento]'][codeForm='perNatRLJ']").parent('div').parent('div').append(ul_error_ci_rl);
            }
    
            if (window.jQuery("input[name='persona_juridica[nit]'][codeForm='perNatRLJ']").length > 0) {
                var ul_error_nit_rl = window.jQuery("input[name='persona_juridica[nit]'][codeForm='perNatRLJ']").parent().parent().find('ul');
                ul_error_nit_rl.remove();
                window.jQuery("input[name='persona_juridica[nit]'][codeForm='perNatRLJ']").parent('div').parent('div').append(ul_error_nit_rl);
            }
        }
    */
    let codeform = "perNat"
    let codeFormJuridico = "perJur"
    return (
        <div className="row">
            <PasosNavigationPrescripcion titulo_paso1={props.titleNavegacion} title_paso2={props.titleNavegacion2}
                paso1_active={true} paso2_active={false} paso3_active={false} paso4_active={false} paso5_active={false} />

            <form action="" className="contact__form needs-validation" name="formPresSolicitante" id="formPresSolicitante"
                method="post" noValidate onSubmit={(e) => props.onSubmitForm(e, props)} style={{ width: '100%' }}>

                {parseInt(derecho_admision) === Constant[0].derecho_admision.juridico ?
                    <div className="row">
                        <div className="col-12  form-group">
                            <h5 className="color-gris">{Texto.persona_juridica}</h5>
                        </div>
                    </div>
                    : ""
                }

                {parseInt(derecho_admision) === Constant[0].derecho_admision.natural ?
                    <div className="row">
                        <div className="col-12  form-group">
                            <h5 className="color-gris">{Texto.datos_contribuyente}</h5>
                        </div>
                    </div>
                    : ""
                }

                {/* contribuyente natural */}
                {parseInt(derecho_admision) === Constant[0].derecho_admision.natural ?
                    <FormPersona nameForm={"persona"} nameFormDomicilio={"domicilio"} toast={props.toast} natural={true}
                        persona={prescripcionDb !== undefined ? prescripcionDb.Persona : undefined}
                        personaJuridica={undefined}
                        domicilio={prescripcionDb !== undefined ? prescripcionDb.Domicilio : undefined} codeform={codeform} />
                    :
                    <FormPersona nameForm={"persona"} nameFormDomicilio={"domicilio"} toast={props.toast} natural={false}
                        persona={prescripcionDb !== undefined ? prescripcionDb.Persona : undefined}
                        personaJuridica={prescripcionDb !== undefined ? prescripcionDb.PresPersonaJuridica : undefined}
                        domicilio={prescripcionDb !== undefined ? prescripcionDb.Domicilio : undefined} codeform={codeFormJuridico} />
                }

                {parseInt(derecho_admision) === Constant[0].derecho_admision.natural || parseInt(derecho_admision) === Constant[0].derecho_admision.juridico ?
                    <div className="row">
                        <div className="col-12 ">
                            <label htmlFor="" style={{ display: 'block' }} >Representante Legal </label>
                        </div>

                        <div className="col-6 col-sm-6 col-md-4 col-lg-4 form-group">
                            <label htmlFor="" className="pr-2">Tiene </label>
                            <input data-toggle="toggle" data-onstyle="secondary" className="toogle-input"
                                type="checkbox" id="CheckRepresentanteLegal" data-on="Si" data-off="No" />
                        </div>

                        {showCheckRepresentanteLegal ?
                            <div className="col-6 col-sm-6 col-md-4 col-lg-4 form-group">
                                <label htmlFor="" className="pr-2" >Buscar por C.I.</label>
                                <input data-toggle="toggle" data-onstyle="secondary" type="checkbox" className="toogle-input "
                                    id="checkRepresentanteLegalCi" name="checkRepresentanteLegalCi" data-on="Si" data-off="No" />
                            </div>
                            : ""}

                        {showCheckRepresentanteLegal ?
                            <div className="col-6 col-sm-6 col-md-4 col-lg-4 form-group">
                                <label htmlFor={''} className="pr-2">Buscar por  N.I.T </label>
                                <input data-toggle="toggle" data-onstyle="secondary" type="checkbox" className="toogle-input "
                                    id='checkRepresentanteLegalNit' name="checkRepresentanteLegalNit" data-on="Si" data-off="No" />
                            </div>
                            : ""}
                    </div>
                    : ""
                }

                {showDatosRepresentanteLegal ?
                    <div className="row">
                        <div className="col-12  form-group">
                            <h5 className="color-gris">{Texto.datos_representante_legal}</h5>
                        </div>
                    </div>
                    : ""
                }

                {showDatosRepresentanteLegal ?
                    <FormPersonaContext.Provider value={"hola mundo"}>
                        <FormPersona nameForm={"pres_representante_legal"} nameFormDomicilio={"domicilio_representante_legal"} toast={props.toast}
                            natural={!showDatosRepresentanteLegalJuridico}
                            persona={prescripcionDb !== undefined ? prescripcionDb.PersonaRL : undefined}
                            personaJuridica={prescripcionDb !== undefined ? prescripcionDb.PresPersonaJuridicaRL : undefined}
                            domicilio={prescripcionDb !== undefined ? prescripcionDb.DomicilioRL : undefined}
                            codeform={'perNatRLJ'} />
                    </FormPersonaContext.Provider>
                    : ""
                }

                <div className="row">
                    <div className="col-sm-12 col-md-12 col-lg-12 form-group">
                        <input name="Siguiente" type="submit" className="button-style pull-right" value="Siguiente" />
                    </div>
                </div>
            </form>
        </div>
    )
}

export default FormSolicitante;