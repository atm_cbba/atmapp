import React, {  } from 'react';

const TableDeudas = (props) => {

    const { data, nameFormObjectoGestion } = props
    
    const handleClickRow = (event, checked) => {
        event.preventDefault()
        if(!checked){
            let image = event.target.closest(".row-table-custom").getElementsByTagName("i")
            let prescribe = image[0].nextElementSibling.value.split(":::")
            let node_i = event.target.closest('.row').getElementsByTagName('i')[0]
            if (node_i.classList.contains('d-none')) {
                node_i.classList.remove('d-none')
                image[0].nextElementSibling.value = prescribe[0]+":::"+prescribe[1]+":::"+prescribe[2]+":::"+prescribe[3]+":::"+"1"
            } else {
                node_i.classList.add('d-none')
                image[0].nextElementSibling.value = prescribe[0]+":::"+prescribe[1]+":::"+prescribe[2]+":::"+prescribe[3]+":::"+"0"
            }
        }
    }

    const handleClickShowObservation = (event, message) => {
        event.preventDefault()
        window.alertBootBox(message)
    }

    const rowTable = (sub_cont, gestion, name, monto, message, classCss, checked) => {
        return <div className={"row pointer row-table-custom ml-1 pt-3 objeto-tributario-deuda-" + gestion + " " + (classCss ? classCss : "")} 
        key={sub_cont + '_' + gestion} onClick={(e) => handleClickRow(e, checked)} >
            <div className="col-3 col-md-1 ">
                <p>{gestion}</p>
            </div>
            <div className="col-4 col-md-6 d-none d-sm-block" >
                {name}
            </div>
            <div className="col-3 col-md-2 " >
                {monto}
            </div>
            <div className="col-3 col-md-1">
                <i className={`fa fa-check fa-2x fa-icon-image ${checked ? "" : "d-none"}` } aria-hidden="true"></i>
                <input type="hidden" name={nameFormObjectoGestion + "[][objeto_prescribir]"} value={`${gestion}:::${name}:::${monto !== undefined ? monto : "0"}:::${message !== undefined ? message : " "}:::0`} />
            </div>
            <div className="col-3 col-md-2 text-center">
                {message !== undefined ?
                    <i className="fa fa-info-circle " aria-hidden="true" style={{ fontSize: '1.7em' }} onClick={(e) => handleClickShowObservation(e, message)}></i>
                    : ""}
            </div>
        </div>
    }

    let sub_cont = 0
    const table = data.map(({ gestion, concepto, monto, message, classCss, checked }) => {
        if (!concepto.includes('MULTA')) {
            sub_cont++
            return rowTable(sub_cont, gestion, concepto, monto, message, classCss, checked)
        }
    })

    return (
        <>
            <div className="row  " >
                <div className="col-3 col-md-1 pt-3 text-center">
                    <p className="font-weight-bold"><strong>Gestión</strong></p>
                </div>
                <div className="col-4 col-md-6 pt-3 d-none d-sm-block" style={{ justifyContent: 'left', display: 'flex' }}>
                    <p className="font-weight-bold text-center" ><strong>Concepto</strong></p>
                </div>
                <div className="col-3 col-md-2 pt-3" style={{ justifyContent: 'left', display: 'flex' }}>
                    <p className="font-weight-bold text-center" ><strong>Deuda</strong></p>
                </div>
                <div className="col-3 col-md-1 pt-3" style={{ justifyContent: 'left', display: 'flex' }}>
                    <p className="font-weight-bold text-center" ><strong>Prescribir</strong></p>
                </div>
                <div className="col-3 col-md-2 pt-3">
                    <p className="font-weight-bold text-center" ><strong>Acciones</strong></p>
                </div>
            </div>
            {table !== undefined ? table : ""}
        </>
    )
}

export default TableDeudas; 