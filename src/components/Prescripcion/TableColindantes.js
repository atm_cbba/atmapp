import React, { } from 'react';

const TableDeudas = (props) => {

    const { data } = props

    const rowTable = (sub_cont, descripcionColindancia, orientacion, id) => {
        return <div className={"row pointer row-table-custom ml-1 pt-3 "} key={sub_cont + '_' + orientacion} >
            <div className="col-5 col-md-5 ">
                <p>{orientacion}</p>
            </div>
            <div className="col-7 col-md-7 " >
                {descripcionColindancia}
            </div>
        </div>
    }

    let sub_cont = 0

    const table = data.map(({ descripcionColindancia, orientacion }) => {
        sub_cont++
        return rowTable(sub_cont, descripcionColindancia, orientacion)
    })

    return (
        <div className="" style={{ marginLeft: -15 }}>
            <div className="row " >
                <div className="col-5 col-md-5 pt-3 text-left">
                    <p className="font-weight-bold"><strong>Orientación</strong></p>
                </div>
                <div className="col-7 col-md-7 pt-3" >
                    <p className="font-weight-bold text-left" ><strong>Descripcion</strong></p>
                </div>
            </div>

            {table !== undefined ? table : ""}
        </div>
    )
}

export default TableDeudas; 