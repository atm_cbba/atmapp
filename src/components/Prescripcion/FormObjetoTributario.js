import React, { Component, useEffect, useState, useRef, useCallback, useMemo } from 'react';
//import { Link } from 'react-router-dom';
//import MapOpenLayer from '../component/MapOpenLayer'
///import MapCatastroPublic from '../component/MapCatastroPublic'
//import Links from '../../data/link';

import DataPropietario from './DataPropietario';
import DataTecnicos from './DataTecnicos';
import DataLiquidacion from './DataLiquidacion';

//import AsyncSelect from 'react-select/async';
import Fetch from '../../components/utils/Fetch';
import AuthService from '../../components/Usuario/AuthService';
import Config from '../../data/config';

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

let titulo_navigation = "";
let solicitanteDb = undefined
let optionsSelect = [];

const FormSolicitante = (props) => {

    const { showBottomPreview } = props

    let optionsSelect = []

    const fetch = new Fetch();
    fetch.setToast(toast);
    const auth = new AuthService();

    const refSelectGestion = useRef()
    const refInputHiddenGestion = useRef()

    const [dataInmueble, setDataInmueble] = useState(undefined)
    const [dataTecnicosInmueble, setDataTecnicosInmueble] = useState(undefined)
    const [dataDeudaInmueble, setDataDeudaInmueble] = useState(undefined)
    const [dataInmuebleCatastro, setDataInmuebleCatastro] = useState(undefined)

    const [showButtonSendATM, setShowButtonSendATM] = useState(true);
    //const [showButtonPrint, setShowButtonPrint] = useState(false);

    useEffect(() => {
        debugger
        const dataInmueble = localStorage.getItem('datosInmueble');
        if (dataInmueble !== undefined) {
            let objectJson = window.base64ToObjectJson(dataInmueble)
            if (objectJson !== null && objectJson.inmueble !== null) {
                //setShowDataInmueble(true)
                setDataInmueble(objectJson)
                loadInmuebleCatastro(objectJson.codigoCatastral)
                //loadHistorialInmueble(objectJson)
            }
        }

        const dataTecnicos = localStorage.getItem('datosTecnicos');
        if (dataTecnicos !== undefined) {
            let objectJsonTecnicos = window.base64ToObjectJson(dataTecnicos)
            if (objectJsonTecnicos !== null) {
                setDataTecnicosInmueble(objectJsonTecnicos)
            }
        }

        const dataDeuda = localStorage.getItem('datosDeuda');
        if (dataDeuda !== undefined) {
            let objectJsonDeduda = window.base64ToObjectJson(dataDeuda)
            if (objectJsonDeduda !== null) {
                setDataDeudaInmueble(objectJsonDeduda)
            }
        }
    }, []);

    const loadInmuebleCatastro = (codigo_catrastral) => {

        const resInmuebleCatastro = fetch.axiosAsyncGet(`api-ruat/inmuebles-catastro/codigo-catastral/${codigo_catrastral}`);
        resInmuebleCatastro.then(inmueblCatastro => {
            if (inmueblCatastro !== null && inmueblCatastro.status === true) {
                if (inmueblCatastro.data.inmueble.length > 0) {

                    let objJsonB64 = window.objetJsonToBase64(inmueblCatastro.data.inmueble[0])
                    localStorage.setItem('inmuebleCatastro', objJsonB64)
                    setDataInmuebleCatastro(inmueblCatastro.data.inmueble[0])
                }
            }
        })
    }

    const handleClickSentATM = (event) => {
        event.preventDefault()
        const tokenPrescripcion = localStorage.getItem('tokenPrescripcion');
        const form = new FormData();
        form.append('pres_prescripcion[token]', tokenPrescripcion);
        fetch.fetchPost(form, 'api/prescripcion/complete', event.target).then(dataJson => {
            if (dataJson !== undefined && dataJson.status === true) {
                setShowButtonSendATM(false)
                toast.success(dataJson.message, {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true
                });
            }
        })
    }


    const handleClickPrintConstance = (event) => {
        event.preventDefault()
        const tokenPrescripcion = localStorage.getItem('tokenPrescripcion');
        window.location.href = Config[0].url + `report/prescripcion-download-approved-user/${tokenPrescripcion}/?auth=` + auth.getToken();
    }

    return (
        <div className="row">

            <form action="" className="contact__form needs-validation" name="formPresObjetoTributario" id="formPresObjetoTributario"
                method="post" noValidate onSubmit={props.onSubmitForm} style={{ width: '100%' }} >

                {dataInmueble ?
                    <DataPropietario dataInmueble={dataInmueble} />
                    : ""
                }

                {dataTecnicosInmueble !== undefined ?
                    <DataTecnicos dataInmueble={dataInmueble} dataTecnicosInmueble={dataTecnicosInmueble}
                        dataInmuebleCatastro={dataInmuebleCatastro} fetch={fetch} nameFormInmueble={"inmueble"} nameFormConstruccion={"construccion"}
                        nameFormConstruccionAdicional={"construccionAdicional"} nameFormDireccionInmueble={"inmueble_direccion"} />
                    : ""
                }

                {dataDeudaInmueble !== undefined ?
                    <DataLiquidacion dataDeudaInmueble={dataDeudaInmueble} />
                    : ""}

                <div className="row">
                    <div className="col-12 col-md-4 col-lg-4 form-group text-center">
                        {showBottomPreview ? <input type="submit" className="button-style  mb-3" value={'Vista Previa'} /> : ""}
                    </div>

                    <div className="col-12 col-md-4 col-lg-4 form-group text-center">
                        {!showBottomPreview && showButtonSendATM ?
                            <input type="button" className="button-style  mb-3" value={'Enviar ATM'} onClick={handleClickSentATM} />
                            : ""}
                    </div>

                    <div className="col-12 col-md-4 col-lg-4 form-group text-center">
                        {!showBottomPreview && !showButtonSendATM ?
                            <input type="button" className="button-style  mb-3" value={'Imprimir Constancia'} onClick={handleClickPrintConstance} />
                            : ""}
                    </div>
                </div>
            </form>
        </div>
    )
}
export default FormSolicitante