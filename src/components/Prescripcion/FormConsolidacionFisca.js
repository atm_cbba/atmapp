import React, { Component, useEffect, useState, useRef, useCallback } from 'react';
import Fetch from '../../components/utils/Fetch';
import AuthService from '../../components/Usuario/AuthService';

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import Links from '../../data/link';
import TitlePage from '../../components/utils/TitlePage';
import Texto from '../../data/es'


const FormConsolidacionFisca = (props) => {

    const fetch = new Fetch();
    fetch.setToast(toast);
    const auth = new AuthService();

    //const { prescripcion, solicitante } = props

    const [formGestion, setFormGestion] = useState(undefined)
    const [idPrescripcion, setIdPrescripcion] = useState(undefined)
    const [idObjetoTributarioGestion, setIdObjetoTributarioGestion] = useState(undefined)

    useEffect(() => {
        loadInmueble()
    }, []);

    const loadInmueble = async () => {
        const tokenPrescripcion = localStorage.getItem('tokenPrescripcion');
        const resDeudas = await fetch.fetchGet(`api/prescripcion/objetos-tributarios-gestion/token-prescripcion/${tokenPrescripcion}`)
        if (Boolean(resDeudas) && resDeudas.status === true) {

            const tempGestion = resDeudas.Data.ObjetoTributarioGestion.map((ObjetoTributarioGestion) => {
                return <div key={ObjetoTributarioGestion.id} className="border p-3 mb-3">
                    <div className="row">
                        <div className="col-12 col-md-2 col-lg-2 form-group">
                            <strong>Gestión: </strong><label>{ObjetoTributarioGestion.gestion.name}</label>
                            <input name="contactanos[][id]" type="hidden" className="form-control" value={ObjetoTributarioGestion.id} />
                        </div>

                        <div className="col-12 col-md-8 col-lg-8 form-group">
                            <strong>Concepto: </strong><label>{ObjetoTributarioGestion.concepto}</label>
                        </div>

                        <div className="col-12 col-md-2 col-lg-2 form-group text-right">
                            <strong>Deuda: </strong> <label>Bs. {ObjetoTributarioGestion.deuda}</label>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-12 form-check ml-3">
                            <input type="checkbox" className="form-check-input" id="contactanos[][determinacion_oficio]" name="contactanos[][determinacion_oficio]" />
                            <label className="form-check-label" htmlFor={"contactanos[][determinacion_oficio]"}>Determinación de Oficio</label>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-12 form-group mt-3 mb-0">
                            <label >Datos del proceso de Fiscalización</label>
                        </div>

                        <div className="col-12 col-md-4 col-lg-4 form-group">
                            <input name="contactanos[][numero_oficio]" type="text" className="form-control" placeholder="Número de Oficio" />
                        </div>

                        <div className="col-12 col-md-4 col-lg-4 form-group">
                            <input name="contactanos[][fecha_oficio]" type="text" className="form-control" placeholder="Fecha de Oficio" />
                        </div>

                        <div className="col-12 col-md-4 col-lg-4 form-group">
                            <input name="contactanos[][fecha_notificacion]" type="text" className="form-control" placeholder="Fecha de Notifición de Oficio" />
                        </div>
                    </div>
                </div>
            })
            setFormGestion(tempGestion)
            setIdPrescripcion(resDeudas.Data.Prescripcion.id)
            setIdObjetoTributarioGestion()
        }
    }

    const handleSubmitForm = async (event) => {
        event.preventDefault();

        const form = new FormData(event.target);
        form.append('pres_validacion_fiscalizacion[id_prescripcion]', idPrescripcion);

        await fetch.fetchPost(form, 'api/prescripcion/fiscalizacion/validacion/create', undefined).then(dataJson => {
            if (dataJson && dataJson.status === true && dataJson.data !== null) {
                toast.success(dataJson.message, {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true
                });
                window.redirect(Links[43].url);
            }else {
                toast.warn(dataJson.message, {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true
                });
            }
        })
    }

    const breadcrumbs = [
        {
            title: Links[0].title,
            url: Links[0].url
        },
        {
            title: Links[43].title,
            url: Links[43].url
        },
        {
            title: 'Validación Prescripción',
            url: '#'
        }
    ];

    return (
        <div id="contact" className="contact paddingTop" >

            <TitlePage titlePage={"VALIDACION DE LA PRESCRIPCION"} breadcrumbs={breadcrumbs} position={'left'} />

            <div className="container" style={{ marginTop: '-50px' }}>
                <div className="row">
                    <form action="" className="contact__form needs-validation" name="formPresConsolidacionFisca" id="formPresConsolidacionFisca"
                        method="post" noValidate onSubmit={handleSubmitForm} style={{ width: '100%' }} >

                        {formGestion}

                        <div className="row">
                            <div className="col-12 form-group">
                                <textarea name="pres_validacion_fiscalizacion[observacion]" className="form-control" rows="4" placeholder="Observación" required ></textarea>
                            </div>
                        </div>

                        <div className="row">
                            <div className="col-12">
                                <input name="submit" type="submit" className="button-style pull-right" value="Enviar A Legal Tributario" />
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <ToastContainer enableMultiContainer containerId={'Z'}
                    position="top-right"
                    autoClose={5000}
                    hideProgressBar={false}
                    newestOnTop={false}
                    closeOnClick
                    rtl={false}
                    pauseOnVisibilityChange
                    draggable
                    pauseOnHover
                />
                <ToastContainer />
        </div>
    )
}
export default FormConsolidacionFisca