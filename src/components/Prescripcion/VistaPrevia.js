import React, { Component, useEffect, useState, useRef, useCallback } from 'react';
import Iframe from 'react-iframe'
import PasosNavigationPrescripcion from '../../components/utils/PasosNavigationPrescripcion';
import Fetch from '../../components/utils/Fetch';
import AuthService from '../../components/Usuario/AuthService';

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


import Constant from '../../data/constant';
import Texto from '../../data/es';
import Languaje from '../../data/es';
import Links from '../../data/link';
import { Link } from 'react-router-dom';
import Config from '../../data/config';


const VistaPrevia = (props) => {

    const fetch = new Fetch();
    fetch.setToast(toast);
    const auth = new AuthService();

    const { prescripcion, solicitante } = props

    //const [declaracionJurada, setDeclaracionJurada] = useState(undefined)
    const [showButtons, setShowButtons] = useState(true)

    useEffect(() => {
        downloadFilePrescripcion()
    }, []);

    const downloadFilePrescripcion = () => {
        window.jQuery.preloader.start();
        window.scrollTo(0, 0);
        debugger
        if (Boolean(prescripcion)) {
            completePrescripcion(prescripcion.token)
        } else {
            //para nuevos registros
            //if (Boolean(this.props.declaracionJurada)) {

            fetch.fetchGet(`api/prescripcion/by-token/${props.prescripcion.token}`).then(dataJson => {
                if (dataJson !== undefined && dataJson.status === true) {

                    completePrescripcion(dataJson.data.Prescripcion.token)

                    /*if (dataJson.data.DeclaracionJurada !== null) {
                        _declaracionJurada = dataJson.data.DeclaracionJurada
                    }
                    if (dataJson.data.Solicitante !== null) {
                        _solicitante = dataJson.data.Solicitante
                    }
                    completePrescripcion(self, dataJson.data.DeclaracionJurada.token)

                    self.fetch.toast.success(dataJson.message, {
                        position: "top-right",
                        autoClose: 5000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true
                    });*/
                }
            })
            //}
        }
    }

    const completePrescripcion = (token_pres) => {
        const form = new FormData();
        form.append('pres_prescripcion[token]', token_pres);

        fetch.fetchPost(form, 'api/prescripcion/complete').then(dataJson => {
            if (dataJson !== undefined && dataJson.status === true) {
                debugger
                if (Boolean(dataJson.Prescripcion)) {

                    downloadPdf(`${Config[0].url}report/prescripcion-online/${token_pres}/?auth=${auth.getToken()}`);

                    if (dataJson.Estado !== null) {
                        if (dataJson.Estado.code === Constant[0].estado.aprobado) {
                            setShowButtons(false)
                        }
                    }
                    fetch.toast.success(dataJson.message, {
                        position: "top-right",
                        autoClose: 5000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true
                    });
                } else {
                    fetch.toast.warn(dataJson.message, {
                        position: "top-right",
                        autoClose: 5000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true
                    });
                }
            } else {
                setTimeout(() => {
                    window.jQuery.preloader.stop();
                }, 2000);
            }
        })
    }

    const downloadPdf = (url) => {
        document.getElementById('iframePreviewPresPdf').src = url
    }

    const hanldeDownloadPdf = (event) => {
        event.preventDefault()
        window.location.href = Config[0].url + `report/prescripcion-download/${prescripcion.token}/?auth=` + auth.getToken();
    }

    const handleOnladIFrame = (event) => {
        window.jQuery.preloader.stop();
    }

    const handleCancelClick = (event) => {
        event.preventDefault();
        window.deleteBootbox( Texto.numero_orden_perderan_datos.replace("%s", prescripcion.numero) , function (result) {
            if (result === true) {
                const delete_lic = fetch.fetchGet(`api/licencia-actividad-economica/delete/${prescripcion.token}`);
                delete_lic.then(res => {
                    if (res !== undefined && res.status === true) {
                        fetch.toast.success(res.message + ". " + Texto.espere_redireccionamos_pagina, {
                            position: "top-right",
                            autoClose: 5000,
                            hideProgressBar: false,
                            closeOnClick: true,
                            pauseOnHover: true,
                            draggable: true
                        });

                        window.redirect(Links[1].url);
                    }
                })
            }
        })
    }

    const handleEditClick = (event) => {
        event.preventDefault();
        debugger
        window.editPrescripcioMiniBootBox(Links[20].url, solicitante.contribuyente, prescripcion.token, prescripcion.numero)
    }

    const handleCheckClick = (event) => {
        event.preventDefault();

        window.confirmBootbox(prescripcion.numero, function (result) {
            if (result === true) {
                window.jQuery.preloader.start();
                const aprobar_lic = fetch.fetchGet(`api/declaracion-jurada/check/${prescripcion.token}`);
                aprobar_lic.then(res => {
                    debugger
                    /*if (res !== undefined && res.status === true) {

                        //downloadPdf(`${Config[0].url}report/licencia-actividad-economica-download/${res.DeclaracionJurada.token}/?auth=${auth.getToken()}`);
                        downloadPdf(`${Config[0].url}report/prescripcion-online/${token_pres}/?auth=${auth.getToken()}`);

                        if (res.Estado.code === Constant[0].estado.aprobado)
                            setShowButtons(false)

                        fetch.toast.success(res.message, {
                            position: "top-right",
                            autoClose: 5000,
                            hideProgressBar: false,
                            closeOnClick: true,
                            pauseOnHover: true,
                            draggable: true
                        });
                    }*/
                })
            }
        })
    }

    return (
        <div className="container" >
            <PasosNavigationPrescripcion titulo_paso1={props.titleNavegacion} titulo_paso2={props.titleNavegacion2}
                paso1_active={true} paso2_active={true} paso3_active={true} paso4_active={true} paso5_active={true} />

            <div className="row">
                <div className="col-sm-12 col-md-12 col-lg-12 embed-container">
                    <Iframe
                        src=""
                        width="640"
                        height="360"
                        frameBorder="0"
                        allow="allow-same-origin allow-scripts allow-popups allow-forms"
                        allowFullScreen
                        id="iframePreviewPresPdf"
                        display="initial"
                        onLoad={handleOnladIFrame}
                    >
                    </Iframe>
                </div>
            </div>

            <div className="row mt-3 mb-3">
                <div className="col-10 col-md-10 col-lg-10 paddingTop15">
                    <p className="text-left">En caso no se visualize el pdf, puedes
                        <Link to="#" onClick={hanldeDownloadPdf} title="Descargar" style={{ paddingLeft: '5px' }} >
                            hacer click aquí para descargar el archivo PDF.
                        </Link>
                    </p>
                </div>

                <div className="col-2 col-md-2 col-lg-2 paddingTop15 pull-right">
                    <Link to="#" onClick={hanldeDownloadPdf} title="Descargar" style={{ fontSize: '3em', float: 'right', marginTop: '-15px' }}  >
                        <i className="fa fa-print fa-icon-image" aria-hidden="true"></i>
                    </Link>
                </div>
            </div>

            {showButtons ?
                <div className="row">
                    <div className="col-sm-12 col-md-4 col-lg-4 form-group ">
                        <button type="button" className=" pull-left button-red" onClick={handleCancelClick} >{Texto.eliminar}</button>
                    </div>

                    <div className="col-sm-12 col-md-4 col-lg-4 form-group">
                        <input type="submit" className="button-style" value={Texto.editar_datos_prescripcion} onClick={handleEditClick} />
                    </div>

                    <div className="col-sm-12 col-md-4 col-lg-4 form-group ">
                        <input type="submit" className="button-style pull-right" value={Texto.estoy_de_acuerdo} onClick={handleCheckClick} />
                    </div>
                </div>
                : ""}
        </div>
    )
}
export default VistaPrevia