import React, { useEffect, useState, useRef } from 'react';
import PasosNavigationPrescripcion from '../utils/PasosNavigationPrescripcion';
import DataTable from 'react-data-table-component';
import styled from 'styled-components';  //styled for data table

import AsyncSelect from 'react-select/async';
import Fetch from '../utils/Fetch';
import AuthService from '../Usuario/AuthService';
import Texto from '../../data/es';
import Constant from '../../data/constant';

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const FormRequisito = (props) => {

    const { prescripcion, prescripcionRequisitoDb } = props

    const fetch = new Fetch();
    fetch.setToast(toast);
    const auth = new AuthService();

    const [requisitosNaturales, setRequisitosNaturales] = React.useState("");
    const [requisitosJuridicos, setRequisitosJuridicos] = React.useState("");
    const [requisitosRL, setRequisitosRL] = React.useState("");

    useEffect(() => {
        loadRequisitos()
    }, []);

    const handleClickRow = (event, id_requisito) => {
        event.preventDefault()
        let input = event.target.closest(".row-table-custom").getElementsByTagName("input")
        let node_i = event.target.closest('.row').getElementsByTagName('i')[0]
        if (node_i.classList.contains('d-none')) {
            node_i.classList.remove('d-none')
            input[0].value = id_requisito
        } else {
            node_i.classList.add('d-none')
            input[0].value = ''
        }
    }

    const loadRequisitos = () => {

        if (parseInt(prescripcion.contribuyente) === Constant[0].derecho_admision.natural) {
            const requisitos_natural = fetch.fetchGet(`api/prescripcion/requisito/by-contribuyente/1`);
            requisitos_natural.then(res => {
                if (res !== undefined && res.status === true && res.Requisito !== null) {  //is ok
                    let sub_cont = 0
                    let list_requisito = res.Requisito.map(({ id, name }) => {
                        sub_cont++
                        return rowTable(sub_cont, id, name)
                    })
                    setRequisitosNaturales(list_requisito)
                    loadDataDefault()
                }
            })
        }

        if (parseInt(prescripcion.contribuyente) === Constant[0].derecho_admision.juridico) {
            const requisitos_juridico = fetch.fetchGet(`api/prescripcion/requisito/by-contribuyente/2`);
            requisitos_juridico.then(res => {
                if (res !== undefined && res.status === true && res.Requisito !== null) {  //is ok
                    let sub_cont = 0
                    let list_requisito = res.Requisito.map(({ id, name }) => {
                        sub_cont++
                        return rowTable(sub_cont, id, name)
                    })
                    setRequisitosJuridicos(list_requisito)
                    loadDataDefault()
                }
            })
        }

        if (Boolean(prescripcion.personaRL) && parseInt(prescripcion.personaRL) > 0) {
            const requisitos_rl = fetch.fetchGet(`api/prescripcion/requisito/by-contribuyente/3`);
            requisitos_rl.then(res => {
                if (res !== undefined && res.status === true && res.Requisito !== null) {  //is ok
                    let sub_cont = 0
                    let list_requisito = res.Requisito.map(({ id, name }) => {
                        sub_cont++
                        return rowTable(sub_cont, id, name)
                    })
                    setRequisitosRL(list_requisito)
                    loadDataDefault()
                }
            })
        }
    }

    const loadDataDefault = async () => {
        let inputs = document.getElementsByName("pres_prescripcion_requisito[][id_requisito]")
        if (prescripcionRequisitoDb !== undefined) {
            prescripcionRequisitoDb.forEach(item => {
                inputs.forEach(input => {
                    let array_class = input.parentElement.parentElement.classList
                    let id = array_class[array_class.length - 1]
                    if (id === "requisito-" + item.id_requisito) {
                        input.value = item.id_requisito
                        input.parentElement.firstElementChild.classList.remove("d-none")
                    }
                })
            });
        }
    }

    const rowTable = (sub_cont, id, name) => {
        return <div className={"row pointer row-table-custom requisito-" + id} key={sub_cont + '_' + id} onClick={(e) => handleClickRow(e, id)} >
            <div className="col-12 col-md-9 ">
                <p>{name}</p>
            </div>
            <div className="col-6 col-md-1 " style={{ justifyContent: 'center', display: 'flex' }}>
                <i className="fa fa-check fa-2x fa-icon-image d-none pt-2" aria-hidden="true"></i>
                <input name={props.nameFormRequisito + "[][id_requisito]"} type="hidden" />
            </div>
            <div className="col-6 col-md-2 ">
                subir archivo
            </div>
        </div>
    }

    return (
        <div className="container">
            <PasosNavigationPrescripcion titulo_paso1={props.titleNavegacion} titulo_paso2={props.titleNavegacion2}
                paso1_active={true} paso2_active={true} paso3_active={true} paso4_active={true} paso5_active={false} />

            <form action="" className="contact__form needs-validation" name="formPresRequisito" id="formPresRequisito"
                method="post" noValidate onSubmit={(e) => props.onSubmitForm(e, props)} style={{ width: '90%' }}>

                <div className="row mb-2">
                    <div className="col-12 form-group">
                        {parseInt(prescripcion.contribuyente) === Constant[0].derecho_admision.natural ?
                            <h5 className="color-gris">{Texto.persona_natural}</h5>
                            : <h5 className="color-gris">{Texto.persona_juridica}</h5>}
                    </div>
                </div>

                <div className="row">
                    <div className="col-12 form-group">
                        {parseInt(prescripcion.contribuyente) === Constant[0].derecho_admision.natural ? requisitosNaturales : requisitosJuridicos}
                    </div>
                </div>

                {Boolean(prescripcion !== null ? prescripcion.personaRL : 1) && parseInt(prescripcion !== null ? prescripcion.personaRL : 1) > 0 ?
                    <div className="row mt-5 mb-2">
                        <div className="col-12 form-group">
                            <h5 className="color-gris">{Texto.terceros_responsables_solicitantes}</h5>
                        </div>
                    </div>
                    : ""}

                {Boolean(prescripcion !== null ? prescripcion.personaRL : 1) && parseInt(prescripcion !== null ? prescripcion.personaRL : 1) > 0 ?
                    <div className="row">
                        <div className="col-12 form-group">
                            {requisitosRL}
                        </div>
                    </div>
                    : ""}

                <div className="row">
                    <div className="col-sm-12 col-md-12 col-lg-12 form-group">
                        <input name={props.buttonName} type="submit" className="button-style pull-right mb-3" value={props.buttonName} />
                    </div>
                </div>
            </form>
        </div>
    )
}
export default FormRequisito