import React, { useEffect, useState } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import ModalFur from './../component/ModalFur';
import Form from '../Prescripcion/Form'

import Links from '../../data/link';
import TitlePage from '../../components/utils/TitlePage';
import Texto from '../../data/es'

const PrescripcionCreate = (props) => {
    const [initial_state, setInitialState] = useState(false);
    const [show_form, setShowForm] = useState(false);
    const [derecho_admision, setDerechoAdmision] = useState("");
    const [fur, setFur] = useState("")

    useEffect(() => {
        window.jQuery('#modalFurFull').modal('show');
        window.jQuery('#modalFurFull').on('hidden.bs.modal', function () {
            debugger
            let fur_cookie = window.leerCookie('fur');
            setDerechoAdmision(window.leerCookie('dapres'))
            setFur(fur_cookie)
            //setTimeout(function () { window.jQuery('[data-toggle="tooltip"]').tooltip(); }, 3000);
            setShowForm(true)
        });
    }, [initial_state],);

    const breadcrumbs = [
        {
            title: Links[0].title,
            url: Links[0].url
        },
        {
            title: Links[3].title,
            url: Links[3].url
        },
        {
            title: 'Nuevo',
            url: '#'
        }
    ];

    return (
        <div id="contact" className="contact paddingTop" >
            {/* Breadcrumb Area Start */}
            <TitlePage titlePage={Texto.prescripcion} breadcrumbs={breadcrumbs} position={'left'} />
            {/* Breadcrumb Area End */}

            <div className="container" style={{ marginTop: '-50px'}}>
                {show_form ? <Form  toast={toast} derecho_admision={derecho_admision} fur={fur} /> : <p>Esperando confirmación de Fur...</p>}
            </div>

            <ModalFur toast={toast} typeFur="PRESCRIPCION" />

            <ToastContainer enableMultiContainer containerId={'Z'}
                position="top-right"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnVisibilityChange
                draggable
                pauseOnHover
            />
            <ToastContainer />
        </div>
    )
}

export default PrescripcionCreate;