import React  from 'react';

const Card = (props) => (
    <div className="card">
        <div className="card-content">
            <div className="card-body">
                <span onClick={props.onClick} className="close"><i className="fa fa-times" aria-hidden="true"></i></span>
                <div className="media d-flex">
                    <div className="media-body text-left " style={{ overflow: 'hidden'}}>
                        <h5 className="danger title5">{props.title}</h5>
                        <span>{props.description}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
);

export default Card;