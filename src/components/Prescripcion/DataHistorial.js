import React, { Component, useEffect, useState, useRef, useCallback, useMemo } from 'react';
import PasosNavigationPrescripcion from '../utils/PasosNavigationPrescripcion';

import { Link } from 'react-router-dom';
import MapOpenLayer from '../component/MapOpenLayer'
import MapCatastroPublic from '../component/MapCatastroPublic'
import ModalSearchDireccion from '../component/ModalSearchDireccion'
import Links from '../../data/link';

import AsyncSelect from 'react-select/async';
import Fetch from '../utils/Fetch';
import AuthService from '../Usuario/AuthService';
import Languaje from '../../data/es';
import TableDeudas from './TableDeudas';
import TableColindantes from './TableColindantes';

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


const DataHistorial = (props) => {

    const { dataHistorial, title, type } = props
    let tablePropietarios = undefined
    let tableTransferencias = undefined
    if (type === 'propietario') {
        const rowTablePropietario = (sub_cont, documentoIdentidad, fechaInicioPropiedad, nombreRazonSocial) => {
            return <div className={"row pointer row-table-custom ml-1 pt-3 "}
                key={sub_cont + '_' + documentoIdentidad}  >
                <div className="col-3 col-md-3 ">
                    <p>{documentoIdentidad}</p>
                </div>
                <div className="col-7 col-md-7" >
                    {nombreRazonSocial}
                </div>
                <div className="col-2 col-md-2 " >
                    {fechaInicioPropiedad}
                </div>
            </div>
        }

        let sub_cont = 0
        tablePropietarios = dataHistorial.propietariosAnteriores.map(({ documentoIdentidad, fechaInicioPropiedad, nombreRazonSocial }) => {
            sub_cont++
            return rowTablePropietario(sub_cont, documentoIdentidad, fechaInicioPropiedad, nombreRazonSocial)
        })
    }

    if (type === 'transferencia') {
        const rowTableTransferencia = (sub_cont, comprador, tipoTransferencia, vendedor) => {
            return <div className={"row pointer row-table-custom ml-1 pt-3 "}
                key={sub_cont }  >
                <div className="col-4 col-md-4 ">
                    <p>{comprador}</p>
                </div>
                <div className="col-4 col-md-4" >
                    {tipoTransferencia}
                </div>
                <div className="col-4 col-md-4 " >
                    {vendedor}
                </div>
            </div>
        }

        let sub_cont_t = 0
        tableTransferencias = dataHistorial.transferencias.map(({ comprador, tipoTransferencia, vendedor }) => {
            sub_cont_t++
            return rowTableTransferencia(sub_cont_t, comprador, tipoTransferencia, vendedor)
        })
    }


    return (
        <div className="card widget widget-simple" >
            <div className="card-body ">

                <div className="row ">
                    <div className="col-12 widget-header">
                        <h4 className="">{Languaje.historial + " - " + title}</h4>
                    </div>
                </div>

                <div className="widget-content">

                    {type === 'propietario' ?
                        <div className="widget-body">
                            <div className="row  " >
                                <div className="col-3 col-md-3 pt-3">
                                    <p className="font-weight-bold"><strong>C.I.</strong></p>
                                </div>
                                <div className="col-7 col-md-7 pt-3 d-none d-sm-block" >
                                    <p className="font-weight-bold" ><strong>Nombre</strong></p>
                                </div>
                                <div className="col-2 col-md-2 pt-3" >
                                    <p className="font-weight-bold " ><strong>Fecha Inicio</strong></p>
                                </div>
                            </div>

                            <div className="row" style={{ marginLeft: '-15px', marginRight: '0px' }}>
                                <div className="col-12 padding-left-right-0">
                                    {tablePropietarios !== undefined ? tablePropietarios : ""}
                                </div>
                            </div>
                        </div>
                        : ""}

                    {type === 'transferencia' ?
                        <div className="widget-body">
                            <div className="row  " >
                                <div className="col-4 col-md-4 pt-3">
                                    <p className="font-weight-bold"><strong>Comprador</strong></p>
                                </div>
                                <div className="col-4 col-md-4 pt-3 d-none d-sm-block" >
                                    <p className="font-weight-bold" ><strong>Tipo</strong></p>
                                </div>
                                <div className="col-4 col-md-4 pt-3" >
                                    <p className="font-weight-bold " ><strong>Vendedor</strong></p>
                                </div>
                            </div>

                            <div className="row" style={{ marginLeft: '-15px', marginRight: '0px' }}>
                                <div className="col-12 padding-left-right-0">
                                    {tableTransferencias !== undefined ? tableTransferencias : ""}
                                </div>
                            </div>
                        </div>
                        : ""}

                </div>
            </div>
        </div>
    )
}
export default DataHistorial