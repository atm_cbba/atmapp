/*!
 * strength.js
 * Original author: @aaronlumsden
 * Further changes, comments: @aaronlumsden
 * Licensed under the MIT license
 */
(function ($, window, document, undefined) {

    var pluginName = "strength",
        defaults = {
            strengthClass: 'strength',
            strengthMeterClass: 'strength_meter',
            strengthButtonClass: 'button_strength',
            strengthButtonText: 'Show Password',
            strengthButtonTextToggle: 'Hide Password'
        };

    function Plugin(element, options) {
        this.element = element;
        this.$elem = $(this.element);
        this.options = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;
        this.init();
    }

    Plugin.prototype = {

        init: function () {
            var characters = 0;
            var capitalletters = 0;
            var loweletters = 0;
            var number = 0;
            var special = 0;

            var upperCase = new RegExp('[A-Z]');
            var lowerCase = new RegExp('[a-z]');
            var numbers = new RegExp('[0-9]');
            var specialchars = new RegExp('([!,%,&,@,#,$,^,*,?,_,~])');

            function GetPercentage(a, b) {
                return ((b / a) * 100);
            }

            function check_strength(thisval, thisid) {
                if (thisval.length > 8) { characters = 1; } else { characters = 0; };
                if (thisval.match(upperCase)) { capitalletters = 1 } else { capitalletters = 0; };
                if (thisval.match(lowerCase)) { loweletters = 1 } else { loweletters = 0; };
                if (thisval.match(numbers)) { number = 1 } else { number = 0; };

                var total = characters + capitalletters + loweletters + number + special;
                var totalpercent = GetPercentage(7, total).toFixed(0);

                get_total(total, thisid);
            }

            function get_total(total, thisid) {

                var thismeter = $('div[data-meter="' + thisid + '"]');
                if (total === 0) {
                    thismeter.removeClass().html('');
                } else if (total <= 1) {
                    thismeter.removeClass();
                    thismeter.addClass('veryweak').html('<p>Muy Debil</p>');
                    //thismeter.addClass('veryweak').html('<p></p>');
                } else if (total === 2) {
                    thismeter.removeClass();
                    thismeter.addClass('weak').html('<p>Debil</p>');
                    //thismeter.addClass('weak').html('<p></p>');
                } else if (total === 3) {
                    thismeter.removeClass();
                    thismeter.addClass('medium').html('<p>Media</p>');
                    //thismeter.addClass('medium').html('<p></p>');
                } else {
                    thismeter.removeClass();
                    thismeter.addClass('strong').html('<p>Segura</p>');
                    //thismeter.addClass('strong').html('<p></p>');
                }
                console.log(total);
            }

            var isShown = false;
            var strengthButtonText = this.options.strengthButtonText;
            var strengthButtonTextToggle = this.options.strengthButtonTextToggle;


            var thisid = this.$elem.attr('id');

            //this.$elem.addClass(this.options.strengthClass).attr('data-password', thisid).after('<input style="display:none" class="' + this.options.strengthClass + '" data-password="' + thisid + '" type="text" name="" value=""><a data-password-button="' + thisid + '" href="" class="' + this.options.strengthButtonClass + '">' + this.options.strengthButtonText + '</a><div class="' + this.options.strengthMeterClass + '"><div data-meter="' + thisid + '"><p></p></div></div>');

            this.$elem.addClass(this.options.strengthClass).attr('data-password', thisid).after('<div class="' + this.options.strengthMeterClass + '"><div data-meter="' + thisid + '"><p></p></div></div>');
            
            this.$elem.bind('keyup keydown', function (event) {
                var thisval = $('#' + thisid).val();
                $('input[type="text"][data-password="' + thisid + '"]').val(thisval);
                check_strength(thisval, thisid);

            });

            $('input[type="text"][data-password="' + thisid + '"]').bind('keyup keydown', function (event) {
                var thisval = $('input[type="text"][data-password="' + thisid + '"]').val();
                //console.log(thisval);
                $('input[type="password"][data-password="' + thisid + '"]').val(thisval);
                check_strength(thisval, thisid);

            });

            //mostrar el password
            $(document.body).on('click', '.' + this.options.strengthButtonClass, function (e) {
                e.preventDefault();
                //console.log("hola mundo");
                alert("click para mostrar boton");

                var thisclass = 'hide_' + $(this).attr('class');

                if (isShown) {
                    $('input[type="text"][data-password="' + thisid + '"]').hide();
                    $('input[type="password"][data-password="' + thisid + '"]').show().focus();
                    $('a[data-password-button="' + thisid + '"]').removeClass(thisclass).html(strengthButtonText);

                    //$('input[type="password"][data-password="'+thisid+'"]').attr("type", "password")
                    isShown = false;

                } else {
                    $('input[type="text"][data-password="' + thisid + '"]').show().focus();
                    $('input[type="password"][data-password="' + thisid + '"]').hide();
                    $('a[data-password-button="' + thisid + '"]').addClass(thisclass).html(strengthButtonTextToggle);

                    //$('input[type="text"][data-password="'+thisid+'"]').attr("type", "text")
                    isShown = true;
                }
            });
        },

        /*yourOtherFunction: function (el, options) {
            // some logic
        }*/
    };

    // A really lightweight plugin wrapper around the constructor,
    // preventing against multiple instantiations
    $.fn[pluginName] = function (options) {
        return this.each(function () {
            if (!$.data(this, "plugin_" + pluginName)) {
                $.data(this, "plugin_" + pluginName, new Plugin(this, options));
            }
        });
    };

})(window.jQuery, window, document);


