window.jQuery(function () {

  'use strict'

  if (window.jQuery('.open-menu').length > 0) {
    window.jQuery('.open-menu').on('click', function (e) {
      e.preventDefault();
      window.jQuery('.sidebar').addClass('active');
      window.jQuery('.overlay').addClass('active');
      // close opened sub-menus
      window.jQuery('.collapse.show').toggleClass('show');
      window.jQuery('a[aria-expanded=true]').attr('aria-expanded', 'false');
    });
  }
})