import React, { Component, useContext, useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import AuthService from '../../components/Usuario/AuthService';
import Fetch from '../../components/utils/Fetch';

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import Links from '../../data/link';
import Config from '../../data/config';
import Constant from '../../data/constant';

const USUARIO = 'USUARIO'

const Header = (props) => {

    let Auth = new AuthService();
    let auth = Auth
    let fetch = new Fetch();
    fetch.setToast(toast);

    const [totalMenu, setTotalMenu] = useState(1)
    const [username, setUsername] = useState("")
    const [role, setRole] = useState("")
    const [thumbail, setThumbail] = useState("")
    const [titleUsuario, setTitleUsuario] = useState("")

    useEffect(  () => {
        if (Auth.loggedIn()) {
            let usuario = Auth.getProfile()
            loadDataUser(usuario)
        } else
                setTotalMenu(1)
    }, []);

    const loadDataUser = async (usuario) => {
        const response = await fetch.axiosAsyncGet(`api/usuario/get-thumbail`);

        setTotalMenu(3)
        setUsername(usuario.username)
        setRole(usuario.name)
        if (response !== null && response.status === true) {
            if (response.Thumbail !== null && response.Thumbail !== "") {
                setThumbail(response.Thumbail)
            } else {
                setThumbail('https://secure.gravatar.com/avatar/950057323d85eb9984bc44374502823d?s=50&d=mm&r=g')
            }
        } else
        setThumbail('https://secure.gravatar.com/avatar/950057323d85eb9984bc44374502823d?s=50&d=mm&r=g')

        if (usuario.code.toUpperCase() === Constant[0].grupos.contribuyente.toUpperCase())
            setTitleUsuario(USUARIO)  //no el menu usuario al contribuyente
    }

    const handleClickLogut = (event) => {
        event.preventDefault()
        auth.logout()
        window.location.href = '/';
    }

    const handleClickLink = (event) => {
        props.updateShowPanelTop(event, false)
    }

    const handleHomeClickLink = (event) => {
        props.updateShowPanelTop(event, true)
    }

    const { menu } = props;
    return <div>
        <nav className="navbar navbar-expand-lg fixed-top activate-menu navbar-light bg-light">
            <Link to={{ pathname: "/" }} className={`navbar-brand`} onClick={handleHomeClickLink}><img src={Config[2].url + '/static/img/escudo_cbba.png'} alt="escudo cbba" className="img-fluid logo-header" /> </Link>

            {Auth.loggedIn() ?
                <Link to={{ pathname: "#" }} className={`nav-link dropdown-toggle avatar-m`}
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                    <img src={thumbail} className="rounded-circle z-depth-0 logo-user" alt="avatar image" />
                </Link>
                : ""}

            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarSupportedContent">
                <ul className="navbar-nav ml-auto">
                    {!Auth.loggedIn() ?
                        menu && menu.map(
                            (menu, key) => <li key={key}>

                                {menu.title.toUpperCase() !== USUARIO ?
                                    <Link to={menu.url} className={`nav-link${menu.url === '/login' ? ' h-button h-button--sales' : ''}`} onClick={handleClickLink}>
                                        {menu.title}
                                        {menu.url === '/login' ?
                                            <i className="fa fa-chevron-right" aria-hidden="true" style={{ fontSize: '13px' }}></i>
                                            : ""
                                        }
                                    </Link>
                                    : ""}
                            </li>
                        )
                        : menu && menu.map(
                            (menu, key) => <li key={key}>
                                {key < totalMenu && menu.title.toUpperCase() !== titleUsuario.toUpperCase() ?
                                    <Link to={menu.url} className={`nav-link${menu.url === '/login' ? ' h-button ' : ''}`} onClick={handleClickLink}>
                                        {menu.title}
                                        {menu.url === '/login' ?
                                            <i className="fa fa-chevron-right" aria-hidden="true" style={{ fontSize: '13px' }}></i>
                                            : ""
                                        }
                                    </Link>
                                    : ""}
                            </li>
                        )
                    }
                </ul>
            </div>

            {Auth.loggedIn() ?
                <>
                    <Link to={{ pathname: "#" }} className={`nav-link dropdown-toggle avatar-d`} id="navbarDropdownMenuLink-d"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                        <img src={thumbail} className="rounded-circle z-depth-0 logo-user" alt="avatar image" />
                    </Link>

                    <div className="dropdown-menu dropdown-menu-right dropdown-secondary menu" aria-labelledby="navbarDropdownMenuLink-d">
                        <div className="client-card">
                            <div className="client-card__picture">
                                <img alt="gravatar" title="avatar" src={thumbail} className="logo-user" />
                            </div>
                            <div className="client-card__details">
                                <div className="client-card__title">
                                    {role}
                                </div>
                                <div className="client-card__subtitle">
                                    {username}
                                </div>
                            </div>
                        </div>

                        <div className="client-card__profile-routes">
                            <Link to={{ pathname: Links[8].url }} className={`dropdown-item`} onClick={handleClickLink} >Perfil</Link>
                            <Link to={{ pathname: "#" }} className={`dropdown-item`}></Link>
                            <Link to={{ pathname: "#" }} className={`dropdown-item`} onClick={handleClickLogut}>Salir</Link>
                        </div>
                    </div>
                </>
                : ""}
        </nav>
    </div>
        ;
}

Header.propTypes = {
    title: PropTypes.string.isRequired,
    menu: PropTypes.array.isRequired
}

export default Header;