import React, { useContext, useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import Config from '../../data/config';
import Contact from '../Home/Contact';
import Texto from '../../data/es';
import Links from '../../data/link';


import { UserContext } from '../utils/UserContext';

const Footer = (props) => {

  let contact = new Contact()

  const { showPanelTop, setShowPanelTop, isPageOrigin, setIsPageOrigin } = useContext(UserContext)

  const [showBannerFooter, setShowBannerFooter] = useState(true)

  useEffect(() => {
    showBanner()
/*
    const onUnload = () => "¿Quieres salir?";

    window.addEventListener("beforeunload", onUnload);

    return () => window.removeEventListener("beforeunload", onUnload);
*/
  }, [showPanelTop]);

  const showBanner = () => {
    let protocol = window.location.protocol
    let host = window.location.hostname + ":3000"
    let path = window.location.pathname

    if (isPageOrigin === 'origen') {
      if (window.location.protocol + "//" + host + path === Config[2].url + "/") {
        setShowBannerFooter(true)
      } else {
        setShowBannerFooter(false)
      }
    } else {
      if (showPanelTop === false) {
        setShowBannerFooter(false)
      } else {
        setShowBannerFooter(true)
      }
    }
  }

  const handleMailClick = (event) => {
    event.preventDefault()
    window.jQuery("#modalFormContacts").modal("show")
  }

  const handleClickNotificcion = (event) => {
    event.preventDefault()
    window.location.href = Links[21].url;
  }

  const handleMailClickTramiteVehiculo = (event) => {
    event.preventDefault()
    window.open('https://www.cochabamba.bo/login', '_blank');
  }

  const handleClickLink = (event) => {
    setShowBannerFooter(false)
  }

  const handleHomeClickLink = () => {
    setShowBannerFooter(true)
  }

  return (
    <>
      <footer className=" text-center pos-re footer__box">
        {showBannerFooter ?
          <div className="container d-none d-sm-block">
            <div className="row  banner-footer">
              <div className="col-12 col-md-6 pb-2 pl-2 text-justify pointer" onClick={handleClickNotificcion}>
                <h5>Notificaciones</h5>
                <span >Oficina virtual ATM, notificación de actos, actuados y/o actuaciones administrativas.</span>
              </div>
              <div className="col-12 col-md-6 pb-2 pl-2 text-justify pointer" onClick={handleMailClickTramiteVehiculo}>
                <h5>Trámite de Vehículos</h5>
                <span>Registro de admisión de trámites y notificaciones.</span>
              </div>
            </div>
          </div>
          : ""}

        {showBannerFooter ?
          <div className="container d-block d-sm-none background-degradado-color pb-2">
            <div className="footer__box">
              <div className="row pt-2 pb-2">
                <div className="col-6 d-inline-block pointer pt-3" onClick={handleClickNotificcion}>
                  <h5 className="text-white">Notificaciones</h5>
                </div>
                <div className="col-6 d-inline-block  pointer pt-3" onClick={handleMailClickTramiteVehiculo}>
                  <h5 className="text-white">Trámite de Vehículos</h5>
                </div>
              </div>
            </div>
          </div>
          : ""}


        <div className="container-fluid background-vine pt-5">
          <div className={(showBannerFooter ? "pt-5" : "") + " d-none d-sm-block"}></div>
          <div className="container ">
            <div className="row ">
              <div className="col-12 col-md-5 pb-5">
                <div className="footer-atm">
                  <Link to={{ pathname: "/" }} className="logo d-inline" onClick={handleHomeClickLink}><img src={Config[2].url + '/static/img/logo_atm.png'} alt="logo" className="img-fluid logo-footer" /></Link>
                  <h6 className="text-footer-atm text-white" >{Texto.administracion_tributaria_municipal}</h6>
                </div>
                <div className="footer-copy-right pt-4 pb-4">
                  <small className="text-white">© {(new Date()).getFullYear()} {Texto.gobierno_autonomo_municipal_cbba}</small>
                </div>
                <div className="social">
                  <Link to={{ pathname: 'https:/www.facebook.com/gamcochabamba' }} target="_blank" rel="noreferrer" aria-label="Facebook de GAMC">
                    <span className="badge badge-light red-social ">
                      <i className="fa fa-facebook  fa-2x text-white" aria-hidden="true"></i>
                    </span>

                  </Link>
                  <Link to={{ pathname: 'https://twitter.com/gam_cochabamba' }} target="_blank" rel="noreferrer" aria-label="Twitter de GAMC">
                    <span className="badge badge-light red-social red-social-circle">
                      <i className="fa fa-twitter fa-2x text-white" aria-hidden="true"></i>
                    </span>
                  </Link>
                  <Link to={{ pathname: 'https://www.instagram.com/cochabambaciudad' }} target="_blank" rel="noreferrer" aria-label="Instagram de GAMC">
                    <span className="badge badge-light red-social red-social-circle">
                      <i className="fa fa-instagram fa-2x text-white" aria-hidden="true"></i>
                    </span>
                  </Link>
                  <Link to={"#"} aria-label="Formulario de Contacto de GAMC">
                    <span className="badge badge-light red-social red-social-circle">
                      <i className="fa fa-envelope-o fa-2x text-white" aria-hidden="true" onClick={handleMailClick} ></i>
                    </span>
                  </Link>
                </div>
              </div>

              <div className="col-12 col-md-7 pb-5 text-left">
                <div className="row">
                  <div className="col">
                    <p className="footer-title">{Texto.atm}</p>
                    <ul>
                      <li><Link to={{ pathname: Links[1].url }} aria-label={Links[1].title} className="text-white" onClick={handleClickLink}>{Links[1].title}</Link></li>
                      <li><Link to={{ pathname: Links[3].url }} aria-label={Links[3].title} className="text-white" onClick={handleClickLink}>{Links[3].title}</Link></li>
                      <li><Link to={{ pathname: Links[15].url }} aria-label={Links[15].title} className="text-white" onClick={handleClickLink}>{Links[15].code}</Link></li>
                      <li><Link to={{ pathname: Links[33].url }} aria-label={Links[34].title} className="text-white" onClick={handleClickLink}>{Links[34].title}</Link></li>
                      <li><Link to={{ pathname: Links[36].url }} aria-label={Links[36].title} className="text-white" onClick={handleClickLink}>{Links[36].title}</Link></li>
                    </ul>
                  </div>
                  <div className="col">
                    <p className="footer-title">{Texto.departamentos}</p>
                    <ul>
                      <li><Link to={{ pathname: '#' }} aria-label="Ingresos Tributarios" className="text-white" >Ingresos Tributarios</Link></li>
                      <li><Link to={{ pathname: '#' }} aria-label="Fiscalización" className="text-white" >Fiscalización</Link></li>
                      <li><Link to={{ pathname: '#' }} aria-label="Legal Tributario" className="text-white" >Legal Tributario</Link></li>
                      <li><Link to={{ pathname: '#' }} aria-label="Plataforma" className="text-white" >Plataforma</Link></li>
                    </ul>
                  </div>
                  <div className="col">
                    <p className="footer-title">{Texto.herramientas}</p>
                    <ul>
                      <li><Link to={{ pathname: Links[22].url }} aria-label="Guías" className="text-white" style={{ fontSize: '14px' }} onClick={handleClickLink}>{Links[22].title}</Link></li>
                      <li><Link to={{ pathname: 'https://www.cochabamba.bo/login' }} target="_blank" aria-label="Guia de trámites de vehículos" className="text-white" style={{ fontSize: '14px' }} >{Texto.tramite_vehiculo}</Link></li>
                      <li><Link to={"#"} aria-label={Texto.contactanos} onClick={handleMailClick} className="text-white">{Texto.contactanos}</Link></li>
                      <li><Link to={{ pathname: Links[23].url }} className="text-white" style={{ fontSize: '14px' }} onClick={handleClickLink}>{Links[23].title}</Link></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="row">
          <div id="mapUbicacionActividadEconomica" className="map-layer" style={{ width: "794px" }}></div>
        </div>

      </footer>
      <Contact />
    </>
  )
}

export default Footer;