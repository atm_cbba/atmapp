import React, { useState, useRef } from 'react';
import Texto from '../../data/es';
import Constant from '../../data/constant';
import Fetch from '../utils/Fetch';
import { Link } from 'react-router-dom';

import AuthService from '../Usuario/AuthService';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Links from '../../data/link';
import TitlePage from '../utils/TitlePage';

let breadcrumbs = [
    {
        title: Links[0].title,
        url: Links[0].url
    },
    {
        title: Links[38].title,
        url: Links[38].url
    }
];

const Perdonazo2020 = (props) => {

    const constant = Constant[0];
    const fetch = new Fetch();
    fetch.setToast(toast);
    const auth = new AuthService();

    const titlePage = Texto.ddjj
    const profile = auth.getProfile();

    //const [showActEco, setActividadEconomica] = useState(true);
    //const [showGuiaVide, setGuiaVideo] = useState(false);
    //const refModal = useRef();

    const redirectToPage = (event, url) => {
        event.preventDefault()
        window.open(url, '_blank');
    }

    return (
        <div className="paddingTop30" id="features" >
            <TitlePage titlePage={titlePage} breadcrumbs={breadcrumbs} position={'left'} leftfull={false} />

            <div className="container features " style={{ marginTop: '-30px' }}  >
                <section className="" style={{ padding: 15 }}>

{/*
                    <div className="row">
                        <div className="col-12 col-md-6 col-lg-6 text-center mb-5"  >
                            <Link to={{ pathname: Links[27].url }} target="_blank" aria-label="Guia de trámites de vehículos"
                                className="text-secondary" style={{ fontSize: 18 }} >
                                <img src="/static/img/law.png" className="rounded z-depth-0" alt="LM 7019-2020" />
                                {'LEY MUNICIPAL 0719/2020 DE INCENTIVO TRIBUTARIO'}
                            </Link>
                        </div>

                        <div className="col-12 col-md-6 col-lg-6 text-center mb-5"  >
                            <Link to={{ pathname: Links[28].url }} target="_blank" aria-label="Guia de trámites de vehículos"
                                className="text-secondary" style={{ fontSize: 18 }} >
                                <img src="/static/img/law.png" className="rounded z-depth-0" alt="DM 187-2020" />
                                {'D.M. 187/2020 APROBACIÓN REGLAMENTO L.M. 0719/2020'}
                            </Link>
                        </div>
                    </div>
*/}
                    <div className="row">
                        <div className="col-12 col-md-0 col-lg-2"  >
                        </div>
                        <div className="col-12 col-md-6 col-lg-4" onClick={e => redirectToPage(e, Links[24].url)} >
                            <div className="button-big button-big-dos " >
                                <div className="left"><h3>{Texto.terceraedad}</h3>
                                    <span>SOLICITUD DE AUTORIZACIÓN DE DESCUENTO DE ADULTO MAYOR</span></div>
                                <div className="right icon-grandsparents"></div>
                            </div>
                        </div>
                        <div className="col-12 col-md-6 col-lg-4" onClick={e => redirectToPage(e, Links[25].url)} >
                            <div className="button-big button-big-dos " >
                                <div className="left"><h3>{Texto.serviciopublico}</h3>
                                    <span>SOLICITUD DE CERTIFICADO DE TRANSPORTE DE SERVICIO PUBLICO</span></div>
                                <div className="right icon-service-public"></div>
                            </div>
                        </div>
                        <div className="col-12 col-md-0 col-lg-2"  >
                        </div>
                    </div>

                </section>
            </div>
        </div>
    )
}

export default Perdonazo2020;