import React, { useState, useRef } from 'react';
import  { Redirect } from 'react-router-dom'
import Texto from '../../data/es';
import Constant from '../../data/constant';
import Fetch from '../utils/Fetch';
import { Link } from 'react-router-dom';
import Links from '../../data/link';
import Config from '../../data/config';

import AuthService from '../Usuario/AuthService';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import TitlePage from '../utils/TitlePage';

let breadcrumbs = [
    {
        title: Links[0].title,
        url: Links[0].url
    },
    {
        title: Links[40].title,
        url: Links[40].url
    }
];

const Fiscalizacion = (props) => {

    const constant = Constant[0];
    const fetch = new Fetch();
    fetch.setToast(toast);
    const auth = new AuthService();

    const titlePage = Texto.fiscalizacion
    const profile = auth.getProfile();

    const redirectToPage = (event, url) => {
        event.preventDefault()
        props.history.push(url)
    }

    return (
        <div className="paddingTop30" id="features" >
            <TitlePage titlePage={titlePage} breadcrumbs={breadcrumbs} position={'left'} leftfull={false} />

            <div className="container features " style={{ marginTop: '-30px' }} >

                <div className="row">
                    <div className="col-sm">
                        <div className="blog-item-box">
                            <figure className="blog-item pointer" style={{ background: 'transparent' }} onClick={e => redirectToPage(e,  Links[43].url)}>
                                <div className="image">
                                    <img className='img-thumbnail rounded mx-auto d-block img-noborder' alt='img-consulta-tu-deuda'
                                        src={Config[2].url + '/static/img/bt_prescripcion.png'} />
                                    <i className="fa fa-pencil" aria-hidden="true"></i>
                                    <div className="date"><span className="fa fa-lock" aria-hidden="true" ></span></div>
                                </div>
                                <figcaption className="p-0">
                                </figcaption>
                            </figure>
                        </div>
                    </div>

                    <div className="col-sm">
                    </div>

                    <div className="col-sm">
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Fiscalizacion;