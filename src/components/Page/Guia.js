import React, { useState, useRef, useEffect, useContext } from 'react';
import AsyncSelect from 'react-select/async';
import Languaje from '../../data/es';
import Constant from '../../data/constant';
import Fetch from '../utils/Fetch';
import FullScreenModal from '../utils/FullScreenModal';
import StateManager from 'react-select';
import { UserContext } from '../utils/UserContext';

var derecho_admision_permanante = 1;
var derecho_admision_temporal = 2;
var optionsSelect = [];
var fetch = null

const Guia = (props) => {

    const {showPanelTop, setShowPanelTop} = useContext(UserContext)

    const [showActEco, setActividadEconomica] = useState(true);
    const [showGuiaVide, setGuiaVideo] = useState(false);

    const refModal = useRef();

    const onSubmitForm = (event) => {
        event.preventDefault()
    }

    useEffect(() => {
        window.scrollTo(0, 0);
    }, []);

    const loadOptionPermanente = async (inputValue) => {
        if (inputValue.length > 2) {
            fetch = new Fetch();
            const response = await fetch.axiosAsyncGet(`api/tipo-actividad-economica/search-by-name-free/${inputValue}/${derecho_admision_permanante}`);
            if (response.status === true)
                return response.data;
            else
                return [];
        } else {
            return optionsSelect
        }
    }

    const noOptionsMessage = (props) => {
        let search = "";
        if ((parseInt(derecho_admision_permanante) === parseInt(Constant[0].derecho_admision.permanente)))
            search = Languaje.ingresa_tipo_actividad_economica + " - " + Languaje.permanente
        else
            search = Languaje.ingresa_tipo_actividad_economica + " - " + Languaje.temporal
        if (props.inputValue === '') {
            return (search);
        }
        return (Languaje.criterio_busqueda_no_corresponde);
    }

    const loadOptionTemporal = async (inputValue) => {
        if (inputValue.length > 2) {
            fetch = new Fetch();
            const response = await fetch.axiosAsyncGet(`api/tipo-actividad-economica/search-by-name-free/${inputValue}/${derecho_admision_temporal}`);
            if (response.status === true)
                return response.data;
            else
                return [];
        } else {
            return optionsSelect
        }
    }

    const openModalFullScreen = (event) => {
        refModal.current.openModal(null, true)
    }

    const handleClickShowActividadEconomica = (event) => {
        setActividadEconomica(true)
        setGuiaVideo(false)
    }

    const handleClickShowVideo = (event) => {
        setActividadEconomica(false)
        setGuiaVideo(true)
    }

    return (
        <div id="services" className="paddingTop30" >
            <div className="container-fluid">

                <div className="row">
                    <div className="col-md-12">
                        <div className="" style={{ marginTop: '40px' }}>
                            <h1 className="custom-h1 text-left"> Recursos</h1>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-12 col-md-3 col-lg-3 uk-container-menu ">
                        <aside className="">
                            <ul className="uk-nav mediatype-nav">
                                <li className="uk-nav-header text-white">Categoria</li>
                                <li className="left-nav--category text-white">
                                    <a href="#" className="text-white" onClick={handleClickShowActividadEconomica}>Actividades Económicas</a>
                                </li>
                                <li className="left-nav--category text-white">
                                    <a href="#" className="text-white" onClick={handleClickShowVideo}>Videos</a>
                                </li>
                                {/*<li className="left-nav--category text-white">
                                    <a href="#" className="text-white">Guías</a>
                                </li>
                                <li className="left-nav--category text-white">
                                    <a href="#" className="text-white">Social</a>
    </li>*/}
                            </ul>
                        </aside>
                    </div>
                    {
                        showActEco ?
                            <div className="col-12 col-md-9 col-lg-9 container-recursos">
                                <div className="recuros-block">
                                    <div className="actividad-economica">
                                        <strong>DA88 - PERMANENTE</strong>
                                        <strong>Bs. 55</strong>
                                        <form className="contact__form needs-validation mt-3"
                                            method="post" noValidate onSubmit={onSubmitForm} style={{ width: '100%' }}>

                                            <div className="col-12 col-md-1 ">
                                            </div>
                                            <div className="col-12 col-md-10 center-container">
                                                <AsyncSelect
                                                    cacheOptions
                                                    loadOptions={loadOptionPermanente}
                                                    defaultOptions
                                                    isClearable
                                                    isSearchable
                                                    placeholder={Languaje.tipo_actividad_economica}
                                                    required
                                                    defaultValue={[optionsSelect[0]]}
                                                    noOptionsMessage={noOptionsMessage}
                                                />
                                                <span>Texto solicitado: </span>
                                            </div>
                                            <div className="col-12 col-md-1">
                                            </div>
                                        </form>
                                    </div>
                                    <div className="actividad-economica">
                                        <strong>DA89 - TEMPORAL</strong>
                                        <strong>Bs. 100</strong>

                                        <form className="contact__form needs-validation mt-3"
                                            method="post" noValidate onSubmit={onSubmitForm} style={{ width: '100%' }}>
                                            <div className="col-12 col-md-1 ">
                                            </div>
                                            <div className="col-12 col-md-10 center-container">
                                                <AsyncSelect
                                                    className=""
                                                    cacheOptions
                                                    loadOptions={loadOptionTemporal}
                                                    defaultOptions
                                                    isClearable
                                                    isSearchable
                                                    placeholder={Languaje.tipo_actividad_economica}
                                                    required
                                                    defaultValue={[optionsSelect[0]]}
                                                    noOptionsMessage={noOptionsMessage}
                                                />
                                                <span>Texto solicitado: </span>
                                            </div>
                                            <div className="col-12 col-md-1">
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div className="recuros-block">
                                    <p className="text-justify">
                                        Para el llenado del formulario, previamente debe adquirir el Formulario Único de Recaudaciones (FUR), en caja recaudadora (GAMC) o entidades financieras autorizadas.
                                    </p>
                                </div>
                            </div>
                            : ""
                    }

                    {
                        showGuiaVide ?
                            <div className="col-12 col-md-9 col-lg-9 container-recursos">
                                <div className="recuros-block">
                                    <h6>Guía para el llenado de la DDJJ de Actividades Económicas</h6>
                                </div>

                                <div className="embed-container mt-4">
                                    <iframe width="560" height="315" src="https://www.youtube.com/embed/i0moEU0GGrM" frameBorder="0"
                                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowFullScreen >
                                    </iframe>
                                </div>
                            </div>
                            : ""
                    }
                </div>

                <div className="row">
                    <div className="col-12">
                        <div className="floating-nav uk-position-bottom-center">
                            <button type="button" className="btn btn-outline-secondary rounded btn-flotante btn-flotante-mobil" onClick={openModalFullScreen} >
                                <i className="fa fa-bars" aria-hidden="true"></i>
                            </button>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <FullScreenModal ref={refModal} clickShowActividad={handleClickShowActividadEconomica} clickShowVideo={handleClickShowVideo} />
                </div>
            </div>
        </div>
    )
}

export default Guia;