import React, { useRef, useEffect } from 'react';
import Fetch from '../utils/Fetch';
import Constant from '../../data/constant';
import Config from '../../data/config';
import AuthService from '../Usuario/AuthService';
import Links from '../../data/link';
import TitlePage from '../utils/TitlePage';
import Texto from '../../data/es';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

let breadcrumbs = [
    {
        title: Links[0].title,
        url: Links[0].url
    },
    {
        title: Links[36].title,
        url: Links[36].url
    }
];

const ConsultaTuDeuda = (props) => {

    const constant = Constant[0];
    const fetch = new Fetch();
    fetch.setToast(toast);
    const auth = new AuthService();

    const titlePage = Texto.consulta_tu_deuda
    const profile = auth.getProfile();

    useEffect(() => {
        window.scrollTo(0, 0);
    }, []);

    const handleRedirectUrlClick = (ev, url) => {
        ev.preventDefault();
        window.open(url, '_blank');
    }

    return (
        <div className="paddingTop30" id="features" >
            <TitlePage titlePage={titlePage} breadcrumbs={breadcrumbs} position={'left'} leftfull={false} />

            <div className="container features " style={{ marginTop: '-30px' }} >

                <div className="row">
                    <div className="col-sm">
                        <div className="blog-item-box">
                            <figure className="blog-item pointer" style={{ background: 'transparent' }} onClick={e => handleRedirectUrlClick(e, 'https://www.ruat.gob.bo/vehiculos/consultageneral/InicioBusquedaVehiculo.jsf')}>
                                <div className="image">
                                    <img className='img-thumbnail rounded mx-auto d-block img-noborder' alt='img-consulta-tu-deuda'
                                        src={Config[2].url + '/static/img/bt_vehiculo.png'} />
                                    <i className="fa fa-clock-o" aria-hidden="true"></i>
                                    <div className="date"><span className="day fa fa-usd" aria-hidden="true" style={{ fontSize: 30 }}></span></div>
                                </div>
                                <figcaption>
                                </figcaption>
                            </figure>
                        </div>
                    </div>

                    <div className="col-sm">
                        <div className="blog-item-box">
                            <figure className="blog-item pointer" style={{ background: 'transparent' }} onClick={e => handleRedirectUrlClick(e, 'https://www.ruat.gob.bo/inmuebles/consultageneral/InicioBusquedaInmueble.jsf')}>
                                <div className="image">
                                    <img className='img-thumbnail rounded mx-auto d-block img-noborder' alt='img-consulta-tu-deuda'
                                        src={Config[2].url + '/static/img/bt_inmuebles.png'} />
                                    <i className="fa fa-clock-o" aria-hidden="true"></i>
                                    <div className="date"><span className="day fa fa-usd" aria-hidden="true" style={{ fontSize: 30 }}></span></div>
                                </div>
                                <figcaption>
                                </figcaption>
                            </figure>
                        </div>
                    </div>

                    <div className="col-sm">
                        <div className="blog-item-box">
                            <figure className="blog-item pointer" style={{ background: 'transparent' }} onClick={e => handleRedirectUrlClick(e, 'https://tramitesgamc.cochabamba.bo/#/consulta-sitiosymercados-iframe')}>
                                <div className="image">
                                    <img className='img-thumbnail rounded mx-auto d-block img-noborder' alt='img-consulta-tu-deuda'
                                        src={Config[2].url + '/static/img/bt_mercados.png'} />

                                    <i className="fa fa-clock-o" aria-hidden="true"></i>
                                    <div className="date"><span className="day fa fa-usd" aria-hidden="true" style={{ fontSize: 30 }}></span></div>
                                </div>
                                <figcaption>
                                </figcaption>
                            </figure>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-sm">
                        <div className="blog-item-box">
                            <figure className="blog-item pointer" style={{ background: 'transparent' }} onClick={e => handleRedirectUrlClick(e, 'https://tramitesgamc.cochabamba.bo/#/consulta-sitiosymercados-iframe')}>
                                <div className="image">
                                    <img className='img-thumbnail rounded mx-auto d-block img-noborder' alt='img-consulta-tu-deuda'
                                        src={Config[2].url + '/static/img/bt_publicidad_exterior.png'} />
                                    <i className="fa fa-clock-o" aria-hidden="true"></i>

                                    <div className="date"><span className="day fa fa-usd" aria-hidden="true" style={{ fontSize: 30 }}></span></div>
                                </div>
                                <figcaption>
                                </figcaption>
                            </figure>
                        </div>
                    </div>

                    <div className="col-sm">
                        <div className="blog-item-box">
                            <figure className="blog-item pointer" style={{ background: 'transparent' }} onClick={e => handleRedirectUrlClick(e, 'https://www.ruat.gob.bo/actividadeseconomicas/consultageneral/InicioBusquedaActividadesEconomicas.jsf')}>
                                <div className="image">
                                    <img className='img-thumbnail rounded mx-auto d-block img-noborder' alt='img-consulta-tu-deuda'
                                        src={Config[2].url + '/static/img/bt_actividad_economica.png'} />
                                    <i className="fa fa-clock-o" aria-hidden="true"></i>

                                    <div className="date"><span className="day fa fa-usd" aria-hidden="true" style={{ fontSize: 30 }}></span></div>
                                </div>
                                <figcaption>
                                </figcaption>
                            </figure>
                        </div>
                    </div>

                    <div className="col-sm">
                    </div>

                </div>
            </div>
        </div>
    )
}

export default ConsultaTuDeuda;