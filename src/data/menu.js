export default [
    {
        title: 'CRAM',
        url: '/cram'
    },
    {
        title: 'Usuario',
        url: '/usuario'
    },
    {
        title: 'Licencia de Funcionamiento',
        url: '/licencia-actividad-economica'
    },
    {
        title: 'Ingresar',
        url: '/login'
    }
];

/*{
        title: 'Features',
        url: '#features'
    },
    {
        title: 'Services',
        url: '#services'
    },*/