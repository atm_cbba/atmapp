export default [
    {
        title: 'Inicio',
        url: '/'
    },
    {
        title: 'Licencia de Funcionamiento',
        url: '/licencia-actividad-economica',
        code: 'licenciaFuncionamiento',
    },
    {
        title: 'Declaracion Jurada Create',
        url: '/licencia-actividad-economica-create'
    },
    {
        title: 'Prescripción',
        url: '/prescripcion'
    },
    {
        title: 'Login',
        url: '/login'
    },
    {
        title: 'Registrar Usuario',
        url: '/usuario-create'
    },
    {
        title: 'Declaracion Jurada Edit',
        url: '/licencia-actividad-economica-edit'
    },
    {
        title: 'Confirmar Cuenta de Usuario',
        url: '/usuario-confirmar-cuenta'
    },
    {
        title: 'Perfil',
        url: '/usuario-perfil'
    },
    {
        title: '404',
        url: '/page-404'
    },
    {
        title: 'Recuperar Contraseña',
        url: '/recuperar-contrasenia'
    },
    {
        title: 'Centro de Ayuda',
        url: '/ayuda'
    },
    {  //12
        title: 'Usuario',
        url: '/usuario'
    },
    {  //13
        title: 'Solicitar Envio de Mail para activación de cuenta.',
        url: '/usuario-reactivacion-cuenta'
    },
    {  //14
        title: 'Soporte Gerencial',
        url: '/soporte-gerencial'
    },
    {  //15
        title: 'Certificado de Registro Ambiental Municipal',  //Certificado de Registro Ambiental Municipal
        code: 'Cram',
        url: '/cram'
    },
    {  //16
        title: 'Cram create',
        code: 'CramCreate',
        url: '/cram-create'
    },
    {  //17
        title: 'Ruat',
        code: 'Ruat',
        url: '/licencia-actividad-economica-ruat'
    },
    {  //18
        title: 'Plataforma de Atención Virtual',
        code: 'PlataformaAtencionVirtual',
        url: '#'
    },
    {   //19
        title: 'Prescripón Create',
        url: '/prescripcion-create'
    },
    {   //20
        title: 'Prescripón Edit',
        url: '/prescripcion-edit'
    },
    {   //21
        title: 'Notificaciones',
        code: 'NOTIFICACIONES',
        url: '/notificacion'
    },
    {   //22
        title: 'Guías',
        code: 'GUIA',
        url: '/guia'
    },
    {   //23
        title: 'Perdonazo 2020',
        code: 'Perdonazo2020',
        url: '/perdonazo-2020'
    },
    {   //24
        title: 'Tercera Edad',
        code: 'TerceraEdad',
        //url: 'http://sandbox.cochabamba.bo/descuentos/solicitar-autorizacion'
        url: 'https://perdonazo2020.cochabamba.bo'
    },
    {   //25
        title: 'Servicio Público',
        code: 'ServicioPublico',
        //url: 'http://sandbox.cochabamba.bo/descuentos/seleccionar-persona'
        url: 'https://perdonazo2020.cochabamba.bo/seleccionar-persona'
    },
    {   //26
        title: 'Sitio y Mercados',
        code: 'SitioMercados',
        url: 'https://tramitesgamc.cochabamba.bo/#/consulta-sitiosymercados-iframe'
    },
    {   //27
        title: 'LEY MUNICIPAL DE INCENTIVO TRIBUTARIO 0719/2020',
        code: '0719/2020',
        url: 'https://atmapi.cochabamba.bo/restServerAtm/legal-tributario/document/10'
    },
    {   //28
        title: 'DECRETO MUNICIPAL 187-2020 09-10-2020 APRUEBA REGLAMENTO L.M. 0719-2020 INCENTIVO TRIBUTARIO',
        code: '187/2020',
        url: 'https://atmapi.cochabamba.bo/restServerAtm/legal-tributario/document/11'
    },
    {   //29
        title: 'API Raiz Catastro',
        code: 'ARCGIS_ROOT',
        url: 'http://192.168.105.219:6080/arcgis/services/'
    },
    {   //30
        title: 'API Raiz Catastro busqueda',
        code: 'ARCGIS_SEARCH',
        url: 'http://192.168.105.219:6080/arcgis/rest/services/'
    },
    {   //31
        title: 'ENCUESTA DE PERCEPCIÓN 2020',
        code: 'AIT',
        url: 'https://docs.google.com/forms/d/1XS-8hZcAsES838DmRoubEZ2wE8aRbBRKhWuTW3DfTHg/viewform?edit_requested=true'
    },
    {   //32
        title: 'CONSULTA TU DEUDA',
        code: 'CTD',
        url: '/notificacion'
    },
    {
        //33
        title: 'Liquidaciones',
        code: 'LIQUIDACIONES ',
        url: '/liquidaciones'
    },
    {
        //34
        title: 'Liquidaciones Mixtas',
        code: 'LIQUIDACIONES MIXTAS',
        url: '/liquidaciones/mixtas'
    },
    {
        //35
        title: 'Notificaciones Mixtas',
        code: 'NOTIFICACIONES MIXTAS',
        url: '/notificaciones/mixtas'
    },
    {
        //36
        title: 'Consulta tu Deuda',
        code: 'CONSULTA TU DEUDA',
        url: '/consulta-tu-deuda'
    },
    {   //37
        title: 'Prescripcion Edit',
        url: '/prescripcion-edit'
    },
    {   //38
        title: 'Declaraciones Juradas',
        code: 'ddjj',
        url: '/declaraciones-juradas'
    },
    {   //39
        title: 'Visación Minutas Inmuebles',
        code: 'VISACION',
        url: 'https://sistemavisacionminutas.cochabamba.bo/datos_registro'
    },
    {   //40
        title: 'Fiscalización',
        code: 'FISCALIZACION',
        url: '/fiscalizacion'
    },
    {   //41
        title: 'Consolidación Fiscalización',
        code: 'CONSOLIDACION_FISCALIZACION',
        url: '/prescripcion-fiscalizacion/consolidacion-fiscalizacion'
    },
    {   //42
        title: 'Prescripción Detalle',
        code: 'PRESCRIPCION DETALLE',
        url: '/prescripcion-fiscalizacion/prescripcion-detalle'
    },
    {   //43
        title: 'Prescripción Fiscalizacion',
        code: 'PRESCRIPCION FISCALIZACION',
        url: '/prescripcion-fiscalizacion'
    }
];