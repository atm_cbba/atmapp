import Config from './config';
export default [
    
    {
        class_active: 'active',
        title: "Consejos para prevenir el contagio de COVID-19",
        src: Config[2].url + '/carrousel/consejos_covid.png',
        href: "/",
        alt: "Consejos para prevenir el contagio de COVID-19",
        target: "_self",
        key: 0
    },
    {
        title: "Realiza el pago de tus impuestos desde casa, el banco de tu preferencia",
        src: Config[2].url + '/carrousel/impuestos_desde_casa.jpg',
        href: "https://www.ruat.gob.bo/serviciosdisponibles/pagointernet/InicioPagoInternet.jsf",
        alt: "Realiza el pago de tus impuestos desde casa",
        class_active: '',
        target: "_blank",
        key: 1
    },
    /*{
        title: "Impuestos a la transferencia",
        src: Config[2].url + '/carrousel/impuestos_transferencia.jpg',
        href: "/",
        alt: "Impuestos a la transferencia",
        class_active: '',
        key: 2
    },*/
    /*{
        title: "Descuento en tus impuestos hasta el 30 Junio de 2020",
        src: Config[2].url + '/carrousel/descuento_junio_15.jpg',
        href: "/",
        alt: "Descuento en tus impuestos hasta el 30 Junio de 2020",
        class_active: '',
        target: "_self",
        key: 2
    },*/
    {
        title: "Gracias a tus Impuestos se Generán obras y progreso",
        src: Config[2].url + '/carrousel/gracias_impuesto_progreso.jpg',
        href: "/",
        alt: "Gracias a tus Impuestos se Generán obras y progreso",
        class_active: '',
        target: "_self",
        key: 3
    },
];