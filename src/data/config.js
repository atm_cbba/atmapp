export default [
    {
        title: 'ApiRestServer',
        //url: 'http://192.168.104.49/restServerAtm/'  //production
        url: 'http://localhost/restServerAtm/'
    },
    {
        title: 'Año minimo fecha nacimiento',
        anio:  (new Date()).setFullYear( (new Date()).getFullYear() - 18 )
    },
    {
        title: 'Client Url',
        protocol: window.location.protocol,
        //url: 'http://192.168.104.49:3000'    //producction
        url: 'http://localhost:3000'
    },
    {
        title: 'Token Authorization',
        token: 'token_auth'
    },
    {
        title: 'Format Date',
        format: 'dd/MM/yyyy'
    },
    {
        title: 'Format Date Moment',
        format_moment: 'DD/MM/YYYY'
    },
    {
        title: 'Código Soporte Gerencial',
        token_sg: 'token_sg'
    }
];
