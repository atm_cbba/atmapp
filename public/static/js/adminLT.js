$(function () {

  'use strict'

  if ($('.open-menu').length > 0) {
    $('.open-menu').on('click', function (e) {
      e.preventDefault();
      $('.sidebar').addClass('active');
      $('.overlay').addClass('active');
      // close opened sub-menus
      $('.collapse.show').toggleClass('show');
      $('a[aria-expanded=true]').attr('aria-expanded', 'false');
    });
  }
})