
$(function () {

  $('.mu-simplefilter li').click(function () {
    $('.mu-simplefilter li').removeClass('active');
    $(this).addClass('active');
  });
});


jQuery(document).ready(function ($) {

  //open the testimonials modal page
  $('.cd-see-all').on('click', function () {
    $('.cd-testimonials-all').addClass('is-visible');
  });

  //close the testimonials modal page
  $('.cd-testimonials-all .close-btn').on('click', function () {
    $('.cd-testimonials-all').removeClass('is-visible');
  });
  $(document).keyup(function (event) {
    //check if user has pressed 'Esc'
    if (event.which == '27') {
      $('.cd-testimonials-all').removeClass('is-visible');
    }
  });

  //build the grid for the testimonials modal page
  $('.cd-testimonials-all-wrapper').children('ul').masonry({
    itemSelector: '.cd-testimonials-item'
  });
});


$(function () {
  $(document).click(function (event) {
    $('.navbar-collapse').collapse('hide');
  });

  $('.navbar-nav li a').on('click', function () {
    $('.navbar-collapse').collapse('hide');
  });

});

/* ----------------------------------------------------------- */
/*   Contact form
/* ----------------------------------------------------------- */
(function ($) {
  'use strict';
  var form = $('.contact__form'),
    message = $('.contact__msg'),
    form_data;
  // Success function
  function done_func(response) {
    message.fadeIn().removeClass('alert-danger').addClass('alert-success');
    message.text(response);
    setTimeout(function () {
      message.fadeOut();
    }, 2000);
    form.find('input:not([type="submit"]), textarea').val('');
  }
  // fail function
  function fail_func(data) {
    message.fadeIn().removeClass('alert-success').addClass('alert-success');
    message.text(data.responseText);
    setTimeout(function () {
      message.fadeOut();
    }, 2000);
  }

  form.submit(function (e) {
    e.preventDefault();
    form_data = $(this).serialize();
    $.ajax({
      type: 'POST',
      url: form.attr('action'),
      data: form_data
    })
      .done(done_func)
      .fail(fail_func);
  });

})(jQuery);

/**
 * convert input value to uppercase
 */
var inputTextUpperCase = function () {
  $('input[type=text]').keyup(function (event) {

    let position_start = event.target.selectionStart
    $(this).val($(this).val().toUpperCase());
    setCaretPosition(this, position_start);
  });
}

var inputConvertUpperCaseForm = function (form) {

  var list_input = $("#" + form).find("input[type=text]")
  $.each(list_input, function (index, item) {
    $(item).val($(item).val().toUpperCase());
  })
}

/**
 * permite establecer la posicion actual del cursor dentro un input
 * @param {input} ctrl 
 * @param {position} pos 
 */
function setCaretPosition(ctrl, pos) {
  // Modern browsers
  if (ctrl.setSelectionRange) {
    ctrl.focus();
    ctrl.setSelectionRange(pos, pos);
    // IE8 and below
  } else if (ctrl.createTextRange) {
    var range = ctrl.createTextRange();
    range.collapse(true);
    range.moveEnd('character', pos);
    range.moveStart('character', pos);
    range.select();
  }
}

/**
 * recive un dropdown menu en formato jquery  $("div[class~='dropdown-menu']")
 * @param {*} dropdown_menu  {$("div[class~='dropdown-menu']") }
 */
var event_drop_down = function (dropdown_menu, callback) {

  var events = window.jQuery._data(dropdown_menu.find('a')[0], "events");
  if (events === undefined || events.click.length === 0) {

    dropdown_menu.find('a').click(function () {
      if (Boolean(this.getAttribute("code")))
        this.parentElement.parentElement.firstElementChild.innerHTML = this.getAttribute("code");

      if (callback !== null)
        callback(this.getAttribute("code"), this);
    });
  }
}

var create_input_hidden = function (value, name, destiny_id) {

  //document.getElementsByName(name).remove();
  var element = document.getElementsByName(name);
  if (element.length > 0)
    element[0].remove();

  var input = document.createElement("input");
  input.setAttribute("type", "hidden");
  input.setAttribute("name", name);
  input.setAttribute("id", name);
  input.setAttribute("value", value);
  //append to form element that you want .
  document.getElementById(destiny_id).appendChild(input);
}

//var log = function (log) { $logs.value = log + '\n' + $logs.value; }
var crearCookie = function (key, value) {
  expires = new Date();
  expires.setTime(expires.getTime() + 31536000000);
  cookie = key + "=" + value + ";expires=" + expires.toUTCString();
  //log("crearCookie: " + cookie);
  return document.cookie = cookie;
}

// Leer Cookie
var leerCookie = function (key) {
  keyValue = document.cookie.match("(^|;) ?" + key + "=([^;]*)(;|$)");
  if (keyValue) {
    //log("getCookie: " + key + "=" + keyValue[2]);
    return keyValue[2];
  } else {
    //log("getCookie: " + key + "=" + "null");
    return null;
  }
}

// Eliminar Cookie
var eliminarCookie = function (key) {
  //log("eliminarCookie: " + key);
  return document.cookie = key + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}

function StringToXML(oString) {
  //code for IE
  if (window.ActiveXObject) {
    var oXML = new ActiveXObject("Microsoft.XMLDOM"); oXML.loadXML(oString);
    return oXML;
  }
  // code for Chrome, Safari, Firefox, Opera, etc. 
  else {
    return (new DOMParser()).parseFromString(oString, "text/xml");
  }
}

var blobToBase64 = function (blob, callback) {
  var reader = new FileReader();
  reader.onload = function () {
    var dataUrl = reader.result;
    var base64 = dataUrl.split(',')[1];
    callback(base64);
  };
  reader.readAsDataURL(blob);
};

function getParameterByName(name) {
  name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
  var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(location.search);
  return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

var redirect = function (url, time) {
  time = 2000 || time;
  setTimeout(() => {
    window.location.href = url;
  }, time);
}


var isMobile = {
  Android: function () {
    return navigator.userAgent.match(/Android/i);
  },
  BlackBerry: function () {
    return navigator.userAgent.match(/BlackBerry/i);
  },
  iOS: function () {
    return navigator.userAgent.match(/iPhone|iPad|iPod/i);
  },
  Opera: function () {
    return navigator.userAgent.match(/Opera Mini/i);
  },
  Windows: function () {
    return navigator.userAgent.match(/IEMobile/i);
  },
  any: function () {
    return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
  }
};

var is_json = function (item) {
  item = typeof item !== "string"
    ? JSON.stringify(item)
    : item;

  try {
    item = JSON.parse(item);
  } catch (e) {
    return false;
  }

  if (typeof item === "object" && item !== null) {
    return true;
  }
  return false;
}

function base64ToBlob(base64) {

  if (base64.indexOf('data:application/pdf;base64') >= 0)
    base64 = base64.replace('data:application/pdf;base64,', '')

  var binary = atob(base64.replace(/\s/g, ''));
  var len = binary.length;
  var buffer = new ArrayBuffer(len);
  var view = new Uint8Array(buffer);
  for (var i = 0; i < len; i++) {
    view[i] = binary.charCodeAt(i);
  }

  // create the blob object with content-type "application/pdf"               
  var blob = new Blob([view], { type: "application/pdf" });
  var url = URL.createObjectURL(blob);
  return url;
}

function downloadFile(base64, fileName) {

  var url = base64ToBlob(base64);

  var a = document.createElement("a");
  a.href = url;
  a.download = fileName;
  a.click();
  window.URL.revokeObjectURL(url);
};

/** convertimos el formato regional sud a formato EPSG:3857 */
var convertToUniversalFormat = function (coordinate) {
  var firstProjection = '+proj=utm +zone=19 +south +datum=WGS84 +units=m +no_defs ';
  var secondProjection = "+proj=merc +a=6378137 +b=6378137 +lat_ts=0.0 +lon_0=0.0 +x_0=0.0 +y_0=0 +k=1.0 +units=m +nadgrids=@null +wktext  +no_defs";
  return proj4(firstProjection, secondProjection, JSON.parse("[" + coordinate + "]"));
}


////////////////////////////////////////////
var deleteBootbox = function (item, callback) {
  bootbox.confirm({
    title: '<i class="fa fa-exclamation-circle" aria-hidden="true"></i> Advertencia!',
    message: "Esta Seguro de Querer Eliminar. " + item + ".",
    buttons: {
      cancel: {
        label: '<i class="fa fa-times"></i> Cancelar'
      },
      confirm: {
        className: 'button-style pull-right',
        label: '<i class="fa fa-check"></i> Confirmar'
      }
    },
    callback: function (result) {
      //console.log('This was logged in the callback: ' + result);
      callback(result)
    }
  });
}

var createBootbox = function (msg, callback) {
  bootbox.confirm({
    title: '<i class="fa fa-exclamation-circle" aria-hidden="true"></i> Advertencia!',
    message: msg, //"Esta Seguro de Continuar.",
    buttons: {
      cancel: {
        label: '<i class="fa fa-times"></i> Cerrar'
      },
      confirm: {
        className: 'button-style pull-right',
        label: '<i class="fa fa-check"></i> Continuar'
      },

    },
    callback: function (result) {
      //console.log('This was logged in the callback: ' + result);
      callback(result)
    }
  });
}

var confirmBootbox = function (item, callback) {
  bootbox.confirm({
    title: '<i class="fa fa-exclamation-circle" aria-hidden="true"></i> Advertencia!',
    message: "Esta seguro de la información proporcionada. Confirmada la información no podrá ser modificada?.",
    buttons: {
      cancel: {
        label: '<i class="fa fa-times"></i> Cancelar'
      },
      confirm: {
        className: 'button-style pull-right',
        label: '<i class="fa fa-check"></i> Confirmar'
      }
    },
    callback: function (result) {
      //console.log('This was logged in the callback: ' + result);
      callback(result)
    }
  });
}

var alertBootBox = function (msg) {
  bootbox.dialog({
    title: '<i class="fa fa-exclamation-circle" aria-hidden="true"></i> Advertencia!',
    message: msg, //"Esta Seguro de Continuar.",
    size: 'large',
    buttons: {
      ok: {
        label: '<i class="fa fa-times"></i> Entendido!',
        className: 'button-style pull-right',
        callback: function () { }
      }
    }
  });
}

var bootBoxWithPromtOneInput = function (message, callback) {
  bootbox.prompt({
    title: message,
    centerVertical: false,
    buttons: {
      cancel: {
        label: '<i class="fa fa-times"></i> Cancelar'
      },
      confirm: {
        className: 'button-style pull-right',
        label: '<i class="fa fa-check"></i> Confirmar'
      }
    },
    callback: function (result) {
      //console.log(result); 
      callback(result)
    }
  });
}


var createBootBoxCustom = function (msg, new_item = false, callback) {

  let menu_buttons = {
    cancel: {
      label: '<i class="fa fa-times"></i> Cerrar',
      callback: function (result) {
        callback(false)
      }
    }
  }
  if (new_item !== false) {
    menu_buttons.additem = {
      label: '<i class="fa fa-file-o"></i> Nuevo',
      className: 'button-style pull-right',
      callback: function (result) {
        callback('add_item')
      }
    }
  }

  menu_buttons.ok = {
    label: '<i class="fa fa-check"></i> Continuar',
    className: 'button-style pull-right',
    callback: function (result) {
      callback(true)
    }
  }

  var dialog = bootbox.dialog({
    title: '<i class="fa fa-exclamation-circle" aria-hidden="true"></i> Advertencia!',
    message: msg,
    size: 'large',
    buttons: menu_buttons
  });
}

/**
 * @param {*} url_base 
 * @param {*} tipo_contribuyente  persona juridica (pjrl) o natural (sol)
 * @param {*} token 
 * @param {*} numero 
 * pw => previo, hace referencia a la pagina previo, donde debe retornar cuando termine la edicion
 */
var editLicenciaMiniBootbox = function (url_base, tipo_contribuyente, token, numero) {

  var contribuyente = tipo_contribuyente === "1" ? 'sol' : 'pjrl';
  var display_block = ""
  var detector = new window.MobileDetect(window.navigator.userAgent)
  if (Boolean(detector.mobile())) {
    display_block = "display: block!important; margin-left: -35px;"
  }

  var content = '<div class="row"> <div class="col-12"><ol class=" breakcrumb-solo" style="margin-top: 0px; margin-left: -30px;' + display_block + '">' +
    '<li class="active" >' +
    '<a href="' + url_base + '?edit=' + contribuyente + '&token=' + token + '&num=' + numero + '&call=pw" style="display: inline-flex;"><span class="paso2-active"></span>' +
    '<span class="texto">Datos del Contribuyente</span> </a>' +
    '</li>' +
    '<li class="active" >' +
    '<a href="' + url_base + '?edit=ds&token=' + token + '&num=' + numero + '&call=pw" style="display: inline-flex;"><span class="paso3-active"></span>' +
    '<span class="texto">Domicilio del Contribuyente</span></a>' +
    '</li>' +
    '<li class="active" >' +
    '<a href="' + url_base + '?edit=dae&token=' + token + '&num=' + numero + '&call=pw" style="display: inline-flex;"><span class="paso4-active"></span>' +
    '<span class="texto">Domicilio Actividad Económica</span></a>' +
    '</li>' +
    '</div></div>';
  bootbox.dialog({
    title: '<i class="fa fa-exclamation-circle" aria-hidden="true"></i> Advertencia!',
    message: content,
    size: 'large',
    buttons: {
      cancel: {
        label: '<i class="fa fa-times"></i> Cancelar',
        className: 'btn-secondary',
        callback: function () {
          console.log('Custom cancel clicked');
        }
      }
    }
  });
}

/**
 * @param {*} url_base 
 * @param {*} tipo_contribuyente  persona juridica (pjrl) o natural (sol)
 * @param {*} token 
 * @param {*} numero 
 * pw => previo, hace referencia a la pagina previo, donde debe retornar cuando termine la edicion
 */
var editPrescripcioMiniBootBox = function (url_base, tipo_contribuyente, token, numero) {
  console.log(url_base)
  var contribuyente = tipo_contribuyente === "1" ? 'sol' : 'pjrl';
  var display_block = ""
  var detector = new window.MobileDetect(window.navigator.userAgent)
  if (Boolean(detector.mobile())) {
    display_block = "display: block!important; margin-left: -35px;"
  }


  var content = '<div class="row"> <div class="col-12"><ol class=" breakcrumb-solo" style="margin-top: 0px; margin-left: -30px;' + display_block + '">' +
    '<li class="active" >' +
    '<a href="' + url_base + '?edit=' + contribuyente + '&token=' + token + '&num=' + numero + '&call=pw" style="display: inline-flex;"><span class="paso2-active"></span>' +
    '<span class="texto">Datos del Contribuyente</span> </a>' +
    '</li>' +
    '<li class="active" >' +
    '<a href="' + url_base + '?edit=domcon&token=' + token + '&num=' + numero + '&call=pw" style="display: inline-flex;"><span class="paso3-active"></span>' +
    '<span class="texto">Domicilio del Contribuyente</span></a>' +
    '</li>' +
    '<li class="active" >' +
    '<a href="' + url_base + '?edit=objtri&token=' + token + '&num=' + numero + '&call=pw" style="display: inline-flex;"><span class="paso4-active"></span>' +
    '<span class="texto">Objetos Tributarios</span></a>' +
    '</li>' +
    '<li class="active" >' +
    '<a href="' + url_base + '?edit=cumpreq&token=' + token + '&num=' + numero + '" style="display: inline-flex;"><span class="paso5-active"></span>' +
    '<span class="texto">Cumplimiento de Requisitos</span></a>' +
    '</li>' +
    '</ol></div></div>';
  bootbox.dialog({
    title: '<i class="fa fa-exclamation-circle" aria-hidden="true"></i> Advertencia!',
    message: content,
    size: 'large',
    buttons: {
      cancel: {
        label: '<i class="fa fa-times"></i> Cancelar',
        className: 'btn-secondary',
        callback: function () {
          console.log('Custom cancel clicked');
        }
      }
    }
  })
}
/**
 * 
 * @param {*} url_base 
 * @param {*} tipo_contribuyente  persona juridica (pjrl) o natural (sol)
 * @param {*} token 
 * @param {*} numero 
 * pw => previo, hace referencia a la pagina previo, donde debe retornar cuando termine la edicion
 */
var editLicenciaBootbox = function (url_base, tipo_contribuyente, token, numero, is_data_contribuyente_complete,
  is_data_domicilio, is_complete_data_domicilio_actividad_economica) {
  var contribuyente = tipo_contribuyente === "1" ? 'sol' : 'pjrl';
  var display_block = ""
  var detector = new window.MobileDetect(window.navigator.userAgent)
  if (Boolean(detector.mobile())) {
    display_block = "display: block!important; margin-left: -35px;"
  }

  var icon_data_contribuyente = '<i class="fa fa-check icon" aria-hidden="true" ></i>';
  if (!is_data_contribuyente_complete)
    icon_data_contribuyente = '<i class="fa fa-times icon" aria-hidden="true" ></i>';

  var icon_data_domicilio = '<i class="fa fa-check icon" aria-hidden="true" ></i>';
  if (!is_data_domicilio)
    icon_data_domicilio = '<i class="fa fa-times icon" aria-hidden="true" ></i>';

  var icon_data_domicilio_actividad = '<i class="fa fa-check icon" aria-hidden="true" ></i>';
  if (!is_complete_data_domicilio_actividad_economica)
    icon_data_domicilio_actividad = '<i class="fa fa-times icon" aria-hidden="true" ></i>';

  var content = '<div class="row"> <div class="col-12"><ol class=" breakcrumb-solo" style="margin-top: 0px; margin-left: -30px;' + display_block + '">' +
    '<li class="active" >' +
    '<a href="' + url_base + '?edit=' + contribuyente + '&token=' + token + '&num=' + numero + '" style="display: inline-flex;"><span class="paso2-active"></span>' +
    '<span class="texto">Datos del Contribuyente</span> ' +
    '<br/> ' + icon_data_contribuyente + '</a>' +
    '</li>' +
    '<li class="active" >' +
    '<a href="' + url_base + '?edit=ds&token=' + token + '&num=' + numero + '" style="display: inline-flex;"><span class="paso3-active"></span>' +
    '<span class="texto">Domicilio del Contribuyente</span>' +
    '<br/> ' + icon_data_domicilio + '</a>' +
    '</li>' +
    '<li class="active" >' +
    '<a href="' + url_base + '?edit=dae&token=' + token + '&num=' + numero + '" style="display: inline-flex;"><span class="paso4-active"></span>' +
    '<span class="texto">Domicilio Actividad Económica</span>' +
    '<br/> ' + icon_data_domicilio_actividad + '</a>' +
    '</li>' +
    '<li class="active" >' +
    '<a href="' + url_base + '?edit=preview&token=' + token + '&num=' + numero + '" style="display: inline-flex;"><span class="paso5-active"></span>' +
    '<span class="texto">Confirmación de Datos</span></a>' +
    '</li>' +
    '</div></div>';
  bootbox.dialog({
    title: '<i class="fa fa-exclamation-circle" aria-hidden="true"></i> Menu Edición!',
    message: content,
    size: 'large',
    buttons: {
      cancel: {
        label: '<i class="fa fa-times"></i> Cancelar',
        className: 'btn-secondary',
        callback: function () {
          console.log('Custom cancel clicked');
        }
      }
    }
  });
}


/**
 * 
 * @param {*} url_base 
 * @param {*} tipo_contribuyente  persona juridica (pjrl) o natural (sol)
 * @param {*} token 
 * @param {*} numero 
 * pw => previo, hace referencia a la pagina previo, donde debe retornar cuando termine la edicion
 */
var editPrescripcionBootbox = function (url_base, tipo_contribuyente, token, numero, is_data_contribuyente_complete,
  is_data_domicilio, is_complete_data_requisitos, is_complete_requisitos) {
  var contribuyente = tipo_contribuyente === "1" ? 'sol' : 'pjrl';
  var display_block = ""
  var detector = new window.MobileDetect(window.navigator.userAgent)
  if (Boolean(detector.mobile())) {
    display_block = "display: block!important; margin-left: -35px;"
  }

  var icon_data_contribuyente = '<i class="fa fa-check icon" aria-hidden="true" ></i>';
  if (!is_data_contribuyente_complete)
    icon_data_contribuyente = '<i class="fa fa-times icon" aria-hidden="true" ></i>';

  var icon_data_domicilio = '<i class="fa fa-check icon" aria-hidden="true" ></i>';
  if (!is_data_domicilio)
    icon_data_domicilio = '<i class="fa fa-times icon" aria-hidden="true" ></i>';

  var icon_data_objetos_tributarios = '<i class="fa fa-check icon" aria-hidden="true" ></i>';
  if (!is_complete_data_requisitos)
    icon_data_objetos_tributarios = '<i class="fa fa-times icon" aria-hidden="true" ></i>';

  var icon_data_requisitos = '<i class="fa fa-check icon" aria-hidden="true" ></i>';
  if (!is_complete_requisitos)
    icon_data_requisitos = '<i class="fa fa-times icon" aria-hidden="true" ></i>';

  var content = '<div class="row"> <div class="col-12"><ol class=" breakcrumb-solo" style="margin-top: 0px; margin-left: -30px;' + display_block + '">' +
    '<li class="active" >' +
    '<a href="' + url_base + '?edit=' + contribuyente + '&token=' + token + '&num=' + numero + '" style="display: inline-flex;"><span class="paso2-active"></span>' +
    '<span class="texto">Datos del Contribuyente</span> ' +
    '<br/> ' + icon_data_contribuyente + '</a>' +
    '</li>' +
    '<li class="active" >' +
    '<a href="' + url_base + '?edit=domcon&token=' + token + '&num=' + numero + '" style="display: inline-flex;"><span class="paso3-active"></span>' +
    '<span class="texto">Domicilio del Contribuyente</span>' +
    '<br/> ' + icon_data_domicilio + '</a>' +
    '</li>' +
    '<li class="active" >' +
    '<a href="' + url_base + '?edit=objtri&token=' + token + '&num=' + numero + '" style="display: inline-flex;"><span class="paso4-active"></span>' +
    '<span class="texto">Objetos Tributarios</span>' +
    '<br/> ' + icon_data_objetos_tributarios + '</a>' +
    '</li>' +
    '<li class="active" >' +
    '<a href="' + url_base + '?edit=cumpreq&token=' + token + '&num=' + numero + '" style="display: inline-flex;"><span class="paso5-active"></span>' +
    '<span class="texto">Cumplimiento de Requisitos</span>' +
    '<br/> ' + icon_data_requisitos + '</a>' +
    '</li>' +
    '<li class="active" >' +
    '<a href="' + url_base + '?edit=preview&token=' + token + '&num=' + numero + '" style="display: inline-flex;"><span class="paso6-active"></span>' +
    '<span class="texto">Confirmación de Datos</span></a>' +
    '</li>' +
    '</div></div>';
  bootbox.dialog({
    title: '<i class="fa fa-exclamation-circle" aria-hidden="true"></i> Menu Edición!',
    message: content,
    size: 'large',
    buttons: {
      cancel: {
        label: '<i class="fa fa-times"></i> Cancelar',
        className: 'btn-secondary',
        callback: function () {
          console.log('Custom cancel clicked');
        }
      }
    }
  });
}


////////////////////////////////////////////
//MAP
var _map = undefined;
//var vectorSource = undefined;
var createMap = function (_bound, _zoom, callback = undefined) {
  if (!jQuery('#mapUbicacionActividadEconomica').html()) {

    let coordinate = convertToUniversalFormat(_bound);
    let longitud_latitud = window.ol.proj.transform(coordinate, 'EPSG:3857', 'EPSG:4326')

    var view = new window.ol.View({
      center: coordinate, //this.map.getView().getCenter(),
      zoom: _zoom //this.map.getView().getZoom()
    });

    //cargamos los layers         
    _map = new window.ol.Map({
      controls: window.ol.control.defaults().extend([
        new window.ol.control.FullScreen({
          source: 'fullscreen'
        })
      ]),
      layers: [
        new window.ol.layer.Tile({
          preload: Infinity,
          source: new window.ol.source.OSM({
            attributions: [
              'All maps © <a href="https://www.opencyclemap.org/">OpenCycleMap</a>', window.ol.source.OSM.ATTRIBUTION],
            url:
              'https://{a-c}.tile.thunderforest.com/cycle/{z}/{x}/{y}.png?apikey=9ba6de985c764b44ba0aa2cf151b0d00',
          })
        })
      ],
      loadTilesWhileInteracting: true,
      view: view
    });
    _map.setTarget("mapUbicacionActividadEconomica");
    _map.getView().setCenter(coordinate);
    _map.getView().setZoom(_zoom);

    setTimeout(() => {
      _map.getView().setCenter(coordinate);
      //_map.getView().setCenter([-7364557.60, -1966711.86]);

      _map.updateSize()
      if (callback !== undefined)
        callback(longitud_latitud)
    }, 1500);
    return _map;
  }
}

var getImage = function (map, callback) {
  map.once('postcompose', function (event) {
    var canvas = event.context.canvas;
    if (navigator.msSaveBlob) {
      navigator.msSaveBlob(canvas.msToBlob(), 'map.png');
    } else {
      canvas.toBlob(function (blob) {
        var blobUrl = URL.createObjectURL(blob);
        window.blobToBase64(blob, function (img64) {
          if (Boolean(img64)) {
            return callback(img64);
          }
          return callback("");
        });
      });
    }
  });
  map.renderSync();
}

/**
 * optiene el punto central del poligono
 * @param {array} pts 
 */
var getCenterCoorderdanada = function (pts) {

  var first = pts[0], last = pts[pts.length - 1];
  if (first[0] != last[0] || first[1] != last[1]) pts.push(first);
  var twicearea = 0,
    x = 0, y = 0,
    nPts = pts.length,
    p1, p2, f;
  for (var i = 0, j = nPts - 1; i < nPts; j = i++) {
    p1 = pts[i];
    p2 = pts[j];
    f = (p1[1] - first[1]) * (p2[0] - first[0]) - (p2[1] - first[1]) * (p1[0] - first[0]);
    twicearea += f;
    x += (p1[0] + p2[0] - 2 * first[0]) * f;
    y += (p1[1] + p2[1] - 2 * first[1]) * f;
  }
  f = twicearea * 3;
  return [x / f + first[0], y / f + first[1]]
}

var paintIcon = function (coordinate, map) {

  let _coordinate = window.convertToUniversalFormat(coordinate);

  var vectorSource = new window.ol.source.Vector({
  });
  var vectorLayer = new window.ol.layer.Vector({
    source: vectorSource,
  });

  var iconFeature = new window.ol.Feature({
    geometry: new window.ol.geom.Point(_coordinate),
  });

  var iconStyle = [new window.ol.style.Style({
    image: new window.ol.style.Icon({
      anchor: [0.5, 0.5],
      anchorXUnits: 'fraction',
      anchorYUnits: 'fraction',
      src: 'http://maps.google.com/mapfiles/ms/micons/blue.png',
      crossOrigin: 'anonymous',
    })
  })];
  iconFeature.setStyle(iconStyle);
  vectorSource.addFeature(iconFeature);
  vectorLayer = new window.ol.layer.Vector({
    source: vectorSource,
  });
  map.addLayer(vectorLayer);

  /////////////////////
  //move to icon feature
  var translate1 = new window.ol.interaction.Translate({
    features: new window.ol.Collection([iconFeature])
  });

  map.addInteraction(translate1);

  map.getView().setCenter(_coordinate);
  map.updateSize()
}

function myIP() {
  var xmlhttp = undefined
  if (window.XMLHttpRequest)
    xmlhttp = new XMLHttpRequest();
  else
    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

  xmlhttp.open("GET", "https://api.ipify.org/?format=jsonp&callback=get_ip", false);
  xmlhttp.send();

  var hostipInfo = xmlhttp.responseText.split("\n");

  for (var i = 0; hostipInfo.length >= i; i++) {
    var ipAddress = hostipInfo[i].split(":");
    ipAddress = ipAddress[1].replace(/;/g, '').replace(/}/g, '').replace(/\)/g, '').replace(/\"/g, '')  //""181.188.189.226"});"  replace(/blue/g, "red");
    return ipAddress
  }
  return 0;
}

var getBrowserInfo = function () {
  var detector = new window.MobileDetect(window.navigator.userAgent)

  if (Boolean(detector.mobile())) {
    return {
      device: detector.mobile(),
      browser: detector.userAgent(),
      os: detector.os(),
      version: detector.versionStr('Build'),
    };
  } else {
    return {
      device: '',
      browser: getNameBrowser(),
      os: checkOS(),
      version: '',
    };
  }
}

function checkOS() {
  if (navigator.userAgent.indexOf('IRIX') !== -1) { var OpSys = "Irix"; }
  else if ((navigator.userAgent.indexOf('Win') !== -1) &&
    (navigator.userAgent.indexOf('4.90') !== -1)) { var OpSys = "Windows Millennium"; }
  else if ((navigator.userAgent.indexOf('Win') !== -1) &&
    (navigator.userAgent.indexOf('98') !== -1)) { var OpSys = "Windows 98"; }
  else if ((navigator.userAgent.indexOf('Win') != -1) &&
    (navigator.userAgent.indexOf('95') !== -1)) { var OpSys = "Windows 95"; }
  else if (navigator.appVersion.indexOf("16") !== -1) { var OpSys = "Windows 3.1"; }
  else if (navigator.appVersion.indexOf("NT") !== -1) { var OpSys = "Windows NT"; }
  else if (navigator.appVersion.indexOf("SunOS") !== -1) { var OpSys = "SunOS"; }
  else if (navigator.appVersion.indexOf("Linux") !== -1) { var OpSys = "Linux"; }
  else if (navigator.userAgent.indexOf('Mac') !== -1) { var OpSys = "Macintosh"; }
  else if (navigator.appName == "WebTV Internet Terminal") { var OpSys = "WebTV"; }
  else if (navigator.appVersion.indexOf("HP") !== -1) { var OpSys = "HP-UX"; }
  else { var OpSys = "other"; }
  return OpSys;
}

function getNameBrowser() {
  var ua = navigator.userAgent, tem,
    M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
  if (/trident/i.test(M[1])) {
    tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
    return 'IE ' + (tem[1] || '');
  }
  if (M[1] === 'Chrome') {
    tem = ua.match(/\b(OPR|Edge)\/(\d+)/);
    if (tem != null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
  }
  M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
  if ((tem = ua.match(/version\/(\d+)/i)) != null) M.splice(1, 1, tem[1]);
  return M.join(' - ');
}

var getPais = function (ip) {
  var xhttp = undefined;
  if (window.XMLHttpRequest)
    xhttp = new XMLHttpRequest();
  else
    xhttp = new ActiveXObject("Microsoft.XMLHTTP");

  //var xhttp = new XMLHttpRequest();
  /*xhttp.onreadystatechange = function() {
      var respuesta = this.responseText;
      var response = JSON.parse(respuesta);
      
      if (this.readyState == 4 && this.status == 200) {
          alert("Hola visitante de "+ response.geoplugin_countryName);
          return 
      }
  };*/
  xhttp.open("GET", 'http://www.geoplugin.net/json.gp?ip=' + ip, false);
  xhttp.send();

  var hostipInfo = JSON.parse(xhttp.responseText);

  return hostipInfo.geoplugin_city + " - " + hostipInfo.geoplugin_countryName
}

/**
 * permit disable button
 * @param {*} button  is object javascript
 */
var disableButton = function (button) {
  button.disabled = true
  button.classList.add("btn-disabled")
}

/**
 * permit enable button
 * @param {*} button  is object javascript
 */
var enableButton = function (button) {
  button.disabled = false
  button.classList.remove("btn-disabled")
}

var fullUrl = function () {
  return window.location.protocol + "//" + window.location.hostname + window.location.pathname;
}

var fechaToLiteral = function (fecha) {
  var date = new Date(fecha);
  var options = { year: 'numeric', month: 'long', day: 'numeric' };
  return date.toLocaleDateString("es-ES", options)
}


var objetJsonToBase64 = function (object_data) {
  //let objJsonStr = JSON.stringify(object_data);
  //return Buffer.from(objJsonStr).toString("base64");
  return btoa(JSON.stringify(object_data))
}

var base64ToObjectJson = function (string_base_64){
  return JSON.parse(atob(string_base_64))
}